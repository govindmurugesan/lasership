(function () {
    'use strict';

    angular
        .module('beam')
        .factory('manifestDetailService', manifestDetailService);

    manifestDetailService.$inject = ['$window', 'deviceService', '$q', 'driverService', '$log','AppVersion','$cordovaNetwork'];
    function manifestDetailService($window, deviceService, $q, driverService, $log,AppVersion,$cordovaNetwork) {
        var service = {
            getManifestDetail: getManifestDetail
        };

        return service;


        function getManifestDetail(barcode) {
            var def = $q.defer();
            if(deviceService.getConnectionStatus()){
            var driverInfo = driverService.getDriverInfo();
            var _date = new Date();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'InfoRequest',
                'eventModifier': 'PCDL',
                'appversion': AppVersion,
                'driverId': driverInfo.driverId,
                'facilityId': driverInfo.FacilityCode,
            }
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([deviceService.getLocation(),
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID()])
                .then(function (data) {
                    var _pos = data[0];
                    var _ip = data[1];
                    var _phoneNum = data[2];
                    var uuid = data[3];
                    var manifestDetailRequest = new WLResourceRequest(
                        "/adapters/Manifest/getManifestDetail",
                        WLResourceRequest.POST,
                        2000
                    );
                    var req = {
                        "DriverInfo": {
                             "DriverID": driverInfo.driverId,
                            "FacilityID": driverInfo.FacilityCode,
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "LocationInfo": {
                            "Longitude": _pos.long,
                            "Latitude": _pos.lat,
                            "GPSPrecision": "1",
                            "GPSFixDateTime": _pos.timeStamp.toMyISOString()
                        },
                        "SecurityGuid": md,
                        "EventISODateTime": guidRequest.eventDate.toMyISOString(),
                        "BarCode": encodeURIComponent(barcode.trim())
                    };
                    manifestDetailRequest.setQueryParameter("params", [req]);
                    try {
                        manifestDetailRequest.send().then(
                            function (response) {
                                if (response.status == 200 && response.responseJSON.isSuccessful) // Logout Successful else throw error
                                {
                                    def.resolve(response.responseJSON);
                                }
                                else
                                    def.reject(response.responseJSON);

                            },
                            function (error) {
                                def.reject(error);

                            }
                        );
                    } catch (error) {
                        $log.error('Error calling adapter', error)
                    }

                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            }else{
                def.resolve("");
            }
            return def.promise;
        }
    }
})();