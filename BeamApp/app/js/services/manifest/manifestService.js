(function () {
    'use strict';

    angular
        .module('beam')
        .factory('manifestService', manifestService);

    manifestService.$inject = ['$window', 'deviceService', '$q', 'manifestSerenityService', 'jsonStoreService', '$log'];
    function manifestService($window, deviceService, $q, manifestSerenityService, jsonStoreService, $log) {
        var service = {
            getManifestCount: getManifestCount,
            getManifestInfo: getManifestInfo,
            refreshManifestInfo: refreshManifestInfo,
            getManifestDetailByStopIdentifier: getManifestDetailByStopIdentifier,
            getManifestDetailByBarcode: getManifestDetailByBarcode,
            getCustomerIds : getCustomerIds,
            getPickUpDetailByStopIdentifier:getPickUpDetailByStopIdentifier,
            getManifestDeliveryCount : getManifestDeliveryCount,
            addManifestDetail : addManifestDetail,
            addManifestData : addManifestData,
            addUnassignPickUpToManifest : addUnassignPickUpToManifest,
            addPickupManifestData : addPickupManifestData,
            addUnassignPickupManifestDetail : addUnassignPickupManifestDetail,
            getManifestDeliveryBarcode : getManifestDeliveryBarcode,
            getManifestPickupBarcode : getManifestPickupBarcode
        };

        return service;

        function getManifestCount() {
            var deferred = $q.defer();
            var manColl = jsonStoreService.getManifestCollection();
            manColl.count().then(function (res) {
                deferred.resolve(res);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getManifestInfo() {
            var manColl = jsonStoreService.getManifestCollection();
            return manColl.findAll();
        }

        function refreshManifestInfo() {
            var manColl = jsonStoreService.getManifestCollection();
            var def = $q.defer();
            manifestSerenityService.loadManifest().then(function (data) {
                angular.forEach(data.Pieces, function (piece, index) {
                    piece._deliveryItem = piece.Destination !==undefined;
                    piece._pickupItem = piece.Origin !==undefined;
                    manColl.add(piece,{markDirty: false});
                });
                def.resolve(data.Pieces?data.Pieces.length:0);
            }, function (err) {
                $log.error("Error loading manifest", err);
                return def.reject(err);
            });
            return def.promise;
        }

        function getManifestDetailByStopIdentifier(stopIdentifier) {
            var query = {
                'Destination.StopIdentifier': stopIdentifier
            };
            var options = {};
            var manColl = jsonStoreService.getManifestCollection();
            return manColl.find(query, options);
        }
        function getPickUpDetailByStopIdentifier(stopIdentifier) {
            var query = {
                'Origin.StopIdentifier': stopIdentifier
            };
            var options = {};
            var manColl = jsonStoreService.getManifestCollection();
            return manColl.find(query, options);
        }

        function getManifestDetailByBarcode(barcode) {
            var query = {
                'Barcode': barcode
            };
            var options = { exact: true };
            var manColl = jsonStoreService.getManifestCollection();
            return manColl.find(query, options);
        }

        function getManifestDeliveryCount() { 
            var query = {
                '_deliveryItem':true
            };
            var options = {};
            var manColl = jsonStoreService.getManifestCollection();
            return manColl.count(query, options);
        }

        function getManifestDeliveryBarcode() { 
            var query = {
                '_deliveryItem':true
            };
            var options = {};
            var manColl = jsonStoreService.getManifestCollection();
            return manColl.find(query, options);
        }

        function getManifestPickupBarcode() { 
            var query = {
                '_pickupItem':true
            };
            var options = {};
            var manColl = jsonStoreService.getManifestCollection();
            return manColl.find(query, options);
        }

        function getCustomerIds() {
            var def = $q.defer();
            var customerIds = [];
            getManifestInfo().then(function (data) {
                for (var item in data) {
                    customerIds.push(data[item].json.CustomerID);
                }
                def.resolve(customerIds);
            }, function (error) {
                $log.error("Error in getting manifest list", error);
                def.reject(error);
            });
            return def.promise;
        }
        function addManifestDetail(barcode,address,name,notes){
           var deferred = $q.defer();
           var manifestColl = jsonStoreService.getManifestCollection();

             var manifestDetail ={
                 Attributes: "",
                 Barcode: barcode,
                 ContainerType: "",
                 CustomerID: "",
                 Description: "",
                 Destination: {
                     Address: address,
                     Address2: "",
                     AddressQuality: "",
                     City: "",
                     Contact: barcode,
                     Country: "US",
                     DeliveryRoute: "",
                     DeliveryRouteSequence: "",
                     FacilityCode: "",
                     Instruction: notes,
                     Latitude: "",
                     LocationType: "",
                     Longitude: "",
                     Organization: "",
                     Phone: "",
                     PhoneExtension: "",
                     PostalCode: "",
                     State: "",
                     StopIdentifier: barcode,
                     UTCExpectedDeliveryBy: "NA"
                 },
                 _deliveryItem : true,
                 DimensionUnit: "",
                 HandleWithCare: "",
                 Height: "",
                 Length: "",
                 PieceKey: "",
                 SignatureRequirement: "",
                 SignatureWaiveable: "",
                 Weight: "",
                 WeightUnit: "",
                 Width: ""
             }

             manifestColl.add(manifestDetail,{markDirty: false}).then(function(data){
                 deferred.resolve(data);
             },function(error){
                 $log.error("Error in adding manifest detail",error);
                 deferred.reject(error);
             })
           return deferred.promise;
       }

       function addManifestData(data,barcode){
           var deferred = $q.defer();
           var manifestColl = jsonStoreService.getManifestCollection();
           var manifestDataValues = data.Pieces[0];

           var manifestData ={
               Attributes: manifestDataValues.Attributes,
               Barcode: barcode,
               ContainerType: manifestDataValues.ContainerType,
               CustomerID: manifestDataValues.CustomerID,
               Description: manifestDataValues.Description,
               Destination: {
                   Address: manifestDataValues.Destination.Address,
                   Address2: manifestDataValues.Destination.Address2,
                   AddressQuality: manifestDataValues.Destination.AddressQuality,
                   City: manifestDataValues.Destination.City,
                   Contact: manifestDataValues.Destination.Contact,
                   Country: manifestDataValues.Destination.Country,
                   DeliveryRoute: manifestDataValues.Destination.DeliveryRoute,
                   DeliveryRouteSequence: parseFloat(manifestDataValues.Destination.DeliveryRouteSequence),
                   FacilityCode: manifestDataValues.Destination.FacilityCode,
                   Instruction: manifestDataValues.Destination.Instruction,
                   Latitude: parseFloat(manifestDataValues.Destination.Latitude),
                   LocationType: manifestDataValues.Destination.LocationType,
                   Longitude: parseFloat(manifestDataValues.Destination.Longitude),
                   Organization: manifestDataValues.Destination.Organization,
                   Phone: manifestDataValues.Destination.Phone,
                   PhoneExtension: manifestDataValues.Destination.PhoneExtension,
                   PostalCode: manifestDataValues.Destination.PostalCode,
                   State: manifestDataValues.Destination.State,
                   StopIdentifier: manifestDataValues.Destination.StopIdentifier,
                   UTCExpectedDeliveryBy: manifestDataValues.Destination.UTCExpectedDeliveryBy
               },
               _deliveryItem: true,
               DimensionUnit: manifestDataValues.DimensionUnit,
               HandleWithCare: manifestDataValues.HandleWithCare,
               Height: manifestDataValues.Height,
               Length: manifestDataValues.Length,
               PieceKey: manifestDataValues.PieceKey,
               SignatureRequirement: manifestDataValues.SignatureRequirement,
               SignatureWaiveable: manifestDataValues.SignatureWaiveable,
               Weight: manifestDataValues.Weight,
               WeightUnit: manifestDataValues.WeightUnit,
               Width: manifestDataValues.Width
           }
           manifestColl.add(manifestData,{markDirty: false}).then(function(data){
               deferred.resolve(data);
           },function(error){
               $log.error("Error in adding manifest detail",error);
               deferred.reject(error);
           })
           return deferred.promise;
       }

       function addUnassignPickUpToManifest(barcode,notes,address,customer,name){
        var deferred = $q.defer();
           var manifestColl = jsonStoreService.getManifestCollection();
           
           var manifestDetail ={
               Attributes: "",
               Barcode: barcode,
               ContainerType: "",
               CustomerID: customer,
               Description: "",
               Origin: {
                   Address: address,
                   Address2: "",
                   AddressQuality: "",
                   City: "",
                   Contact: name,
                   Country: "US",
                   DeliveryRoute: "",
                   DeliveryRouteSequence: "",
                   FacilityCode: "",
                   Instruction: notes,
                   Latitude: "",
                   LocationType: "",
                   Longitude: "",
                   Organization: "",
                   Phone: "",
                   PhoneExtension: "",
                   PostalCode: "",
                   State: "",
                   StopIdentifier: barcode,
                   UTCExpectedDeliveryBy: ""
               },
               DimensionUnit: "",
               HandleWithCare: "",
               Height: "",
               Length: "",
               PieceKey: "",
               SignatureRequirement: "",
               SignatureWaiveable: "",
               Weight: "",
               WeightUnit: "",
               Width: ""
           }
           manifestColl.add(manifestDetail,{markDirty: false}).then(function(data){
               deferred.resolve(data);
           },function(error){
               $log.error("Error in adding manifest detail",error);
               deferred.reject(error);
           })
           return deferred.promise;
       }

       function addPickupManifestData(data,barcode){
           var deferred = $q.defer();
           var manifestColl = jsonStoreService.getManifestCollection();
           var manifestDataValues = data.Pieces[0];

           var manifestData ={
               Attributes: manifestDataValues.Attributes,
               Barcode: barcode,
               ContainerType: manifestDataValues.ContainerType,
               CustomerID: manifestDataValues.CustomerID,
               Description: manifestDataValues.Description,
               Origin: {
                   Address: manifestDataValues.Origin.Address,
                   Address2: manifestDataValues.Origin.Address2,
                   AddressQuality: manifestDataValues.Origin.AddressQuality,
                   City: manifestDataValues.Origin.City,
                   Contact: manifestDataValues.Origin.Contact,
                   Country: manifestDataValues.Origin.Country,
                   DeliveryRoute: manifestDataValues.Origin.DeliveryRoute,
                   DeliveryRouteSequence: parseFloat(manifestDataValues.Origin.DeliveryRouteSequence),
                   FacilityCode: manifestDataValues.Origin.FacilityCode,
                   Instruction: manifestDataValues.Origin.Instruction,
                   Latitude: parseFloat(manifestDataValues.Origin.Latitude),
                   LocationType: manifestDataValues.Origin.LocationType,
                   Longitude: parseFloat(manifestDataValues.Origin.Longitude),
                   Organization: manifestDataValues.Origin.Organization,
                   Phone: manifestDataValues.Origin.Phone,
                   PhoneExtension: manifestDataValues.Origin.PhoneExtension,
                   PostalCode: manifestDataValues.Origin.PostalCode,
                   State: manifestDataValues.Origin.State,
                   StopIdentifier: manifestDataValues.Origin.StopIdentifier,
                   UTCExpectedDeliveryBy: manifestDataValues.Origin.UTCExpectedDeliveryBy
               },
               _pickupItem: true,
               DimensionUnit: manifestDataValues.DimensionUnit,
               HandleWithCare: manifestDataValues.HandleWithCare,
               Height: manifestDataValues.Height,
               Length: manifestDataValues.Length,
               PieceKey: manifestDataValues.PieceKey,
               SignatureRequirement: manifestDataValues.SignatureRequirement,
               SignatureWaiveable: manifestDataValues.SignatureWaiveable,
               Weight: manifestDataValues.Weight,
               WeightUnit: manifestDataValues.WeightUnit,
               Width: manifestDataValues.Width
           }
           manifestColl.add(manifestData,{markDirty: false}).then(function(data){
               deferred.resolve(data);
           },function(error){
               $log.error("Error in adding manifest detail",error);
               deferred.reject(error);
           })
           return deferred.promise;
       }

       function addUnassignPickupManifestDetail(barcode,address,name,notes){
           var deferred = $q.defer();
           var manifestColl = jsonStoreService.getManifestCollection();

             var manifestDetail ={
                 Attributes: "",
                 Barcode: barcode,
                 ContainerType: "",
                 CustomerID: "",
                 Description: "",
                 Origin: {
                     Address: address,
                     Address2: "",
                     AddressQuality: "",
                     City: "",
                     Contact: barcode,
                     Country: "US",
                     DeliveryRoute: "",
                     DeliveryRouteSequence: "",
                     FacilityCode: "",
                     Instruction: notes,
                     Latitude: "",
                     LocationType: "",
                     Longitude: "",
                     Organization: "",
                     Phone: "",
                     PhoneExtension: "",
                     PostalCode: "",
                     State: "",
                     StopIdentifier: barcode,
                     UTCExpectedDeliveryBy: "NA"
                 },
                 _pickupItem : true,
                 DimensionUnit: "",
                 HandleWithCare: "",
                 Height: "",
                 Length: "",
                 PieceKey: "",
                 SignatureRequirement: "",
                 SignatureWaiveable: "",
                 Weight: "",
                 WeightUnit: "",
                 Width: ""
             }

             manifestColl.add(manifestDetail,{markDirty: false}).then(function(data){
                 deferred.resolve(data);
             },function(error){
                 $log.error("Error in adding manifest detail",error);
                 deferred.reject(error);
             })
           return deferred.promise;
       }
    }
})();