(function () {
    'use strict';

    angular
        .module('beam')
        .factory('eventsAdapterService', eventsAdapterService);

    eventsAdapterService.$inject = ['$q', 'SyncStats', '$log','$window'];
    function eventsAdapterService($q, SyncStats, $log,$window) {
        var service = {
            sendEvents: sendEventsSerial,
            sendDataToAdapter: sendDataToAdapter
        };
        var _serverResponses = [];
        return service;


        function sendEventsSerial(eventsData) {
            var promises = [];
            var deferred = $q.defer();
            if (SyncStats.syncInProgress) {
                //SyncStats.synced = false;
                deferred.resolve([]);
                //console.info('Send in progress. Skipping send request');
            }
            else {
                _serverResponses = [];
                SyncStats.syncInProgress = true;
                SyncStats.lastSynced = 'Started > ' + Date();
                //console.info('Sending Events @' + new Date());
                doAsyncInSeries(eventsData).then(function (results) {
                    //console.log('Sending Events Done @' + new Date(), _serverResponses.length);
                    //SyncStats.synced = false;
                    SyncStats.lastSynced = 'End > ' + Date();
                    SyncStats.syncInProgress = false;
                    deferred.resolve(_serverResponses);
                });
            }
            return deferred.promise;
        }

        function doAsyncInSeries(arr) {
            return arr.reduce(function (promise, item) {
                return promise.then(function (result) {
                    return sendDataToAdapter(item);
                });
            }, $q.when(null));
        }

        function sendDataToAdapter(event) {
            var deferred = $q.defer();
             if(event.json.Events[0]){
                 var jsonEvent = {
                BarCode: event.json.Events[0].BarCode,
                EventType: event.json.Events[0].EventType,
                EventModifier: event.json.Events[0].EventModifier,
                EventISODateTime: event.json.Events[0].EventISODateTime
            };
            //console.info('Sending >>',jsonEvent);
            
            var sendEventsReq = new WLResourceRequest(
                "/adapters/Events/sendEvents",
                WLResourceRequest.POST,
                2000
            );
            sendEventsReq.setQueryParameter("params", [event]);
            if(jsonEvent.EventModifier!=='LLOG' && $window.localStorage.beamCredentials){
                sendEventsReq.send().then(function (response) {
                    if (response.status == 200 && response.responseJSON.isSuccessful) {
                        deferred.resolve(response.responseJSON.adapterResponse);
                        _serverResponses.push(response.responseJSON.adapterResponse);
                    }
                    else {
                        $log.info('Event Send Err from Adapter', response);
                        deferred.resolve([]);
                    }
                }, function (err) {
                    $log.info('Error sending event to adapter : ' + JSON.stringify(jsonEvent), err);
                    deferred.resolve([]);
                });
            }
            else
                deferred.resolve([]);
             }
             else
                deferred.resolve([]);
            
            
            return deferred.promise;
        }
    }
})();