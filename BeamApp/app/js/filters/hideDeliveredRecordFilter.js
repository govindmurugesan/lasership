(function () {
      'use strict';

      angular
      .module('beam')
      .filter('hideDeliveredRecord', hideDeliveredRecord);
      function hideDeliveredRecord ($rootScope) {
            function hideDlvrdFilter (barcode,filterOptions) {
                  if(filterOptions != "")return true;
                  var showItem =true;
                  if($rootScope.packageCollection.length > 0){
                        angular.forEach($rootScope.packageCollection, function (piece, index) { 
                              if(showItem){
                                    if(piece.json.Barcode == barcode){
                                          if(piece.json.Delivered == 'true'){
                                                showItem= false;                   
                                          }  else if(piece.json.pickUp == 'true') {
                                                showItem= false; 
                                          }  else{
                                                showItem = true;
                                          }           
                                    }            
                              }
                        });
                  }else{
                        showItem = true
                  }
                  return showItem;
            }
            hideDlvrdFilter.$stateful = true;
            return hideDlvrdFilter;
      };
})();




