#!/usr/bin/env node

var path = require("path"),
  fs = require("fs"),
  rootdir = process.argv[2],
  exec = require('child_process').exec,
  platforms = process.env.CORDOVA_PLATFORMS;

var filestocopy = [{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner.jar':'/platforms/android/libs/ScanditBarcodeScanner.jar'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/beep.wav':'platforms/android/res/raw/beep.wav'},

{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/scandit_logo.png':'platforms/android/res/raw/scandit_logo.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/scandit_logo2x.png':'platforms/android/res/raw/scandit_logo2x.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/scandit_logo3x.png':'platforms/android/res/raw/scandit_logo3x.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/scan_line_blue.png':'platforms/android/res/raw/scan_line_blue.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/scan_line_white.png':'platforms/android/res/raw/scan_line_white.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/camera_swap_icon.png':'platforms/android/res/raw/camera_swap_icon.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/camera_swap_icon_pressed.png':'platforms/android/res/raw/camera_swap_icon_pressed.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/ocr_turn_on_icon.png':'platforms/android/res/raw/ocr_turn_on_icon.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/ocr_turn_off_icon.png':'platforms/android/res/raw/ocr_turn_off_icon.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/flashlight_turn_on_icon.png':'platforms/android/res/raw/flashlight_turn_on_icon.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/flashlight_turn_off_icon.png':'platforms/android/res/raw/flashlight_turn_off_icon.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/flashlight_turn_off_icon_pressed.png':'platforms/android/res/raw/flashlight_turn_off_icon_pressed.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/flashlight_turn_on_icon_pressed.png':'platforms/android/res/raw/flashlight_turn_on_icon_pressed.png'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/res/raw/ic_btn_search.png':'platforms/android/res/raw/ic_btn_search.png'},

{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/jni/x86/libscanditsdk-android-4.16.4.so':'platforms/android/libs/x86/libscanditsdk-android-4.16.4.so'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/jni/armeabi-v7a/libscanditsdk-android-4.16.4.so':'platforms/android/libs/armeabi-v7a/libscanditsdk-android-4.16.4.so'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/jni/armeabi/libscanditsdk-android-4.16.4.so':'platforms/android/libs/armeabi/libscanditsdk-android-4.16.4.so'},
{'/external/Scandit/scandit-barcodescanner-phonegap_4.16.4/src/android/sbs-android_4.16.4/ScanditBarcodeScanner/jni/arm64-v8a/libscanditsdk-android-4.16.4.so':'platforms/android/libs/arm64-v8a/libscanditsdk-android-4.16.4.so'},
]

exec('mfp cordova plugin add ' + rootdir + '/external/Scandit/scandit-barcodescanner-phonegap_4.16.4', function (error, stdout, stderr) {
    console.log('Added Scandit : ' + stdout);
  }); 
var androidRawResDir = path.resolve(__dirname, '../../platforms/android/res/raw');
try {
  fs.mkdirSync(androidRawResDir, function (err) {
    if (err) { console.error('Err creating raw for android ' + err); }
  });
} catch(ex) {}
    filestocopy.forEach(function(obj) {
        Object.keys(obj).forEach(function(key) {
            var val = obj[key];
            var srcfile = path.join(rootdir, key);
            var destfile = path.join(rootdir, val);
            //console.log("copying scandit --> "+srcfile+" to "+destfile);
            var destdir = path.dirname(destfile);
            if (fs.existsSync(srcfile) && fs.existsSync(destdir)) {
                fs.createReadStream(srcfile).pipe(
                fs.createWriteStream(destfile));
            }
        });
    });

console.log('**** Scandit Hook Done ****');

