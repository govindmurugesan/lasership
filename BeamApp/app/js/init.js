(function () {
    'use strict';

    window.Messages = {

    };

    window.wlInitOptions = {
        // Options to initialize with the WL.Client object.
        // For initialization options please refer to IBM MobileFirst Platform Foundation Knowledge Center.
        enableFIPS: false
    };

    window.MFPClientDefer = angular.injector(['ng']).get('$q').defer();
    window.wlCommonInit = window.MFPClientDefer.resolve;
    window.MFPClientDefer.promise.then(function wlCommonInit() {
        // Common initialization code goes here or use the angular service MFPClientPromise
        console.log('MobileFirst Client SDK Initilized');
        document.addEventListener(WL.Events.WORKLIGHT_IS_CONNECTED, connectDetected, false); 
        document.addEventListener(WL.Events.WORKLIGHT_IS_DISCONNECTED, disconnectDetected , false);
        WL.Client.setHeartBeatInterval(180);
        mfpMagicPreviewSetup();
    });

    function mfpMagicPreviewSetup() {
        var platform;
        //nothing to see here :-), just some magic to make ionic work with mfp preview, similar to ionic serve --lab
        if (WL.StaticAppProps.ENVIRONMENT === 'preview') {
            //running mfp preview (MBS or browser)
            platform = WL.StaticAppProps.PREVIEW_ENVIRONMENT === 'android' ? 'android' : 'ios';
            if (location.href.indexOf('?ionicplatform=' + platform) < 0) {
                location.replace(location.pathname + '?ionicplatform=' + platform);
            }
        }
    }

    function connectDetected(){
        var mfpServer = {
            mfpConnectionStatus: true, 
            mfpConnectionUpdatedTime: new Date()
        }
        window.localStorage.mfpConnection = JSON.stringify(mfpServer);
    }
    function disconnectDetected(){
        var mfpServer = {
            mfpConnectionStatus: false, 
            mfpConnectionUpdatedTime: new Date()
        }
        window.localStorage.mfpConnection = JSON.stringify(mfpServer);
    }

    window.onMapsApiLoaded = function () {
        console.log('Maps loaded');
    };

    $(document).on('WL/FIPS/READY', function (evt, obj) {
        //evt - Contains information about the event
        //obj - JavaScript object sent after the attempt to enable FIPS completes
        // if successfully enabled, object will be {enabled: true}
        // if enablement failed, object will be {enabled: false, msg: "message
        // indicating cause of the failure to enable"}
        console.log('FIPS Stats ',obj);
    });
})();

