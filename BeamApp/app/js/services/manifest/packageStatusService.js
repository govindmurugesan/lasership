(function () {
    'use strict';

    angular
        .module('beam')
        .factory('packageStatusService', packageStatusService);

    packageStatusService.$inject = ['$window', 'deviceService', '$q',  'jsonStoreService', 'manifestService', '$log'];
    function packageStatusService($window, deviceService, $q, jsonStoreService, manifestService, $log) {
        var service = {
            getCheckedOutPackages: getCheckedOutPackages,
            getAttemptedPackageCount: getAttemptedPackageCount,
            getNotAssignedPackageCount: getNotAssignedPackageCount,
            getDeliveredPackageCount: getDeliveredPackageCount,
            getPackageInfo: getPackageInfo,
            getPackageByBarcode: getPackageByBarcode,
            getPackageByStopIdentifier: getPackageByStopIdentifier,
            getPickUpPackageCount: getPickUpPackageCount,
            getUndeliveredPackageCount: getUndeliveredPackageCount,
            getLoadedPackageCount: getLoadedPackageCount,
            loadPackageInfo: loadPackageInfo,
            updatePackageStatus: updatePackageStatus,
            updateDeliveredPackageStatus: updateDeliveredPackageStatus,
            getDeliveredPackages: getDeliveredPackages,
            updateAttemptPackageStatus: updateAttemptPackageStatus,
            updateUnassignPickUpStatus: updateUnassignPickUpStatus,
            updatePickUpStatus: updatePickUpStatus,
            updateUnassignLoadPackageStatus: updateUnassignLoadPackageStatus,
            getLoadPackages: getLoadPackages,
            getLoadedCount: getLoadedCount
        };

        return service;

        function getAttemptedPackageCount() {
            var deferred = $q.defer();
            var condition1 = WL.JSONStore.QueryPart().like('Attempted', true);
            var condition2 = WL.JSONStore.QueryPart().like('Attempted', 'true');
            var options = {};
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            packageStatusColl.advancedFind([condition1,condition2], options).then(function (res) {
                deferred.resolve(res.length);
            }, function (err) {
                deferred.reject(err);
            })
            return deferred.promise;
        }

        function getNotAssignedPackageCount() {
            var deferred = $q.defer();
            var condition1 = WL.JSONStore.QueryPart().like('Loaded', true);
            condition1 = WL.JSONStore.QueryPart().like('Assigned', false);
            var condition2 = WL.JSONStore.QueryPart().like('Loaded', 'true');
            condition2 = WL.JSONStore.QueryPart().like('Assigned', 'false');
            var options = {};
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            packageStatusColl.advancedFind([condition1,condition2], options).then(function (res) {
                deferred.resolve(res.length);
            }, function (err) {
                deferred.reject(err);
            })
            return deferred.promise;
        }

        function getDeliveredPackageCount() {
            var deferred = $q.defer();
            var condition1 = WL.JSONStore.QueryPart().like('Delivered', true);
            var condition2 = WL.JSONStore.QueryPart().like('Delivered', 'true');
            var options = {};
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            packageStatusColl.advancedFind([condition1,condition2], options).then(function (res) {
                deferred.resolve(res.length);
            }, function (err) {
                deferred.reject(err);
            })
            return deferred.promise;
        }

        function getCheckedOutPackages() {
            var deferred = $q.defer();
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            packageStatusColl.count().then(function (res) {
                deferred.resolve(res);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getPackageInfo() {
            var packageColl = jsonStoreService.getPackageStatusCollection();
            return packageColl.findAll();
        }

        function getPackageByBarcode(barcode) {
            var deferred = $q.defer();
            var query = {
                'Barcode': barcode
            };
            var options = { exact : true };
            var packageColl = jsonStoreService.getPackageStatusCollection();
            packageColl.find(query, options).then(function (res) {
                deferred.resolve(res);
            }, function (err) {
                deferred.reject(err);
            })
            return deferred.promise;
        }

        function getPackageByStopIdentifier(stopIdentifier) {
             var deferred = $q.defer();            
            var query = {
                'StopIdentifier': stopIdentifier
            };
            var options = {exact : true};
            var packageColl = jsonStoreService.getPackageStatusCollection();
         packageColl.find(query, options).then(function (res) {
                deferred.resolve(res);
            }, function (err) {
                deferred.reject(err);
            })
            return deferred.promise;
        }

        function getPickUpPackageCount() {
            var deferred = $q.defer();
            var condition1 = WL.JSONStore.QueryPart().like('Pickup', true);
            var condition2 = WL.JSONStore.QueryPart().like('Pickup', 'true');
            var options = {};
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            packageStatusColl.advancedFind([condition1, condition2], options).then(function (res) {
                deferred.resolve(res.length);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getUndeliveredPackageCount() {
            var deferred = $q.defer();
            var condition1 = WL.JSONStore.QueryPart().like('Delivered', false);
            condition1 = WL.JSONStore.QueryPart().like('Pickup', false);
            var condition2 = WL.JSONStore.QueryPart().like('Delivered', 'false');
            condition2 = WL.JSONStore.QueryPart().like('Pickup', 'false');
            var options = {};
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            packageStatusColl.advancedFind([condition1, condition2], options).then(function (res) {
                deferred.resolve(res.length);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function loadPackageInfo(loadedBarcodes) {

            var deferred = $q.defer();
            var promiseColl = [];
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            angular.forEach(loadedBarcodes, function (piece, index) {
                if (piece.barcode.length > 0) {
                    promiseColl.push(
                        manifestService.getManifestDetailByBarcode(piece.barcode).then(
                            function (data) {
                                if (data.length > 0) {
                                    var options = {};
                                    var document = { Barcode: data[0].json.Barcode, StopIdentifier: data[0].json.Destination.StopIdentifier, Loaded: true, PickUp: false, Attempted: false, Delivered: false, Assigned: true }
                                    packageStatusColl.add(document,{markDirty: false});
                                } else {
                                    $log.info("manifest data is not available");
                                }

                            }, function (error) {
                                $log.error('Error in updating package status', error);
                            }
                        )
                    )
                } else {
                    $log.warn("package not found in package collection");
                }

            });
            $q.all(promiseColl).then(function () {
                deferred.resolve(promiseColl);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;

        }

        function updatePackageStatus(dataToUpdate) {
            var deferred = $q.defer();
            var options = {};
            jsonStoreService.getPackageStatusCollection().replace(dataToUpdate, options).then(function (numberOfDocsReplaced) {
                deferred.resolve(dataToUpdate.length);
            }).fail(function (error) {
                $log.error('Error in update package status', error);
                deferred.reject(error);
            });
            return deferred.promise;
        }

        function updateDeliveredPackageStatus(scannedBarcodes) {
            var deferred = $q.defer();
            var promiseColl = [];
            angular.forEach(scannedBarcodes, function (piece, index) {
                promiseColl.push(
                    getPackageByBarcode(piece.barcode).then(
                        function (data) {
                            if (data.length > 0) {
                                var document = data[0];
                                document.json.Delivered = true;
                                updatePackageStatus(document);
                            }else{                                
                                var deferred = $q.defer();
                                var options = {};
                                var newDocument = [] ;
                                manifestService.getManifestDetailByBarcode(piece.barcode).then(
                                    function (manifestData) {
                                        if (manifestData.length > 0) {
                                            newDocument = { Barcode: manifestData[0].json.Barcode, StopIdentifier: manifestData[0].json.Destination.StopIdentifier, Loaded: false, PickUp: false, Attempted: false, Delivered: true, Assigned: true }
                                            jsonStoreService.getPackageStatusCollection().add(newDocument,{markDirty: false}).then(function(res){
                                                deferred.resolve(res);
                                            },function(error){
                                                deferred.reject(error);
                                            });
                                        } else {
                                            newDocument = { Barcode: piece.barcode, StopIdentifier: piece.barcode, Loaded: false, PickUp: false, Attempted: false, Delivered: true, Assigned: true }
                                            jsonStoreService.getPackageStatusCollection().add(newDocument,{markDirty: false}).then(function(res){
                                                deferred.resolve(res);
                                            },function(error){
                                                deferred.reject(error);
                                            });
                                        }
                                    },function (error) {
                                        $log.error('Error in updating package status', error);
                                        deferred.reject(err);                                   
                                    }
                                ) 
                                return deferred.promise;
                            }
                        }, function (error) {
                            $log.error('err in updating package status', error);
                        }
                    )
                );
            });
            $q.all(promiseColl).then(function () {
                deferred.resolve(promiseColl);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getDeliveredPackages() {
            var deferred = $q.defer();
            var condition1 = WL.JSONStore.QueryPart().like('Delivered', true);
            var condition2 = WL.JSONStore.QueryPart().like('Delivered', 'true');
            var options = {};
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            packageStatusColl.advancedFind([condition1, condition2], options).then(function (res) {
                deferred.resolve(res);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function updateAttemptPackageStatus(scannedBarcodes) {
            var deferred = $q.defer();
            var promiseColl = [];
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            angular.forEach(scannedBarcodes, function (piece, index) {
                if (piece.barcode.length > 0) {
                    promiseColl.push(
                        getPackageByBarcode(piece.barcode).then(
                            function (data) {
                                if (data.length > 0) {
                                    var options = {};
                                    var document = data[0];
                                    document.json.Attempted = true;
                                    updatePackageStatus(document);
                                  
                                } else {
                                    var deferred = $q.defer();
                                    var options = {};
                                    var newDocument = [] ;
                                    manifestService.getManifestDetailByBarcode(piece.barcode).then(
                                        function (manifestData) {
                                            if (manifestData.length > 0) {
                                                newDocument = { Barcode: manifestData[0].json.Barcode, StopIdentifier: manifestData[0].json.Destination.StopIdentifier, Loaded: false, PickUp: false, Attempted: true, Delivered: false, Assigned: true }
                                                jsonStoreService.getPackageStatusCollection().add(newDocument,{markDirty: false}).then(function(res){
                                                    deferred.resolve(res);
                                                },function(error){
                                                    deferred.reject(error);
                                                });
                                            } else {
                                                newDocument = { Barcode: piece.barcode, StopIdentifier: piece.barcode, Loaded: false, PickUp: false, Attempted: true, Delivered: false, Assigned: true }
                                                jsonStoreService.getPackageStatusCollection().add(newDocument,{markDirty: false}).then(function(res){
                                                    deferred.resolve(res);
                                                },function(error){
                                                    deferred.reject(error);
                                                });

                                            }
                                        },function (error) {
                                            $log.error('Error in updating package status', error);
                                            deferred.reject(err);                                   
                                        }
                                    ) 
                                return deferred.promise;
                                }

                            }, function (error) {
                                $log.error('Error in updating package status', error);
                            }
                        )
                    )
                } else {
                    $log.warn("package not found in package collection");
                }

            });
            $q.all(promiseColl).then(function () {
                deferred.resolve(promiseColl);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function updateUnassignPickUpStatus(scannedBarcodes) {
            var deferred = $q.defer();
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            for (var scannedBarcodesIndex in scannedBarcodes) {
                var pieceDetail = { Barcode: scannedBarcodes[scannedBarcodesIndex].barcode, StopIdentifier: '', Loaded: false, PickUp: true, Attempted: false, Delivered: false, Assigned: false };
                packageStatusColl.add(pieceDetail,{markDirty: false});
                deferred.resolve(pieceDetail);
            }
            return deferred.promise;
        }

        function updateUnassignLoadPackageStatus(scannedBarcodes) {
            var deferred = $q.defer();
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            for (var scannedBarcodesIndex in scannedBarcodes) {
                var pieceDetail = { Barcode: scannedBarcodes[scannedBarcodesIndex].barcode, StopIdentifier: '', Loaded: true, PickUp: false, Attempted: false, Delivered: false, Assigned: false };
                packageStatusColl.add(pieceDetail,{markDirty: false});
                deferred.resolve(pieceDetail);
            }
            return deferred.promise;
        }

        function updatePickUpStatus(scannedBarcodes) {
            var deferred = $q.defer();
            var promiseColl = [];
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            angular.forEach(scannedBarcodes, function (piece, index) {
                if (piece.barcode.length > 0) {
                    promiseColl.push(
                        manifestService.getManifestDetailByBarcode(piece.barcode).then(
                            function (data) {
                                if (data.length > 0) {
                                    var options = {};
                                    var document = { Barcode: data[0].json.Barcode, StopIdentifier: data[0].json.StopIdentifier, Loaded: false, PickUp: true, Attempted: false, Delivered: false, Assigned: true }
                                    packageStatusColl.add(document,{markDirty: false});
                                } else {
                                    $log.info("package is not available");
                                }

                            }, function (error) {
                                $log.error('Error in updating package status', error);
                            }
                        )
                    )
                } else {
                    $log.warn("package not found in package collection");
                }

            });
            $q.all(promiseColl).then(function () {
                deferred.resolve(promiseColl);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getLoadedPackageCount() {
            var deferred = $q.defer();
            var query = { Loaded: true };
            var options = {};
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            packageStatusColl.count(query, options).then(function (res) {
                deferred.resolve(res);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }
        function getLoadedCount() {
            var deferred = $q.defer();
            var query = { Loaded: "true" };
            var options = {};
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            packageStatusColl.count(query, options).then(function (res) {
                deferred.resolve(res);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }
        function getLoadPackages() {
            var deferred = $q.defer();
            var condition1 = WL.JSONStore.QueryPart().like('Pickup', false);
            var condition2 = WL.JSONStore.QueryPart().like('Pickup', 'false');
            var options = {};
            var packageStatusColl = jsonStoreService.getPackageStatusCollection();
            packageStatusColl.advancedFind([condition1,condition2], options).then(function (res) {
                deferred.resolve(res.length);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        
    }
})();