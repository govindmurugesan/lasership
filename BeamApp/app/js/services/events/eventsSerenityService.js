(function () {
    'use strict';

    angular
        .module('beam')
        .factory('eventsSerenityService', eventsSerenityService);

    eventsSerenityService.$inject = ['$q', 'SyncStats', '$log', '$http', 'SerenityURL','$window'];
    function eventsSerenityService($q, SyncStats, $log, $http, SerenityURL,$window) {
        var service = {
            sendEvents: sendEvents,
            sendDataToSerenity: sendDataToSerenity
        };
        var _serverResponses = [];
        var reqdata = "";
        return service;


        function sendEvents(eventsData) {
            var promises = [];
            var deferred = $q.defer();
            if (SyncStats.directSyncInProgress) {
                //SyncStats.synced = false;
                deferred.resolve([]);
                //console.info('Send in progress. Skipping send request');
            }
            else {
                _serverResponses = [];
                SyncStats.directSyncInProgress = true;
                SyncStats.lastSynced = 'Started > ' + Date();
                //console.info('Sending Events to Serenity @' + new Date());
                doAsyncInSeries(eventsData).then(function (results) {
                    //console.log('Sending Events to Serenity Done @' + new Date(), _serverResponses.length);
                    //SyncStats.synced = false;
                    SyncStats.lastSynced = 'End > ' + Date();
                    SyncStats.directSyncInProgress = false;
                    deferred.resolve(_serverResponses);
                });
            }
            return deferred.promise;
        }

        function doAsyncInSeries(arr) {
            return arr.reduce(function (promise, item) {
                return promise.then(function (result) {
                    return sendDataToSerenity(item);
                });
            }, $q.when(null));
        }

        function getEventString(data) {
            var curDat = new Date().toISOString();
            var req = '/v10/';
            req += data.Events[0].EventType + '/';
            req += data.Events[0].EventModifier;
            req += '/Beam/';
            req += data.DeviceInfo.AppVersion + '/';
            req += data.DriverInfo.FacilityID + '/';
            req += data.DriverInfo.DriverID + '/';
            req += data.Events[0].EventISODateTime + '/';
            req += data.Events[0].LocationInfo.Longitude + '/';
            req += data.Events[0].LocationInfo.Latitude + '/';
            req += data.Events[0].LocationInfo.GPSFixDateTime + '/';
            req += data.Events[0].LocationInfo.GPSPrecision + '/';
            req += data.Events[0].SecurityGuid + '/';
            req += data.DeviceInfo.DeviceID + '/';
            req += data.DeviceInfo.PhoneIp + '/';
            req += data.DeviceInfo.PhoneNum + '/';
            req += data.DriverInfo.DriverID + '/';
            req += (data.Events[0].EventActionValue ? data.Events[0].EventActionValue : '') + '/';
            if (data.Events[0].SignatureWKT) { req += '1'; }
            else { req += '0'; }
            req += '//';
            if (data.Events[0].CustomerID && data.Events[0].CustomerID !== "") { req += data.Events[0].CustomerID; }
            else if (data.Events[0].Customer && data.Events[0].Customer !== "") { req += encodeURI(data.Events[0].Customer).replace("/", "%2F"); }
            req += '/';
            if (data.Events[0].PieceKey) { req += data.Events[0].PieceKey; }
            req += '/';
            if (data.Events[0].BarCode) { req += data.Events[0].BarCode; }
            req += '/';
            if (data.Events[0].Location) { req += data.Events[0].Location; }
            req += '/';
            if (data.Events[0].DoorTag) { req += data.Events[0].DoorTag; }
            req += '/';
            if (data.Events[0].TypedNameSignature) { req += data.Events[0].TypedNameSignature; }
            req += '//';
            if (data.Events[0].GroupingGuid) { req += data.Events[0].GroupingGuid; }
            return req;
        }

        function sendDataToSerenity(event) {
            var deferred = $q.defer();
            var responseArray = [];
            if(event.json.Events[0]){
            var jsonEvent = {
                            BarCode: event.json.Events[0].BarCode,
                            EventType: event.json.Events[0].EventType,
                            EventModifier: event.json.Events[0].EventModifier,
                            EventISODateTime: event.json.Events[0].EventISODateTime
                        };
           
            var jsonData = event.json;
            var reqdata = getEventString(jsonData);
            var serenityURL = SerenityURL + reqdata;
            if ($window.localStorage.beamCredentials) { //If user logged out then don't send anything
                if (event.json.Events[0].SignatureWKT == "" && (event.json.Events[0].Notes == undefined || event.json.Events[0].Notes == "")) {
                    $http.get(serenityURL, { timeout: 2000 }).then(function (success) {
                        var response = success;
                        response._eventID = jsonData.Events[0]._eventID;
                        response._jsonID = event._id;
                        response._errSerenitySend = false;
                        responseArray.push(response);
                        deferred.resolve(responseArray);
                        _serverResponses.push(responseArray);
                    }, function (error) {
                        var response = error;
                        response._eventID = jsonData.Events[0]._eventID;
                        response._jsonID = event._id;
                        response._errSerenitySend = true;
                        responseArray.push(response);
                        $log.error('Error sending event to serenity : ' + JSON.stringify(jsonEvent), error);
                        deferred.resolve(responseArray);
                    });
                } else {
                    var request = {
                        method: 'POST',
                        url: serenityURL,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: 'SignatureWKT=' + (jsonData.Events[0].SignatureWKT ? jsonData.Events[0].SignatureWKT : '') + '&AdditionalSignatureWKT=&Note=' + (jsonData.Events[0].Notes ? jsonData.Events[0].Notes : ''),
                        config: { timeout: 4000 }
                    };
                    $http(request).then(function (success) {
                        var response = success;
                        response._eventID = jsonData.Events[0]._eventID;
                        response._jsonID = event._id;
                        response._errSerenitySend = false;
                        responseArray.push(response);
                        deferred.resolve(responseArray);
                        _serverResponses.push(responseArray);
                    }, function (error) {
                        var response = error;
                        response._eventID = jsonData.Events[0]._eventID;
                        response._jsonID = event._id;
                        response._errSerenitySend = true;
                        responseArray.push(response);
                        $log.error('Error sending event to serenity : ' + JSON.stringify(jsonEvent), error);
                        deferred.resolve(responseArray);
                    });
                }
            }
            else{
                deferred.resolve([]);
            }

            }
            else
                deferred.resolve([]);
            return deferred.promise;
        }
    }
})();