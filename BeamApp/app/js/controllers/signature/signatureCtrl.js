  (function () {
    'use strict';

    angular
      .module('beam')
      .controller('signatureCtrl', signatureCtrl);

    signatureCtrl.$inject = ['$scope', '$state', '$ionicHistory', '$ionicModal', 'packageStatusService', 'jsonStoreService', 'notificationService', 
    '$stateParams', 'eventsService', '$q', '$rootScope', 'manifestService', '$ionicPopup', 'scanFunctionService', 'customerService','$window','scanService',
    '$ionicSideMenuDelegate', '$timeout', '$ionicLoading','$filter','$log','rootScopeService','$ionicConfig','deviceService'];

    function signatureCtrl($scope, $state, $ionicHistory, $ionicModal, packageStatusService, jsonStoreService, notificationService, 
    $stateParams, eventsService, $q, $rootScope, manifestService, $ionicPopup, scanFunctionService, customerService,$window,scanService,$ionicSideMenuDelegate, $timeout, $ionicLoading,
    $filter,$log,rootScopeService,$ionicConfig,deviceService) {
      var vm = this;
      vm.attemptScan = attemptScan;
      vm.complete = complete;
      vm.goBack = goBack;
      vm.location = "";
      vm.typedSignature = "";
      $rootScope.deliveryEvent = false;
      var SigCheckCount = 0;
      vm.answer1 = [];
      vm.answer2 = [];
      vm.name =[];
      vm.type = [];
      vm.CustomerConfig = [];
      var CustomerID = [];
      vm.questionnaire = [];
      vm.CustomerAttempt = [];
      vm.dropLocList = dropLocList;
      vm.droplocationvalue = "";
      vm.droplocation = "";
      vm.doorTagScan = doorTagScan;
      var EventActionValue = "" 
      vm.deliveryEventType = [];
      vm.eventTrigger = eventTrigger;
      var currentStopIdentifier;
      var questionnaireCount = 0;
      vm.attemptEventTrigger = attemptEventTrigger;
      var _deliveryScanItems = [];
      var _attemptScanItems = [];
      var _pickupScanItems = [];
      var _locationScanItems = [];
      var _unassignPickupScanItems = [];
      var _doorScanItems = [];
      var _doorScanItemBarcode = [];
      var _doorTag = "";
      var signatureRequired = "";

      $scope.$on('$ionicView.beforeEnter', function(){ 
        _deliveryScanItems = rootScopeService.getDeliveryScanItems();
        _attemptScanItems = rootScopeService.getAttemptScanItems();
        _pickupScanItems = rootScopeService.getPickupScanItems();
        _locationScanItems = rootScopeService.getLocationScanItems();
        _unassignPickupScanItems = rootScopeService.getUnassignPickupScanItems();
        _doorScanItems = rootScopeService.getDoorScanItems();
        _doorScanItemBarcode = rootScopeService.getDoorScanItemBarcode();
        vm.deliveryCount = _deliveryScanItems.length;
        if($window.localStorage.droplocationcode != undefined)
          vm.location = JSON.parse($window.localStorage.droplocationcode);
        if($window.localStorage.droplocationvalue != undefined)
          vm.droplocationvalue = JSON.parse($window.localStorage.droplocationvalue);
        if($window.localStorage.podname != undefined)
          vm.typedSignature = JSON.parse($window.localStorage.podname);
        if($window.localStorage.signatureRequired != undefined)
          signatureRequired = JSON.parse($window.localStorage.signatureRequired);
      });

      $scope.$on('$ionicView.afterLeave', function(){
        cordova.plugins.Keyboard.disableScroll(false);
      });

      $scope.$on('$ionicView.beforeLeave', function(){
        $window.screen.lockOrientation('portrait');
        $window.localStorage.podname = JSON.stringify(vm.typedSignature);
        $window.localStorage.signature = JSON.stringify($('#canvasSection').signature('toJSON'));
      });   

      $scope.$on('$ionicView.enter', function () {
        $window.screen.lockOrientation('landscape');
        cordova.plugins.Keyboard.disableScroll(true);
        $ionicConfig.views.swipeBackEnabled(false);
        $scope.$watch(function() {
          return $state.params;
        });
        if(_doorScanItems.length > 0){
          _doorTag = _doorScanItems[0].barcode;
        }else if(_doorScanItemBarcode.length > 0){
          _doorTag = _doorScanItemBarcode[0].barcode;
        }
        currentStopIdentifier = $state.params.stopIdentifier;
        $ionicSideMenuDelegate.canDragContent(false);
        vm.placeholder = placeholder;
        var promiseColl = [];
        var promiseCollattempt = [];
        angular.forEach(_deliveryScanItems, function (piece, index) {
          var deferred = $q.defer();
          promiseColl.push(
            manifestService.getManifestDetailByBarcode(piece.barcode).then(
              function (manifestData) {
                angular.forEach(manifestData, function (piece, index) {
                  if (piece.json.SignatureRequirement == "" || piece.json.SignatureRequirement == "NotRequired") {
                    CustomerID.push(piece.json.CustomerID);
                  } else {
                    $('#selectLocation').css({ 'background': '#eff0f1', 'pointer-events': 'none' });
                    SigCheckCount += 1;
                    vm.droplocationvalue = "Person";
                    vm.location = "LSR-PERSON";
                  }
                })
              }, function (error) {
                $log.warn("Error in getting manifest detail",error);
              }
            ))
        })
        $q.all(promiseColl).then(function (data) {
          CustomerID = jQuery.unique(CustomerID);
          customerService.getQuestionnaire(CustomerID).then(function(data){
            angular.forEach(data, function (piece, index) { 
              angular.forEach(piece, function (questionnaireArray, index) { 
                vm.questionnaire.push(questionnaireArray);
            })
            })
          })

          customerService.getCustomerConfigByID(CustomerID).then(function (data) {
            angular.forEach(data[0], function (piece, index) {
              vm.CustomerConfig.push(piece);
            })
            var uniqueCustomerConfig = jQuery.unique(vm.CustomerConfig);
            vm.CustomerConfig = $filter('orderBy')(uniqueCustomerConfig, 'Sort');
          }, function (err) {
            $log.info("Error in getting customer info",err);
          })
        }, function (err) {
          $log.info("Error in promise call",err);
          deferred.reject(err);
        });

        angular.forEach(_attemptScanItems, function (piece, index) {
          var deferred = $q.defer();
          promiseCollattempt.push(
            manifestService.getManifestDetailByBarcode(piece.barcode).then(
              function (manifestData) {
                angular.forEach(manifestData, function (piece, index) {
                  CustomerID.push(piece.json.CustomerID);
                })
              }, function (error) {
                $log.warn("Error in getting manifest detail",error);
              }
            ))
        })
        $q.all(promiseCollattempt).then(function (data) {
          CustomerID = jQuery.unique(CustomerID);
          customerService.getCustomerAttemptByID(CustomerID).then(function (data) {
            angular.forEach(data[0], function (piece, index) {
              vm.CustomerAttempt.push(piece);
            })
            vm.CustomerAttempt = jQuery.unique(vm.CustomerAttempt);
          }, function (err) {
              $log.warn("Error in getting customer config",err);
          })
        }, function (err) {
          $log.info("Error in promise call",err);
          deferred.reject(err);
        });
        $scope.$apply();
      });
      $scope.$on("$ionicView.beforeEnter", function () {
      
      });
      $scope.$on("$ionicView.afterEnter", function () {
        notificationService.hideLoad(); 
      });
      function goBack() {
        $window.screen.lockOrientation('portrait');
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('app.scan.delivery', { 'stopIdentifier': currentStopIdentifier });
      };
      function attemptScan() {
        $window.screen.lockOrientation('portrait');
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $rootScope.BarcodesBelongsToStop = [];
        $state.go('app.scan.attempt', { 'stopIdentifier': currentStopIdentifier });
      };
      function placeholder() {
        $('#placeholder').hide();
      }
      $(':input').on("keydown",function(event){
         if (event.keyCode == 9) {
             //if clicked "NEXT" Btn
         }
         if (event.keyCode == 13) {
             //if clicked  i.e "GO" Btn
              cordova.plugins.Keyboard.close();
         }
      });

      function dropLocList() {
        if((CustomerID.length == 0) || (vm.CustomerConfig.length == 0)) {
          vm.CustomerConfig = [ 
            {
              Code: "LSR-PERSON",
              Label: "Person",
              SignatureRequired: true
            },
            {
              Code: "LSR-FRONT_DOOR",
              Label: "Front Door",
              SignatureRequired: false

            },
            {
              Code: "LSR-BACK_DOOR",
              Label: "Back Door",
              SignatureRequired: false

            },
             {
              Code: "LSR-DOOR_PERSON",
              Label: "Doorman",
              SignatureRequired: true

            },
            {
              Code: "LSR-MAILROOM",
              Label: "Mailroom",
              SignatureRequired: true
            },
            {
              Code: "LSR-RECEPTION",
              Label: "Reception",
              SignatureRequired: true
            },
             {
              Code: "LSR-DOCK",
              Label: "Shipping Dock",
              SignatureRequired: true
            },
            {
              Code: "LSR-SUPERINTENDENT",
              Label: "Building Superintendent",
              SignatureRequired: true
            },
            {
              Code: "LSR-GUARD",
              Label: "Guard",
              SignatureRequired: true
            },
             {
              Code: "LSR-MSG_CENTER",
              Label: "Message Center",
              SignatureRequired: true
            },
            {
              Code: "LSR-LEASING_OFFICE",
              Label: "Leasing Office",
              SignatureRequired: true
            },
            {
              Code: "LSR-NEIGHBOR",
              Label: "Neighbor",
              SignatureRequired: true
            },
             {
              Code: "LSR-SIDE_DOOR",
              Label: "Side Door",
              SignatureRequired: false
            },
            {
              Code: "LSR-LOBBY",
              Label: "Lobby",
              SignatureRequired: true
            },
            {
              Code: "LSR-DESK",
              Label: "Desk",
              SignatureRequired: true
            }

          ]
        };
        var popup = $ionicPopup.show({
          'template': '<div><ion-radio ng-model="vm.droplocation" ng-value="data" ng-repeat="data in vm.CustomerConfig">{{data.Label}}</ion-radio></div>',
          title: 'Drop Location List',
          scope: $scope,
          'buttons': [
            {
              'text': 'Cancel',
              'type': 'button-positive'
            },
            {
              'text': 'Ok',
              'type': 'button-positive',
              'onTap': function (event) {
                vm.location = vm.droplocation.Code;
                vm.droplocationvalue = vm.droplocation.Label;
                signatureRequired = vm.droplocation.SignatureRequired;
                $window.localStorage.droplocationcode = JSON.stringify(vm.location);
                $window.localStorage.droplocationvalue = JSON.stringify(vm.droplocationvalue);
                $window.localStorage.signatureRequired = JSON.stringify(signatureRequired);
              }
            }
          ]
        });
      }
      
      var signatureWKT;
      function complete() {
        var jsonStr = $('#canvasSection').signature('toJSON');
        jsonStr = jsonStr.replace('{"lines":[', '(');
        jsonStr = jsonStr.replace(']]]}', '))');
        while (jsonStr.indexOf('[[') > -1) {
          jsonStr = jsonStr.replace('[[', '(');
        }
        while (jsonStr.indexOf(']]') > -1) {
          jsonStr = jsonStr.replace(']]', '),');
        }
        while (jsonStr.indexOf(',') > -1) {
          jsonStr = jsonStr.replace(',', ' ');
        }
        while (jsonStr.indexOf('] [') > -1) {
          jsonStr = jsonStr.replace('] [', ',');
        }
        while (jsonStr.indexOf(')  (') > -1) {
          jsonStr = jsonStr.replace(')  (', '),(');
        }

        signatureWKT = jsonStr;
        if (jsonStr.length == 3 && SigCheckCount > 0) {
          if (SigCheckCount == _deliveryScanItems.length) {
            notificationService.alert('','', 'You have not captured a signature.', function () {

            })
          } else if (SigCheckCount < _deliveryScanItems.length) {
            notificationService.alert('','', 'Some piece(s) require a signature.', function () {

            })
          } else {            
          }
        }else if(SigCheckCount > 0 && vm.typedSignature == ""){
          notificationService.alert('Alert','', "Please enter a pod name", null);
        } else if(vm.droplocationvalue == ""){
            notificationService.alert('Alert','', "Please select the drop location", null);
        }else{
          if(vm.droplocationvalue != "" && signatureRequired == true){
            if (jsonStr.length == 3){
              notificationService.alert('','', 'Signature is required.', null)
            }else if(vm.typedSignature == ""){
              notificationService.alert('Alert','', "Please enter a pod name", null);
            }else{
              if(vm.questionnaire.length == 0){
                 eventTrigger();
               }else{
                //quest
                askQuestionnaire();
               }
             
            }
          }else{
            if(vm.questionnaire.length == 0){
                 eventTrigger();
               }else{
                //quest
                askQuestionnaire();
               }
          }        
        }
      }
      function attemptEventTrigger() {
        if((CustomerID.length == 0) || (vm.CustomerAttempt.length == 0)) {
          vm.CustomerAttempt = [
              {
                Code: "BCLD",
                Label: "Business Closed"
              },
             {
                Code: "CTRF",
                Label: "Recipient refused delivery"
              },
             {
                Code: "RFDM",
                Label: "Damaged"
              },
             {
                Code: "NDMI",
                Label: "Need More Information"
              },
              {
                Code: "NTHM",
                Label: "No one at delivery location"
              },
             {
                Code: "INAD",
                Label: "Incorrect Address"
              },
             {
                Code: "ACSS",
                Label: "Unable to gain access to deliver"
              },
             {
                Code: "MSPK",
                Label: "Carton Placed on Wrong Truck"
              },
             {
                Code: "OTBD",
                Label: "Mechanical breakdown, may impact delivery"
              },
             {
                Code: "ROOT",
                Label: "Unable to Complete before 5pm"
              },
             {
                Code: "WRDL",
                Label: "Delay due to weather or natural disaster"
              },
             {
                Code: "USFR",
                Label: "Unsafe for Release"
              },
             {
                Code: "RRDV",
                Label: "Customer requested re-delivery"
              },
             {
                Code: "UTLV",
                Label: "Unable to Leave"
              },
             {
                Code: "PUIA",
                Label: "Could not locate Pickup. Incorrect Address"
              },
             {
                Code: "PUKT",
                Label: "Pickup refused. Customer kept piece"
              },
             {
                Code: "PURU",
                Label: "Return unavailable for Pickup"
              }
          ]
        }
        var myPopup = $ionicPopup.show({
          'template': '<div class="list"><label class="item item-input item-select brdr_1px"><div class="input-label"></div><select class="maxwidth_100p" ng-model="vm.selectedName" ng-value="data.Label"><option value="" disabled selected>Please Select an Reason</option><option ng-repeat="data in vm.CustomerAttempt">{{data.Label}}</option></select></label></div>',
          title: 'Attempt Reasons',
          scope: $scope,
          'buttons': [
            {
              'text': 'Ok',
              'type': 'button-positive',
              'onTap': function (event) {
                if(vm.selectedName != undefined){
                  angular.forEach(vm.CustomerAttempt, function (piece, index) {
                    if (piece.Label == vm.selectedName) {
                      $rootScope.Reasonselected = piece.Code;
                    }
                  })
                  $scope.selectedReason();
                }else{
                  notificationService.alert('Alert','', "Please Select the Attempt Reason", null);
                  event.preventDefault();
                } 
              }
            },
            {
              'text': 'Cancel',
              'type': 'button-positive',
              'onTap': function(event) {
                
              }
            }
          ]
        });
        $scope.selectedReason = function () {
          myPopup.close();
          $ionicPopup.show({
            title: 'Door Tag',
            template: 'Do you want to scan a doortag?',
            scope: $scope,
            'buttons': [
            {
              'text': 'No',
              'type': 'button-positive',
              'onTap': function() {
                vm.doortag = '';
                var doorTag = vm.doortag;
                if (_pickupScanItems.length == 0 && _unassignPickupScanItems.length == 0 && $rootScope.addUnassignpickupItems.length == 0) {
                  notificationService.showTextSpinner('Processing Delivery');
                }
                packageStatusService.updateAttemptPackageStatus(_attemptScanItems).then(function(data){
                  if(_pickupScanItems.length > 0){
                    $ionicHistory.nextViewOptions({
                      disableBack: true
                    });
                    $state.go('app.pickupDetail');
                  }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
                    $ionicHistory.nextViewOptions({
                      disableBack: true
                    });
                    $state.go('app.unassignPickupDetail');
                  }else{
                    $ionicHistory.nextViewOptions({
                      disableBack: true
                    });
                    $state.go('app.manifest');
                  }
                },function(err){
                  $log.error('Error in updating package status',err);
                });
                if(_locationScanItems.length>0){
                  var count = 0;
                  for (var i=0; i<_attemptScanItems.length; i+=1) {      //chunk attempt events
                    var _chunkAttemptEvents = _attemptScanItems.slice(i,i+1);
                    count+=_chunkAttemptEvents.length;
                    eventsService.attemptBarcodesEventUpdate(_chunkAttemptEvents,doorTag,$rootScope.Reasonselected,_locationScanItems[0].barcode).then(function (data) {
                      if(_attemptScanItems.length == count ){
                        if(_pickupScanItems.length == 0 && _unassignPickupScanItems.length == 0 && $rootScope.addUnassignpickupItems.length == 0){
                          eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
                            if(deviceService.getConnectionStatus()){
                              eventsService.sendEvents();
                            }
                            scanFunctionService.clearRootScope();
                            rootScopeService.clearLocalStorage();
                            rootScopeService.clearDeliveryLocalVariables();
                          }, function (err) {
                            $log.info('Error in updating location event',err);
                          })
                        }
                      }
                    }, function (err) {
                      $log.info('Error in updating packages status',err);
                    })
                  }
                }else{
                  var count = 0;
                  for (var i=0; i<_attemptScanItems.length; i+=1) {      //chunk attempt events
                    var _chunkAttemptEvents = _attemptScanItems.slice(i,i+1);
                    count+=_chunkAttemptEvents.length;
                    eventsService.attemptBarcodesEventUpdate(_chunkAttemptEvents,doorTag,$rootScope.Reasonselected,"").then(function (data) {
                      if(_attemptScanItems.length == count ){
                        if(_pickupScanItems.length == 0 && _unassignPickupScanItems.length == 0 && $rootScope.addUnassignpickupItems.length == 0){
                          if(deviceService.getConnectionStatus()){
                            eventsService.sendEvents();
                          }
                          scanFunctionService.clearRootScope();
                          rootScopeService.clearLocalStorage();
                          rootScopeService.clearDeliveryLocalVariables();
                        }
                      }
                    }, function (err) {
                      $log.info('error in updating packages status',err);
                    })
                  }
                }
              }
            },
            {
              'text': 'Yes',
              'type': 'button-positive',
              'onTap': function() {
                $window.screen.lockOrientation('portrait');
                $state.go('app.scan.door',{'stopIdentifier': ''});
              }
            }
            ]
          });
        }
      }
      function eventTrigger() {
        if(vm.deliveryEventType.length != 0) { 
          EventActionValue = vm.deliveryEventType; 
        }
        if (_deliveryScanItems.length > 0) {
          if (_attemptScanItems.length == 0 && _pickupScanItems.length == 0 && _unassignPickupScanItems.length == 0 && $rootScope.addUnassignpickupItems.length == 0) {
            notificationService.showTextSpinner('Processing Delivery');
          }
          packageStatusService.updateDeliveredPackageStatus(_deliveryScanItems).then(function () {
            if (_attemptScanItems.length == 0 && _pickupScanItems.length == 0 && _unassignPickupScanItems.length == 0 && $rootScope.addUnassignpickupItems.length == 0) {
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go('app.manifest');
              $state.params.stopIdentifier = "";
            }
          }, function (err) {
            $log.info('error in updating status',err);
          })
          if (_locationScanItems.length > 0) {
            if (signatureWKT == '(]}') {
              var count = 0;
              for (var i=0; i<_deliveryScanItems.length; i+=1) {      //chunk delivery events
                var _chunkDeliveryEvents = _deliveryScanItems.slice(i,i+1);
                count+=_chunkDeliveryEvents.length;
                eventsService.deliveredEventUpdate(_chunkDeliveryEvents, vm.typedSignature, "", vm.location,EventActionValue,_doorTag).then(function(data){
                  if(_deliveryScanItems.length == count ){
                    $('#canvasSection').signature('clear');
                    if (_attemptScanItems.length > 0) {
                      _deliveryScanItems = [];
                      rootScopeService.saveDeliveryScanItems(_deliveryScanItems);
                      attemptEventTrigger();
                    }else if(_pickupScanItems.length > 0){
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.pickupDetail');
                    }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.unassignPickupDetail');
                    }else{
                      eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
                        if(deviceService.getConnectionStatus()){
                          eventsService.sendEvents();
                        }
                        scanFunctionService.clearRootScope();
                        rootScopeService.clearLocalStorage();
                        rootScopeService.clearDeliveryLocalVariables();
                      }, function (err) {
                        $log.warn('Error in updating location event',err);
                      })
                    }
                  }
                }, function (err) {
                  $log.warn('Error in updating delivered event',err);
                })
              }
            }else{
              var count = 0;
              for (var i=0; i<_deliveryScanItems.length; i+=1) {      //chunk delivery events
                var _chunkDeliveryEvents = _deliveryScanItems.slice(i,i+1);
                count+=_chunkDeliveryEvents.length;
                eventsService.deliveredEventUpdate(_chunkDeliveryEvents, vm.typedSignature, signatureWKT, vm.location,EventActionValue,_doorTag).then(function(data){
                  if(_deliveryScanItems.length == count ){
                    $('#canvasSection').signature('clear');
                    if (_attemptScanItems.length > 0) {
                      _deliveryScanItems = [];
                      rootScopeService.saveDeliveryScanItems(_deliveryScanItems);
                      attemptEventTrigger();
                    }else if(_pickupScanItems.length > 0){
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.pickupDetail');
                    }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.unassignPickupDetail');
                    }else{
                      eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
                        if(deviceService.getConnectionStatus()){
                          eventsService.sendEvents();
                        }
                        scanFunctionService.clearRootScope();
                        rootScopeService.clearLocalStorage();
                        rootScopeService.clearDeliveryLocalVariables();
                      }, function (err) {
                        $log.warn('Error in updating location event',err);
                      })
                    }
                  }
                }, function (err) {
                  $log.warn('Error in updating delivered event',err);
                })
              }
            }
          }else{
            if (signatureWKT == '(]}') {
              var count = 0;
              for (var i=0; i<_deliveryScanItems.length; i+=1) {      //chunk delivery events
                var _chunkDeliveryEvents = _deliveryScanItems.slice(i,i+1);
                count+=_chunkDeliveryEvents.length;
                eventsService.deliveredEventUpdate(_chunkDeliveryEvents, vm.typedSignature, "", vm.location,EventActionValue,_doorTag).then(function(data){
                  if(_deliveryScanItems.length == count ){
                    $('#canvasSection').signature('clear');
                    if (_attemptScanItems.length > 0) {
                      _deliveryScanItems = [];
                      rootScopeService.saveDeliveryScanItems(_deliveryScanItems);
                      attemptEventTrigger();
                    }else if(_pickupScanItems.length > 0){
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.pickupDetail');
                    }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.unassignPickupDetail');
                    }else{
                      if(deviceService.getConnectionStatus()){
                        eventsService.sendEvents();
                      }
                      scanFunctionService.clearRootScope();
                      rootScopeService.clearLocalStorage();
                      rootScopeService.clearDeliveryLocalVariables();
                    }
                  }
                }, function (err) {
                    $log.warn('Error in updating delivered event',err);
                })
              }
            }else{
              var count = 0;
              for (var i=0; i<_deliveryScanItems.length; i+=1) {      //chunk delivery events
                var _chunkDeliveryEvents = _deliveryScanItems.slice(i,i+1);
                count+=_chunkDeliveryEvents.length;
                eventsService.deliveredEventUpdate(_chunkDeliveryEvents, vm.typedSignature, signatureWKT, vm.location,EventActionValue,_doorTag).then(function(data){
                  if(_deliveryScanItems.length == count ){
                    $('#canvasSection').signature('clear');
                    if (_attemptScanItems.length > 0) {
                      _deliveryScanItems = [];
                      rootScopeService.saveDeliveryScanItems(_deliveryScanItems);
                      attemptEventTrigger();
                    }else if(_pickupScanItems.length > 0){
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.pickupDetail');
                    }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.unassignPickupDetail');
                    }else{
                      if(deviceService.getConnectionStatus()){
                        eventsService.sendEvents();
                      }
                      scanFunctionService.clearRootScope();
                      rootScopeService.clearLocalStorage();
                      rootScopeService.clearDeliveryLocalVariables();
                    }
                  }
                }, function (err) {
                    $log.warn('Error in updating delivered event',err);
                })
              }
            }
          }
        }else if (_attemptScanItems.length > 0) {
          _deliveryScanItems = [];
          rootScopeService.saveDeliveryScanItems(_deliveryScanItems);
          attemptEventTrigger();
        }else if(_pickupScanItems.length > 0){
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.pickupDetail');
        }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.unassignPickupDetail');
        }else if(_locationScanItems > 0){
          eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
            if(deviceService.getConnectionStatus()){
              eventsService.sendEvents();
            }
            scanFunctionService.clearRootScope();
            rootScopeService.clearLocalStorage();
            rootScopeService.clearDeliveryLocalVariables();
          },function(err){
            $log.warn("Error in updating location event",err);
          })
        }else{
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.manifest');
          if(deviceService.getConnectionStatus()){
            eventsService.sendEvents();
          }
          scanFunctionService.clearRootScope();
          rootScopeService.clearLocalStorage();
          rootScopeService.clearDeliveryLocalVariables();
        }
      }
    $('#podname').keyup(function(e)
        {
          if (this.value != this.value.replace(/[^a-zA-z0-9 .-]/g, '')) 
            { 
              this.value = this.value.replace(/[^a-zA-z0-9 .-]/g, ''); 
           }
      });  

      function askQuestionnaire(){
          if(vm.questionnaire.length != questionnaireCount){
            if(vm.questionnaire[questionnaireCount].Type == "YesNoAmount"){
              notificationService.confirm('', vm.questionnaire[questionnaireCount].Name, 'Yes', 'No',  
                function () {
                  vm.name.push(vm.questionnaire[questionnaireCount].Name);
                  vm.type.push(vm.questionnaire[questionnaireCount].Type);
                  vm.answer1.push('yes');
                  $scope.data = {}
                  cordova.plugins.Keyboard.disableScroll(true)
                  var questionnairePopup = $ionicPopup.show({
                    template: '<div class="numberValidation"><input type="number" ng-model="data.model"  id="amountid" autofocus="autofocus" placeholder="Enter Amount" class="numberValidation" style="height: 45px;font-size: 17px;"/></div>',
                    scope: $scope,
                    buttons: [
                    {
                      text: '<b>Save</b>',
                      type: 'button-positive',
                      onTap: function(e) {                      
                        cordova.plugins.Keyboard.disableScroll(false)
                        if ($scope.data.model == null) {                       
                          e.preventDefault();
                          notificationService.alert('Alert','', "Please enter a valid amount", null);
                        } else {                       
                          vm.answer2.push($scope.data.model);
                          questionnaireCount++;
                          askQuestionnaire();
                        }
                      }
                    }
                    ]
                  });
                },function () {
                  vm.answer1.push('no');
                  $scope.data = {}
                  cordova.plugins.Keyboard.disableScroll(false) 
                  var questionnairePopup = $ionicPopup.show({
                    template: '<div class="numberValidation" ><input   type="number" ng-model="data.model"  id="amountid2" autofocus="autofocus" placeholder="Enter Amount" class="numberValidation" style="height: 45px;font-size: 17px;"/></div>',
                    scope: $scope,
                    buttons: [
                      {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function(e) {                       
                          cordova.plugins.Keyboard.disableScroll(true)
                          if ($scope.data.model == null) {                         
                            e.preventDefault();
                            notificationService.alert('Alert','', "Please enter a valid amount", null);
                          } else {                          
                            vm.answer2.push($scope.data.model);
                            questionnaireCount++;
                            askQuestionnaire();
                          }
                        }
                      }
                    ]
                  });   
                }
              )
          }else{
            vm.name.push(vm.questionnaire[questionnaireCount].Name);
            vm.type.push(vm.questionnaire[questionnaireCount].Type);
            cordova.plugins.Keyboard.disableScroll(true)
            var questionnairePopup = $ionicPopup.show({
                  template: '<div class="numberValidation"><input   type="number" ng-model="data.model"  id="amountid3" autofocus="autofocus" placeholder="Enter Amount" class="numberValidation" style="height: 45px;font-size: 17px;"/></div>',
                  scope: $scope,
                  buttons: [
                    {
                      text: '<b>Save</b>',
                      type: 'button-positive',
                      onTap: function(e) {                     
                        cordova.plugins.Keyboard.disableScroll(false)
                        if ($scope.data.model == null) {                         
                          e.preventDefault();
                          notificationService.alert('Alert','', "Please enter a valid amount", null);
                        } else {
                          vm.answer1.push($scope.data.model);
                           vm.answer2.push("");
                          questionnaireCount++;
                          askQuestionnaire();
                        }
                      }
                    }
                  ]
                }); 
          }
      }else{
          for(var i=0; i<vm.name.length; i++){
            var data1= vm.name[i]+'|'+ vm.type[i]+'|'+vm.answer1[i]+'|'+vm.answer2[i];
            vm.deliveryEventType.push(data1);
          }
          vm.deliveryEventType.join('||');
        eventTrigger();
      }
      }  

       $(':input, #amountid, #amountid2, #amountid3').on("keydown",function(event){
         if (event.keyCode == 13) {
             //if clicked  "GO" Btn hide softkeyboard
              cordova.plugins.Keyboard.close();
         }
      });

      $('#amountid, #amountid2, #amountid3').keyup(function(e){
          if (this.value != this.value.replace(/[^0-9\.]/g, '')) { 
              this.value = this.value.replace(/[^0-9\.]/g, ''); 
          }
      });
      
      function doorTagScan(){
        $rootScope.deliveryEvent = true;
         $ionicHistory.nextViewOptions({
            disableBack: true
          });
         $window.screen.lockOrientation('portrait');
         $state.params.stopIdentifier = "";
         $state.go('app.scan.door',{'stopIdentifier': ''});
      } 

      $scope.getWindowOrientation = function () {
        return $window.orientation;
      };
      
      $scope.$watch($scope.getWindowOrientation, function (newValue, oldValue) {
        $scope.degrees = newValue;
        if(newValue == 90){ //Landscape
          $('#canvasSection').signature();
          if($window.localStorage.signature != undefined){
            $('#placeholder').hide();
            $('#canvasSection').signature('draw', JSON.parse($window.localStorage.signature));
          }
        }
        else if(newValue == 0){  //Portrait
          $('#canvasSection').signature();
          if($window.localStorage.signature != undefined){
            $('#placeholder').hide();
            $('#canvasSection').signature('draw', JSON.parse($window.localStorage.signature));
          }
        }
      }, true);
    }
  })();