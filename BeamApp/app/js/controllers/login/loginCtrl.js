(function () {
  'use strict';

  angular
    .module('beam')
    .controller('loginController', loginController);

  loginController.$inject = ['$scope', '$stateParams', '$state', 'AuthSerenityService', 'notificationService', 'jsonStoreService',
    'driverService', 'manifestService', 'customerService', '$q', 'deviceService', '$window', 'dataSyncService', '$log', 'rootScopeService', '$ionicHistory', 'AppVersion','$timeout','logoutService'];

  function loginController($scope, $stateParams, $state, AuthSerenityService, notificationService, jsonStoreService,
    driverService, manifestService, customerService, $q, deviceService, $window, dataSyncService, $log, rootScopeService, $ionicHistory, AppVersion, $timeout,logoutService) {

    var vm = this;
    vm.signIn = signIn;
    vm.driverId = '';
    vm.password = '';
    var _bgTimer=null;

    $scope.$on('$ionicView.beforeEnter', function () {
      vm.driverId = '';
      vm.password = '';
      vm.appVersion = AppVersion;
    });
    $scope.$on("$ionicView.afterEnter", function () {
      notificationService.hideLoad();
    });

    function signIn() {
      if (validateInputs(vm.driverId, vm.password)) {
        rootScopeService.clearLocalStorageVariables();
        rootScopeService.clearLoadedLocalVariables();
        rootScopeService.clearDriverProfile();
        $window.WL.UserInfo.USER_ID = vm.driverId;
        if (deviceService.getConnectionStatus()) {
          notificationService.showLoad('Logging in ....');
          processOnlineAuth(vm.driverId, vm.password);
        }
        else {
          processOfflineAuth(vm.driverId, vm.password);
        }
      } else {
        $log.info("invalid login and password");
        notificationService.alert('Login', '', "Please enter valid login and password", null);
      }
    }

    function processOnlineAuth(driverId, password) {
      AuthSerenityService.login(vm.driverId, vm.password).then(function (user) {
        initLocalStore(driverId).then(function () {
          notificationService.hideLoad();
          checkPreviousSession(driverId).then(function (exists) {
            if (exists) {
              driverService.updateStoredDriverInfo(driverId);
              notificationService.confirm('Session', 'Resume session or Start New', 'Resume', 'New', function () {
                notificationService.showTextSpinner('Loading manifest');
                jsonStoreService.clearCustomerCollection().then(function () {
                  manifestService.getCustomerIds().then(function (data) {
                    customerService.loadCustomerConfig(jQuery.unique(data));
                    notificationService.hideLoad();
                  }, function (error) {
                    $log.error("Error in getting customer configuration", error);
                    notificationService.hideLoad();
                  });
                  gotoManifest();
                });
              }, function () {
                rootScopeService.clearLocalStorageVariables();
                rootScopeService.clearLoadedLocalVariables();
                jsonStoreService.clearLoadedItems();
                jsonStoreService.clearCollsforNewSession().then(function () {
                  notificationService.showTextSpinner('Loading manifest');
                  refreshManifestAndContinue();
                });
              });
            }
            else {
              notificationService.showTextSpinner('Loading manifest');
              refreshManifestAndContinue();
              driverService.saveDriverInfoToStore();
            }
            eventSyncr();
          });
          startLocationPoll(user.GPSInterval);
          deviceService.startLocationWatcher();
        }, function (err) {
          notificationService.hideLoad();
          $log.warn("Error Setting Up JSON Store", err);
          notificationService.alert('Error', '', 'Error Setting Up JSON Store. Try again', function () {
            vm.driverId = '';
            vm.password = '';
            driverService.clearDriverInfo();
          });
          $log.warn('Err JSON Store Setup', err);
        });
      }, function (error) {
        notificationService.hideLoad();
        notificationService.alert('Login Error', '', error, function () {
          vm.driverId = '';
          vm.password = '';
        });
      });
    }

    function processOfflineAuth(driverId, password) {
      //TODO Identify Defaults

      initLocalStore(driverId).then(function () {
        checkPreviousSession(driverId).then(function (exists) {
          if (exists) {
            driverService.findDriverInfo(driverId).then(function (driver) {
              var driverObject = driver[0].json;
              driverObject.LastLogin = Date();
              driverObject.OfflineAuth = true;
              $window.localStorage["beamCredentials"] = JSON.stringify(driverObject);
              driverService.updateStoredDriverInfo(driverId);
              startLocationPoll(driverObject.GPSInterval);
              eventSyncr();
              gotoManifest();
            });
          }
          else {
            var driverObject = {
              'driverId': driverId,
              'password': 'lasership', //save TODO
              'BarcodeMaxLength': 45,
              'FacilityCode': 'DULL',
              'GPSInterval': '300',
              'Photo': '',
              'Token': '',
              'DriverName': driverId,
              'LastLogin': Date(),
              'Containers': [{"Code":"Box","Label":"Box"},{"Code":"Tube","Label":"Tube"},{"Code":"Pak","Label":"Pak"},{"Code":"Envelope","Label":"Envelope"},{"Code":"CustomPackaging","Label":"Custom Packaging"},{"Code":"Tote","Label":"Tote"},{"Code":"Cooler","Label":"Cooler"},{"Code":"Case","Label":"Case"}],
              'OfflineAuth': true
            };
            $window.localStorage["beamCredentials"] = JSON.stringify(driverObject);
            driverService.saveDriverInfoToStore();
            startLocationPoll(driverObject.GPSInterval);
            eventSyncr();
            deviceService.startLocationWatcher();
            gotoManifest();
          }
        });
      }, function (err) {
        notificationService.alert('Error', '', 'Error Setting Up JSON Store. Try again', function () {
          vm.driverId = '';
          vm.password = '';
          driverService.clearDriverInfo();
        });
      });
    }

    function initLocalStore(driverId) {
      var def = $q.defer();
      jsonStoreService.initStore(driverId).then(function (res) {
        def.resolve();
      }, function (err) {
        $log.info("initializing local Storage", err);
        def.reject(err);
      });
      return def.promise;
    }

    function checkPreviousSession(driverId) {
      var def = $q.defer();
      driverService.findDriverInfo(driverId).then(function (res) {
        def.resolve(res.length > 0);
      }, function (err) {
        $log.info("Checking Previous session", err);
        def.resolve(false);
      });
      return def.promise;
    }

    function refreshManifestAndContinue() {
      manifestService.refreshManifestInfo().then(function (data) {
        notificationService.showLoad('Updating Manifest');
        gotoManifest();
        manifestService.getCustomerIds().then(function (data) {
          customerService.loadCustomerConfig(jQuery.unique(data));
          notificationService.hideLoad();
        }, function (error) {
          $log.error("Error in getting customer configuration", error);
          notificationService.hideLoad();
        });
      }, function (err) {
        $log.info('Error loading manifest', err);
        notificationService.hideLoad();
        gotoManifest();
      });
    }

    function gotoManifest() {
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('app.manifest');
    }

    function startLocationPoll(time) {
      dataSyncService.locationPoll(time);
      if ($window.cordova.plugins.backgroundMode) {
        $window.cordova.plugins.backgroundMode.setDefaults({
          text: "Tap to return to Eli App.",
          ticker: "Eli is running. Tap to return.",
          color: "#ba0009",
          icon: "icon"
        });
        $window.cordova.plugins.backgroundMode.onactivate = function () { //console.debug('BG Mode Enabled'); 
          startBGTimer();
        };
        $window.cordova.plugins.backgroundMode.enable();
        $window.cordova.plugins.backgroundMode.ondeactivate = function () { //console.debug('BG Mode Disabled'); 
          cancelBGTimer();
        };
      }
    }

    function eventSyncr() {
      dataSyncService.startEventSyncr();
      dataSyncService.registerSync();
    }

    $('#paswrd').on("keydown", function (event) {
      if (event.keyCode == 13) {
        //if clicked "GO" Btn
        cordova.plugins.Keyboard.close();
        signIn();
      }
    });

    function validateInputs(driverId, password) {
      if (driverId !== "" && password !== "") {
        var Exp = /^[a-zA-Z0-9]+$/;
        if ((driverId.length > 1 && driverId.length <= 32) && (password.length > 1 && password.length <= 32) && (Exp.test(driverId))) {
          return true;
        } else {
          vm.driverId = '';
          vm.password = '';
          return false;
        }
      }
    }

    function startBGTimer(){
      cancelBGTimer();
      _bgTimer = $timeout(function(){
        $log.error('Logging out from app due to inactivity');
        logoutService.processLogout();
      },7200000); 
    }

    function cancelBGTimer(){
      if(_bgTimer)
        $timeout.cancel(_bgTimer);
      _bgTimer = null;
    }
  }
})();