(function () {
  'use strict';

  angular
    .module('beam')
    .controller('preferenceCtrl', preferenceCtrl);

  preferenceCtrl.$inject = ['$scope', 'driverService', '$window', 'deviceService', '$cordovaGeolocation', 'notificationService', '$interval','$rootScope','$log','eventsService'];

  function preferenceCtrl($scope, driverService, $window, deviceService, $cordovaGeolocation, notificationService, $interval,$rootScope,$log,eventsService) {
    var vm = this;
    vm.save = save;
    vm.unassignedPrompts = $rootScope.isChecked;
    $scope.$on('$ionicView.beforeEnter', function () {
      getLocationStatus();
    })
    $scope.$on("$ionicView.enter", function () {
      $interval(getLocationStatus, 2000);
      $scope.$watch(function () {
        return vm.locationStatus;
      }, function (data) {
        if (vm.locationStatus === false) {
          getLocationStatus();
        }
      });
    });
    $scope.$on("$ionicView.afterEnter", function () {
      notificationService.hideLoad();
    });
    function getLocationStatus() {
      deviceService.getLocation().then(function (data) {
        if (data.default)
          vm.locationStatus = false;
        else
          vm.locationStatus = true;
      }, function (err) {
          $log.info(" error in getting location status");
          vm.locationStatus = false;
      });
    }
    function save(view) {
      var routing = { 'path': view };
      $window.localStorage["preferenceView"] = JSON.stringify(routing);
      notificationService.alert('','', "Changes saved successfully", null);
    }
  }
})();