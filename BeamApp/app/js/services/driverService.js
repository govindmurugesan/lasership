(function () {
    'use strict';

    angular
        .module('beam')
        .factory('driverService', driverService);

    driverService.$inject = ['$window', 'jsonStoreService', '$log'];
    function driverService($window, jsonStoreService, $log) {
        var service = {
            getDriverInfo: getDriverInfo,
            saveDriverInfo: saveDriverInfo,
            clearDriverInfo: clearDriverInfo,
            findDriverInfo: findDriverInfo,
            saveDriverInfoToStore: saveDriverInfoToStore,
            updateStoredDriverInfo: updateStoredDriverInfo,
            removeStoredDriverInfo: removeStoredDriverInfo
        };

        return service;


        function getDriverInfo() {
            if (!$window.localStorage["beamCredentials"])
                throw new Error('Driver info not available.')
            else
                return JSON.parse($window.localStorage["beamCredentials"])
        }

        function saveDriverInfo(obj) {

            var driverObject = {
                'driverId': obj.currentDriverId,
                'password': 'lasership',
                'BarcodeMaxLength': obj.BarcodeMaxLength,
                'FacilityCode': obj.FacilityCode,
                'GPSInterval': obj.GPSInterval,
                'Photo': obj.Photo,
                'Token': obj.Token,
                'DriverName': obj.DriverName,
                'LastLogin': Date(),
                'Containers': obj.Containers,
                'OfflineAuth':false
            };

            $window.localStorage["beamCredentials"] = JSON.stringify(driverObject);
        }

        function clearDriverInfo() {
            $window.localStorage.removeItem("beamCredentials");
        }

        function findDriverInfo(driverId) {
            var store = jsonStoreService.getDriverCollection();
            return store.find({ driverId: driverId }, { exact: true, limit: 1 })
        }
        
        function saveDriverInfoToStore() {
            var drivColl = jsonStoreService.getDriverCollection();
            var driver = getDriverInfo();
            if (driver) {
                drivColl.add(driver,{markDirty: false}).then(function (data) {
                }, function (err) {
                    $log.error('Err Saving Driver Info', err);
                });
            }            
        }

        function updateStoredDriverInfo(driverId) {
            var drivColl = jsonStoreService.getDriverCollection();
            var driverObj = getDriverInfo();
            if (driverObj) {
                findDriverInfo(driverId).then(function(driver){
                    if(driver.length>0)
                    drivColl.replace({_id:driver[0]._id,json:driverObj}).then(function(){
                    });
                });
            }            
        }

        function removeStoredDriverInfo(driverId) {
            var store = jsonStoreService.getDriverCollection();
            return store.remove({ driverId: driverId }, { exact: true, limit: 1 });           
        }
    }
})();