(function() {
'use strict';

    angular
        .module('beam')
        .factory('scanService', scanService);

    scanService.$inject = ['$window','ScanditKey','driverService'];
    function scanService($window,ScanditKey,driverService) {
        var service = {
            initScanner:initScanner,
            showScanner : showScanner,
            hideScanner : hideScanner,
            pauseScanner : pauseScanner,
            resumeScanner : resumeScanner
        };
        
        return service;
        var settings = null,
        picker = null;
        var scannerStatus = false;
        
        ////////////////
        function initScanner() {
            var driverInfo = driverService.getDriverInfo();
            var countlength = driverInfo.BarcodeMaxLength - 7;
            var initialCount = 6
            var Counts = [];
            Counts.push[initialCount];
            for(var i=0; i <= countlength; i++){
                Counts.push(initialCount +=1);
            }
            Scandit.License.setAppKey(ScanditKey);   //Professional
            settings = new Scandit.ScanSettings();
            settings.codeDuplicateFilter = -1;
            settings.codeCachingDuration = 5000;
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.EAN13, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.UPC12, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.EAN8, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.CODE39, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.CODE128, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.QR, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.EAN8, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.UPCE, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.CODE11, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.ITF, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.DATA_MATRIX, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.PDF417, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.CODE25, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.MSI_PLESSEY, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.GS1_DATABAR, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.GS1_DATABAR_LIMITED, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.GS1_DATABAR_EXPANDED, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.CODABAR, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.CODE93, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.AZTEC, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.MAXICODE, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.FIVE_DIGIT_ADD_ON, true);
            settings.setSymbologyEnabled(Scandit.Barcode.Symbology.TWO_DIGIT_ADD_ON, true);          
            var symbolCounts = Counts;
            settings.getSymbologySettings(Scandit.Barcode.Symbology.CODE128).activeSymbolCounts = symbolCounts;
            settings.activeScanningAreaPortrait = new Scandit.Rect(0, 0.5, 1, 0.01);
            picker = new Scandit.BarcodePicker(settings);
            picker.continuousMode = true;
            picker.getOverlayView().setGuiStyle(Scandit.ScanOverlay.GuiStyle.LASER);
            picker.getOverlayView().setViewfinderColor('FF0000');
            picker.getOverlayView().setViewfinderDecodedColor('FF0000');
            var constraints = new Scandit.Constraints();
            constraints.topMargin = (ionic.Platform.isAndroid()?'45':'65');
            constraints.leftMargin = '1';
            constraints.rightMargin = '1';
            constraints.height = "30%";
            constraints.width = "100%";
            picker.setConstraints(constraints, constraints, 0);
            return true;
         }
         function showScanner(successFn, errorFn){
            if(scannerStatus){
                hideScanner();
                errorFn('Error enabling scanner. Try again');
            }else{
                scannerStatus = true;
                if(!picker && !settings)
                    initScanner();
                picker.show(successFn, null, errorFn);
                picker.startScanning();
                return true;
            }
         }
         function hideScanner(){
            scannerStatus = false;
           picker.cancel();
           picker.stopScanning();
         }
         function pauseScanner() {
            picker.pauseScanning();
            return true;
          }
         function resumeScanner() {
            picker.resumeScanning();
          }   
    }
})();