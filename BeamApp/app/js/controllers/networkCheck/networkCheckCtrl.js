(function () {
    'use strict';

    angular
        .module('beam')
        .controller('networkCheckCtrl', networkCheckCtrl);

    networkCheckCtrl.$inject = ['$scope', '$rootScope', 'eventsService', 'customerService', '$cordovaNetwork', 'deviceService', '$cordovaDevice', '$state', 'deviceEventsService', 'notificationService', '$log', 'AppVersion', '$http','SyncStats','dataSyncService'];

    function networkCheckCtrl($scope, $rootScope, eventsService, customerService, $cordovaNetwork, deviceService, $cordovaDevice, $state, deviceEventsService, notificationService, $log, AppVersion, $http,SyncStats,dataSyncService) {
        var vm = this;
        vm.appVersion = AppVersion;
        vm.platform = device.platform;
        vm.version = device.version;
        vm.cordova = device.cordova;
        vm.model = device.model;
        vm.uuid = deviceService.getUUID();
        vm.model = device.model;
        vm.network = $cordovaNetwork.getNetwork();
        vm.conStatus = "";
        vm.refreshPendingEvents = refreshPendingEvents;
        vm.deletePendingEvents = deletePendingEvents;
        vm.deleteIcon = false;
        vm.sendJsonDump = sendJsonDump;
        vm.syncStats = SyncStats;
        vm.sendEvents = sendEvents;


        $scope.$on('$ionicView.enter', function () {
            var pendingEvents = eventsService.getUpdatedEventCount();
            pendingEvents.then(function (pendingEventsCount) {
                vm.eventsCount = pendingEventsCount;
                if (pendingEventsCount > 0) {
                    vm.deleteIcon = true;
                }
            }, function (err) {
                $log.error('Error in getting pending events count', err);
            });
            var pendingEventsD = eventsService.getUpdatedEvent();
            pendingEventsD.then(function(peCountD){
                vm.eventsCountD = peCountD.length;
            },function(err){
                $log.error('Error Getting Events Data Count',err);
            });
            deviceEventsService.sendPing();
            var ip = deviceService.getIpAddress();
            ip.then(function (data) {
                vm.ipadd = data;
            }, function (err) {
                $log.error('Error in getting IP address', err);
            });
            var location = deviceService.getLocation();
            location.then(function (loc) {
                vm.latitude = loc.lat;
                vm.longitude = loc.long;
            }, function (err) {
                $log.error('Error in getting location info', err);
            });
            if (deviceService.getConnectionStatus()) {
                vm.conStatus = "Online";
            }
            else {
                vm.conStatus = "Offline";
            }
            var mfpServer = JSON.parse(window.localStorage.mfpConnection?window.localStorage.mfpConnection:'{}');
            if (mfpServer.mfpConnectionStatus)
                vm.mfpStatus = "Online";
            else
                vm.mfpStatus = "Offline";
        });

        $scope.$on("$ionicView.afterEnter", function () {
            notificationService.hideLoad();
        });

        function refreshPendingEvents() {
            vm.syncStats = SyncStats;
            var pendingEvents = eventsService.getUpdatedEventCount();
            pendingEvents.then(function (pendingEventsCount) {
                vm.eventsCount = pendingEventsCount;
                if (pendingEventsCount > 0) {
                    vm.deleteIcon = true;
                } else {
                    vm.deleteIcon = false;
                }
            }, function (err) {
                $log.info('Error in getting pending events count', err);
            });
        }
        function deletePendingEvents() {
            notificationService.confirm('', 'Do you want to clean all the Pending Log events?', 'Yes', 'No', function (event) {
                $(event.toElement).attr('disabled', true);
                var events = eventsService.getUpdatedEvent();
                events.then(function (logEvents) {
                    for (var i = 0; i < logEvents.length; i++) {
                        if (logEvents[i].json.Events[0].EventModifier == 'LLOG') {
                            eventsService.removeEvent(logEvents[i]._id);
                        }
                    }
                    refreshPendingEvents();
                }, function (err) {
                    $log.info('Error in getting LLOG events', err);
                });
            }, function (event) {
                $(event.toElement).attr('disabled', true);
            })
        }

        function sendJsonDump() {
            if (!deviceService.getConnectionStatus())
                notificationService.alert('', '', "Can't send, Device is offline", null);
            else {
                var dumpEvents = eventsService.getUpdatedEvent();
                var dumpURL = "https://ngevent.lasership.com/dump_post.php";
                dumpEvents.then(function (jsonDumpEvents) {
                    if (jsonDumpEvents.length > 0) {
                        notificationService.showTextSpinner('Sending Dump Events');
                        $http.post(dumpURL, jsonDumpEvents).then(function (response) {
                            // This function handles success
                            notificationService.hideLoad();
                            if(!SyncStats.directSyncInProgress){
                                notificationService.showTextSpinner('Sending Done. Removing JSON Events from Store');
                                eventsService.removeAllEvents().then(function(res){
                                    notificationService.hideLoad();
                                    notificationService.alert('', '', "JSON Events Dumped & Cleared", null);
                                    refreshPendingEvents();
                                },function(err){
                                    notificationService.hideLoad();
                                    $log.error('Error removing all events',err);
                                });
                            }
                            else{
                                notificationService.alert('', '', "JSON Events Dumped", null);
                            }
                            refreshPendingEvents();
                        }, function (error) {
                            // this function handles error
                            if (error.status == -1) {
                                notificationService.hideLoad();
                                notificationService.alert('', '', "Timed-out error, Try again...", null);
                            } else {
                                notificationService.hideLoad();
                                notificationService.alert('', '', error.statusText + "Try again...", null);
                            }
                        });
                    } else {
                        notificationService.alert('', '', "No dump events to send", null);
                    }
                }, function (err) {
                    $log.info('Error in getting dump events', err);
                });
            }

        }

        function sendEvents(){
            if (!deviceService.getConnectionStatus())
                notificationService.alert('', '', "Can't send, Device is offline", null);
            else{
                var events = eventsService.getUpdatedEvent();
                events.then(function (allEvents) {
                    if(allEvents.length > 0)
                        eventsService.sendEvents();
                    else
                        notificationService.alert('', '', "No events to send", null);
                    dataSyncService.resetSyncr();
                }, function (err) {
                    $log.error('Error in getting events', err);
                });
            }
        }

    }
})();