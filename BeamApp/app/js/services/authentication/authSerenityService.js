(function () {
    'use strict';

    angular
        .module('beam')
        .factory('AuthSerenityService', AuthSerenityService);

    AuthSerenityService.$inject = ['$window', '$http', 'deviceService', '$q', 'driverService', 'jsonStoreService', 'AppVersion', '$log', 'SerenityURL'];
    function AuthSerenityService($window, $http, deviceService, $q, driverService, jsonStoreService, AppVersion, $log, SerenityURL) {
        var service = {
            login: login,
            loginStatus: status,
            logOut: logOut,
        };
        return service;

        function getSerenityString(data, eventModifier) {
            //Parse the data object and build the string below
            var curDat = new Date().toISOString();
            var req = '/v10/InfoRequest/';
            req += eventModifier;
            req += '/Beam/';
            req += data.DeviceInfo.AppVersion + '/';
            req += data.DriverInfo.FacilityID + '/';
            req += data.DriverInfo.DriverID + '/';
            req += data.EventISODateTime + '/';
            req += data.LocationInfo.Longitude + '/';
            req += data.LocationInfo.Latitude + '/';
            req += data.LocationInfo.GPSFixDateTime + '/';
            req += data.LocationInfo.GPSPrecision + '/';
            req += data.SecurityGuid + '/';
            req += data.DeviceInfo.DeviceID + '/';
            req += data.DeviceInfo.PhoneIp + '/';
            req += data.DeviceInfo.PhoneNum + '/';
            req += data.DriverInfo.DriverID;
            req += '///////////';
            return req;
        }

        function login(driverId, password) {
            if (status()) {
                driverService.clearDriverInfo();
            }
            var def = $q.defer();
            var _date = new Date();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'InfoRequest',
                'eventModifier': 'LOGN',
                'appversion': AppVersion,
                'driverId': driverId,//'06243',
                'FacilityCode': driverId
            };
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([deviceService.getLocation(),
            deviceService.getIpAddress(),
            deviceService.getPhoneNum(),
            deviceService.getUUID()])
                .then(function (data) {
                    var _pos = data[0];
                    var _ip = data[1];
                    var _phoneNum = data[2];
                    var uuid = data[3];
                    var req = {
                        "DriverInfo": {
                            "DriverID": guidRequest.driverId,
                            "FacilityID": guidRequest.FacilityCode,
                            "Password": password//"19857977"
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "LocationInfo": {
                            "Longitude": _pos.long,
                            "Latitude": _pos.lat,
                            "GPSPrecision": "1",
                            "GPSFixDateTime": _pos.timeStamp.toMyISOString()
                        },
                        "SecurityGuid": md,
                        "EventISODateTime": guidRequest.eventDate.toMyISOString()
                    };
                    
                    var request = {
                        method: 'POST',
                        url: SerenityURL + getSerenityString(req, 'LOGN'),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: 'Password=' + req.DriverInfo.Password,
                        config: { timeout: 4000 }
                    };
                    try {
                        $http(request).then(
                            function (response) {
                                if (response.status == 200) {
                                    if (response.data.ErrorMessage && response.data.Error)
                                        def.reject(response.data.ErrorMessage);
                                    else {
                                        response.data.currentDriverId = driverId;
                                        driverService.saveDriverInfo(response.data);
                                        def.resolve(response.data);
                                    }
                                }
                                else {
                                    def.reject('Login Failed. Try Again');
                                }

                            },
                            function (error) {
                                def.reject(error);
                            }
                        );
                    } catch (error) {
                        def.reject(error);
                    }

                }, function (err) {
                    console.log('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }

        function logOut() {
            var def = $q.defer();
            var _date = new Date();
            var driverInfo = driverService.getDriverInfo();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'InfoRequest',
                'eventModifier': 'LOGT',
                'appversion': AppVersion,
                'driverId': driverInfo.driverId,
                'facilityId': driverInfo.FacilityCode,
            }
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([deviceService.getLocation(),
            deviceService.getIpAddress(),
            deviceService.getPhoneNum(),
            deviceService.getUUID()])
                .then(function (data) {
                    var _pos = data[0];
                    var _ip = data[1];
                    var _phoneNum = data[2];
                    var uuid = data[3];
                    var req = {
                        "DriverInfo": {
                            "DriverID": driverInfo.driverId,
                            "FacilityID": driverInfo.FacilityCode,
                            "Password": "19857977"
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "LocationInfo": {
                            "Longitude": _pos.long,
                            "Latitude": _pos.lat,
                            "GPSPrecision": "1",
                            "GPSFixDateTime": _pos.timeStamp.toMyISOString()
                        },
                        "SecurityGuid": md,
                        "EventISODateTime": guidRequest.eventDate.toMyISOString()
                    };
                    var request = {
                        method: 'GET',
                        url: SerenityURL + getSerenityString(req, 'LOGT'),
                        config: { timeout: 2000 }
                    };
                    try {
                        $http(request).then(
                            function (response) {
                                if (response.status == 200) {
                                    if (response.data.ErrorMessage && response.data.Error)
                                        def.reject(response.data.ErrorMessage);
                                    else {
                                        def.resolve(response.data);
                                    }
                                }
                                else {
                                    def.reject('Logout Failed. Try Again');
                                }
                                jsonStoreService.closeStore();
                            },
                            function (error) {
                                def.reject(error);
                                jsonStoreService.closeStore();
                            }
                        );
                    } catch (error) {
                        $log.error('Error calling adapter', error)
                    }

                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }

        function status() {
            return $window.localStorage.beamCredentials !== undefined;

        }
    }

})();
