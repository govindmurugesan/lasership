angular
.module('beam')
.service('thumbWheelService', thumbWheelService);

function thumbWheelService() {
    this.thumbWheelServiceCtr = thumbWheelServiceCtr;
    function thumbWheelServiceCtr ($scope){ 
        $scope.onSwipeFirstButton = function () { 
            $('.swipeCont,.leftClick,.rightClick').removeClass('second third fourth fifth sixth seventh').addClass('first');
        };
        $scope.onSwipeSecondButton = function () {
            $('.swipeCont').removeClass('first third fourth fifth sixth seventh').addClass('second');
        };
        $scope.onSwipethirdButton = function () {
           $('.swipeCont').removeClass('first second fourth fifth sixth seventh').addClass('third');
        };
        $scope.onSwipeFourthButton = function () {
           $('.swipeCont').removeClass('first second third fifth sixth seventh').addClass('fourth');
        };
        $scope.onSwipeFifthButton = function () {
            $('.swipeCont').removeClass('first second third fourth sixth seventh').addClass('fifth');
        };
        $scope.onSwipeSixthButton = function () {
           $('.swipeCont').removeClass('first second third fifth fourth seventh').addClass('sixth');
        };
        $scope.onSwipeSeventhButton = function () {
            $('.swipeCont').removeClass('first second third fifth fourth sixth').addClass('seventh');
        };
        
        $scope.onSwipeRightFirstButton = function () {
            $('.swipeCont').removeClass('first second third fourth fifth sixth').addClass('seventh');
        };
        $scope.onSwipeLeftFirstButton = function () {
            $('.swipeCont').removeClass('first third fourth fifth sixth seventh').addClass('second');
        };
        $scope.onSwipeRightSecondButton = function () {
            $('.swipeCont').removeClass('second third fourth fifth sixth seventh').addClass('first');
        };
        $scope.onSwipeLeftSecondButton = function () {
            $('.swipeCont').removeClass('first second fourth fifth sixth seventh').addClass('third');
        };
        $scope.onSwipeRightThirdButton = function () {
            $('.swipeCont').removeClass('first third fourth fifth sixth seventh').addClass('second');
        };
        $scope.onSwipeLeftThirdButton = function () {
            $('.swipeCont').removeClass('first second third fifth sixth seventh').addClass('fourth');
        };
        $scope.onSwipeRightFourthButton = function () {
            $('.swipeCont').removeClass('first second fourth fifth sixth seventh').addClass('third');
        };
        $scope.onSwipeLeftFourthButton = function () {
            $('.swipeCont').removeClass('first second third fourth sixth seventh').addClass('fifth');
        };
        $scope.onSwipeRightFifthButton = function () {
            $('.swipeCont').removeClass('first second third fifth sixth seventh').addClass('fourth');
        };
        $scope.onSwipeLeftFifthButton = function () {
            $('.swipeCont').removeClass('first second third fourth fifth seventh').addClass('sixth');
        };
        $scope.onSwipeRightSixthButton = function () {
            $('.swipeCont').removeClass('first second third fourth sixth seventh').addClass('fifth');
        };
        $scope.onSwipeLeftSixthButton = function () {
            $('.swipeCont').removeClass('first second third fourth fifth sixth').addClass('seventh');
        };
        $scope.onSwipeRightSeventhButton = function () {
            $('.swipeCont').removeClass('first second third fourth fifth seventh').addClass('sixth');
        };
        $scope.onSwipeLeftSeventhButton = function () {
            $('.swipeCont').removeClass('second third fourth fifth sixth seventh').addClass('first');
        };
    }
}