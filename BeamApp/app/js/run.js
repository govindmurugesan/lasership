(function () {
    'use strict';

    angular
        .module('beam')
        .run(runBlock);

    runBlock.$inject = ['$ionicPlatform', 'MFPClientPromise', '$cordovaNetwork', 'MapsAPIKey', 'jsonStoreService', 'dataSyncService', '$rootScope', '$window','$log'];
    function runBlock($ionicPlatform, MFPClientPromise, $cordovaNetwork, MapsAPIKey, jsonStoreService, dataSyncService, $rootScope, $window, $log) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            MFPClientPromise.then(function () {
                WL.Logger.ctx({ pkg: 'BeamApp' }).debug('mfp and ionic are ready, safe to use WL.* APIs');
                WL.Logger.config({autoSendLogs: false,level:'error'});
                $rootScope.scannedBarcodeItems = [];
                $rootScope.unassignedScanItems = [];
                $rootScope.addUnassignpickupItems = [];
                $rootScope.isChecked = false;
                $rootScope.onRoute = false;
                $rootScope.prePreviousURL = '';
                $rootScope.BarcodesBelongsToStop = [];
                $rootScope.unCheckedScanItems = [];
                $rootScope.deliveryEvent = false;
                jsonStoreService.closeStore();
            });
            if ($cordovaNetwork.isOnline()) {
                loadGoogleMaps();
            }
            else {
                $window.addEventListener('online', loadGoogleMaps, false);
            }
            $window.screen.lockOrientation('portrait');
            ionic.Platform.isFullScreen = true;
            if(ionic.Platform.isAndroid()){
                $ionicPlatform.registerBackButtonAction(function(e){
                    e.preventDefault();
                },100);
            }
        });

        function loadGoogleMaps() {
            $.getScript('https://maps.googleapis.com/maps/api/js?key=' + MapsAPIKey + '&sensor=true&callback=onMapsApiLoaded&libraries=geometry,places');
            $window.removeEventListener('offline', loadGoogleMaps, false);
        }
    }


})();