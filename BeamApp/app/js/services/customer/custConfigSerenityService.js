(function () {
    'use strict';

    angular
        .module('beam')
        .factory('custConfigSerenityService', custConfigSerenityService);

    custConfigSerenityService.$inject = ['$window', 'deviceService', '$q', 'driverService','$log','AppVersion','SerenityURL','$http'];
    function custConfigSerenityService($window, deviceService, $q ,driverService, $log,AppVersion,SerenityURL,$http) {
        var service = {
            getCustConfig: getCustConfig
        };

        return service;

        function getCustConfig(custId) {
            var driverInfo = driverService.getDriverInfo();
            var def = $q.defer();
            var _date = new Date();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'InfoRequest',
                'eventModifier': 'CRCF',
                'appversion': AppVersion,
                'driverId': driverInfo.driverId,
                'facilityId': driverInfo.FacilityCode,
            }
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([deviceService.getLocation(),
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID()])
                .then(function (data) {
                    var _pos = data[0];
                    var _ip = data[1];
                    var _phoneNum = data[2];
                    var uuid = data[3];
                    var custConfigRequest = new WLResourceRequest(
                        "/adapters/CustomerConfig/getCustomerConfig",
                        WLResourceRequest.POST
                    );
                    var req = {
                        "DriverInfo": {
                            "DriverID": driverInfo.driverId,
                            "FacilityID": driverInfo.FacilityCode,
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "LocationInfo": {
                            "Longitude": _pos.long,
                            "Latitude": _pos.lat,
                            "GPSPrecision": "1",
                            "GPSFixDateTime": _pos.timeStamp.toMyISOString()
                        },
                        "SecurityGuid": md,
                        "EventISODateTime": guidRequest.eventDate.toMyISOString(),
                        "CustomerId": custId
                    };
                    
                    try {
                        var reqdata = getCustomerConfigString(req,'CRCF');
                        var serenityURL = SerenityURL +reqdata;
                        $http.get(serenityURL,{timeout:2000}).then(function (success) {
                            def.resolve(success.data);
                        }, function (error) {
                            def.reject(error);
                        });
                    } catch (error) {
                        $log.error('Error in calling CRCF Serenity', error);
                        def.reject(error);
                    }
                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }
    }
    function getCustomerConfigString(data, eventModifier) {
        //Parse the data object and build the string below
        var curDat = new Date().toISOString();
        var req = '/v10/InfoRequest/';
        req += eventModifier;
        req += '/Beam/';
        req += data.DeviceInfo.AppVersion+'/';
        req += data.DriverInfo.FacilityID+'/';
        req += data.DriverInfo.DriverID+'/';
        req += data.EventISODateTime+'/';
        req += data.LocationInfo.Longitude+'/';
        req += data.LocationInfo.Latitude+'/';
        req += data.LocationInfo.GPSFixDateTime+'/';
        req += data.LocationInfo.GPSPrecision+'/';
        req += data.SecurityGuid+'/';
        req += data.DeviceInfo.DeviceID+'/';
        req += data.DeviceInfo.PhoneIp+'/';
        req += data.DeviceInfo.PhoneNum+'/';
        req += data.DriverInfo.DriverID;
        req += '////';
        req += data.CustomerId;
        req += '///////';
        return req;
    }
})();