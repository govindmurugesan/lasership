(function () {
  'use strict';

  angular
    .module('beam')
    .controller('loadReportCtrl', loadReportCtrl);

  loadReportCtrl.$inject = ['$scope', '$state', '$ionicHistory', 'notificationService', 'packageStatusService', 'manifestService', '$stateParams', 'eventsService', '$rootScope', 'scanFunctionService','$q','$log','$window','rootScopeService','deviceService','jsonStoreService'];

  function loadReportCtrl($scope, $state, $ionicHistory, notificationService, packageStatusService, manifestService, $stateParams, eventsService, $rootScope, scanFunctionService,$q,$log,$window,rootScopeService,deviceService,jsonStoreService) {
    var vm = this;
    vm.loadedBarcodesBind = [];
    var _allBarCodes = [];
    var _loadScanItems = [];
    var _unassignLoadScanItems = [];
    var _locationScanItems = [];
    var _scannedLoadItems = [];
    vm.loadScan = loadScan;
    vm.checkOut = checkOut;
    vm.getBarcodeStatus = getBarcodeStatus;    
    vm.packagesInCustody;
    var departedLoadEvents = [];
    var departedUnassignLoadEvents = [];

    $scope.$on('$ionicView.beforeEnter', function () {
      angular.forEach(rootScopeService.getLoadItems(), function (piece, index) {
        _loadScanItems.push(piece);
      });
      angular.forEach(rootScopeService.getUnasignLoadItems(), function (piece, index) {
        _unassignLoadScanItems.push(piece);
      });

      angular.forEach(rootScopeService.getScannedLoadItems(), function (piece, index) {
        _allBarCodes.push(piece.barcode);
        _scannedLoadItems.push(piece);
        packageStatusService.getPackageByBarcode(piece.barcode).then(function(packagedata){
          if(!(packagedata.length > 0 )){
            var assignLoad = $.grep(_loadScanItems, function (e) {
                return piece.barcode === e.barcode;
            });
            if (assignLoad.length === 0) {
              _loadScanItems.push(piece);
            }
          }
        })
      });
      angular.forEach(rootScopeService.getScannedUnassignLoadItems(), function (piece, index) {
        _allBarCodes.push(piece.barcode);
        _scannedLoadItems.push(piece);
        packageStatusService.getPackageByBarcode(piece.barcode).then(function(packagedata){
          if(!(packagedata.length > 0 )){
            var unassignLoad = $.grep(_unassignLoadScanItems, function (e) {
                return piece.barcode === e.barcode;
            });
            if (unassignLoad.length === 0) {
              _unassignLoadScanItems.push(piece);
            }
          }
        })
      });

      _locationScanItems = rootScopeService.getLocationScanItems();
    });
    $scope.$on('$ionicView.enter', function () {
      vm.loadedBarcodesBind = _allBarCodes;
      var notScannedPackages = 0;
      manifestService.getManifestDeliveryCount().then(
        function (data) {
          vm.packagesInCustody = data;
          var notScannedItem;
          if(data>0){
            vm.scannedPackages = _scannedLoadItems.length;
            packageStatusService.getLoadPackages().then(
              function (loaddata) {
                if(loaddata > 0){
                  if(vm.packagesInCustody == loaddata){
                    vm.notScannedPackages = 0;
                  }else if(vm.packagesInCustody > loaddata){
                    notScannedPackages = vm.packagesInCustody - (loaddata + _loadScanItems.length + _unassignLoadScanItems.length);
                    if(notScannedPackages > 0){
                      vm.notScannedPackages = notScannedPackages;
                    }else{
                      vm.notScannedPackages = 0;
                    }
                  }else{
                    vm.notScannedPackages = 0;
                  }
                }else{
                  notScannedPackages =  vm.packagesInCustody - (_scannedLoadItems.length);
                  if(notScannedPackages > 0){
                    vm.notScannedPackages = notScannedPackages;
                  }else{
                    vm.notScannedPackages = 0;
                  }
                }
              },
              function (err) {
                $log.error('Error in getting package count', err);
              })
          }else{
            vm.scannedPackages = _loadScanItems.length + _unassignLoadScanItems.length; 
            vm.notScannedPackages = '0';
          }

        },
        function (err) {
          $log.error('Error in getting PackagesInCustody count', err);
        }
      ); 
      vm.loadedItemCount = _allBarCodes.length;
      packageStatusService.getNotAssignedPackageCount().then(
        function (data) {
          vm.notAssignedPackage = data + _unassignLoadScanItems.length;           
        },
        function (err) {
          $log.error('Error getting delivered package count', err);
        }
      );
    });
    $scope.$on("$ionicView.afterEnter", function () {
      notificationService.hideLoad();
    });

    function loadScan() {
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('app.scan.load',{'stopIdentifier': ''}); 
    }
    
    function checkOut(){
      if(_locationScanItems.length>0){
         eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
          if(deviceService.getConnectionStatus()){
            eventsService.sendEvents();
          }
         },function(err){
            $log.warn("Error in location barcodes eventupdate",err);
         });
      }

      notificationService.confirm('', 'Are you ready to be “Out for Delivery”?', 'Yes', 'No', 
        function () {
          $rootScope.onRoute = true;
          notificationService.showTextSpinner('Processing Departure');
          if(_unassignLoadScanItems.length > 0){
            packageStatusService.updateUnassignLoadPackageStatus(_unassignLoadScanItems);
          }
          if(_loadScanItems.length > 0){
            packageStatusService.loadPackageInfo(_loadScanItems);
          }
          jsonStoreService.clearLoadedItems();
          $q.all([ 
            departedEvent(),
          ]).then(function(data){
            if(departedLoadEvents.length>0){
              if(_locationScanItems.length>0){
                var count = 0;
                for (var i=0; i<departedLoadEvents.length; i+=1) {      //chunk load events
                  var _chunkLoadEvents = departedLoadEvents.slice(i,i+1);
                  count+=_chunkLoadEvents.length;
                  eventsService.loadEventUpdate(_chunkLoadEvents,_locationScanItems[0].barcode, "Departed").then(function(data){
                  if(departedLoadEvents.length == count ){
                    if(deviceService.getConnectionStatus()){
                      eventsService.sendEvents();
                    }
                    scanFunctionService.clearRootScope();
                    rootScopeService.clearLocalStorage();
                    departedLoadEvents = [];
                    departedUnassignLoadEvents = [];
                    $window.localStorage.saveScannedLoadItems = [];
                    $window.localStorage.unassignLoadItems = [];
                    $ionicHistory.nextViewOptions({
                      disableBack: true
                    });
                    $state.go('app.manifest');
                   }
                  },function(err){
                    $log.warn("Error in load event update",err);
                   });
                }
                if(departedUnassignLoadEvents.length>0){         //chunk Unassign load events
                  var count = 0;
                  for (var i=0; i<departedUnassignLoadEvents.length; i+=1) {
                    var _chunkUnassignLoadEvents = departedUnassignLoadEvents.slice(i,i+1); 
                    count+=_chunkUnassignLoadEvents.length;
                    eventsService.loadEventUpdate(_chunkUnassignLoadEvents, _locationScanItems[0].barcode, "Departed").then(function(data){
                    if(departedUnassignLoadEvents.length == count){
                      if(deviceService.getConnectionStatus()){
                        eventsService.sendEvents();
                      }
                      scanFunctionService.clearRootScope();
                      rootScopeService.clearLocalStorage();
                      departedLoadEvents = [];
                      departedUnassignLoadEvents = [];
                      $window.localStorage.saveScannedLoadItems = [];
                      $window.localStorage.unassignLoadItems = [];
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.manifest');
                     }
                    },function(err){
                        $log.warn("Error in Unassign load event update",err);
                     });
                  }
                 }
              }else{
                var count=0;
                if(departedUnassignLoadEvents.length>0){
                    for(var i=0; i<departedUnassignLoadEvents.length; i+=1) {    //chunk Unassign load events
                      var _chunkUnassignLoadEvents = departedUnassignLoadEvents.slice(i,i+1);
                      count+=_chunkUnassignLoadEvents.length;
                      eventsService.loadEventUpdate(_chunkUnassignLoadEvents, "", "Departed").then(function(data){
                      if(departedUnassignLoadEvents.length == count){
                        if(deviceService.getConnectionStatus()){
                          eventsService.sendEvents();
                        }
                        scanFunctionService.clearRootScope();
                        rootScopeService.clearLocalStorage();
                        departedLoadEvents = [];
                        departedUnassignLoadEvents = [];
                        $window.localStorage.saveScannedLoadItems = [];
                        $window.localStorage.unassignLoadItems = [];
                        $ionicHistory.nextViewOptions({
                          disableBack: true
                        });
                        $state.go('app.manifest');
                       }
                      },function(err){
                          $log.warn("Error in unassign load event update",err);
                      }); 
                    }
                    var loadcount=0;
                    for(var i=0; i<departedLoadEvents.length; i+=1) {    //chunk  load events
                      var _chunkLoadEvents = departedLoadEvents.slice(i,i+1);
                      loadcount+=_chunkLoadEvents.length;
                      eventsService.loadEventUpdate(_chunkLoadEvents,"", "Departed").then(function(data){
                        if(departedLoadEvents.length == loadcount ){
                          if(deviceService.getConnectionStatus()){
                            eventsService.sendEvents();
                          }
                          scanFunctionService.clearRootScope();
                          rootScopeService.clearLocalStorage();
                          departedLoadEvents = [];
                          departedUnassignLoadEvents = [];
                          $window.localStorage.saveScannedLoadItems = [];
                          $window.localStorage.unassignLoadItems = [];
                          $ionicHistory.nextViewOptions({
                            disableBack: true
                          });
                          $state.go('app.manifest');
                        }
                    },function(err){
                      $log.warn("Error in load event update",err);
                   });
                  }
                }else{
                  var count=0;
                   for(var i=0; i<departedLoadEvents.length; i+=1) {    //chunk  load events
                      var _chunkLoadEvents = departedLoadEvents.slice(i,i+1);
                      count+=_chunkLoadEvents.length;
                      eventsService.loadEventUpdate(_chunkLoadEvents,"", "Departed").then(function(data){
                       if(departedLoadEvents.length == count ){
                        if(deviceService.getConnectionStatus()){
                          eventsService.sendEvents();
                        }
                        scanFunctionService.clearRootScope();
                        rootScopeService.clearLocalStorage();
                        departedLoadEvents = [];
                        departedUnassignLoadEvents = [];
                        $window.localStorage.saveScannedLoadItems = [];
                        $window.localStorage.unassignLoadItems = [];
                        $ionicHistory.nextViewOptions({
                          disableBack: true
                        });
                        $state.go('app.manifest');
                       }
                     },function(err){
                      $log.warn("Error in load event update",err);
                     });
                  }
                }
              }
          }else{
            var count=0;
            if(_locationScanItems.length>0){
                for (var i=0; i<departedUnassignLoadEvents.length; i+=1) {   //chunk Unassign load events
                  var _chunkUnassignLoadEvents = departedUnassignLoadEvents.slice(i,i+1);
                  count+=_chunkUnassignLoadEvents.length;
                  eventsService.loadEventUpdate(_chunkUnassignLoadEvents, _locationScanItems[0].barcode, "Departed").then(function(data){
                    if(departedUnassignLoadEvents.length == count ){
                      if(deviceService.getConnectionStatus()){
                        eventsService.sendEvents();
                      }
                      scanFunctionService.clearRootScope();
                      rootScopeService.clearLocalStorage();
                      departedLoadEvents = [];
                      departedUnassignLoadEvents = [];
                      $window.localStorage.saveScannedLoadItems = [];
                      $window.localStorage.unassignLoadItems = [];
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.manifest');
                    }
                   },function(err){
                      $log.warn("Error in unassign load event update",err);
                   }); 
                }
              }else{
                  var count=0;
                  for (var i=0; i<departedUnassignLoadEvents.length; i+=1) {   //chunk Unassign load events
                    var _chunkUnassignLoadEvents = departedUnassignLoadEvents.slice(i,i+1);
                    count+=_chunkUnassignLoadEvents.length;
                    eventsService.loadEventUpdate(_chunkUnassignLoadEvents, '', "Departed").then(function(data){
                    if(departedUnassignLoadEvents.length == count ){
                      if(deviceService.getConnectionStatus()){
                        eventsService.sendEvents();
                      }
                      scanFunctionService.clearRootScope();
                      rootScopeService.clearLocalStorage();
                      departedLoadEvents = [];
                      departedUnassignLoadEvents = [];
                      $window.localStorage.saveScannedLoadItems = [];
                      $window.localStorage.unassignLoadItems = [];
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.manifest');
                    }
                   },function(err){
                      $log.warn("Error in unassign load event update",err);
                   });
                }
              }
            }
          },function(error){
            $log.warn("Error in updating events");
          }) 
         }, function () {
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.loadreport');
        }); 
    }
    function getBarcodeStatus(barcode) {
      var status = true;
      angular.forEach(_unassignLoadScanItems, function (piece, index) {
        if(piece.barcode == barcode){
          status = false;
        }
      });
      return status;
    };
    function departedEvent(){
      var departedTime = new Date();
      angular.forEach(rootScopeService.getScannedLoadItems(), function (piece, index) {
        departedLoadEvents.push({
          barcode:piece.barcode,
          scannedTime:departedTime
        })
      });
      angular.forEach(rootScopeService.getScannedUnassignLoadItems(), function (piece, index) {
        departedUnassignLoadEvents.push({
          barcode:piece.barcode,
          scannedTime:departedTime
        })
      });
    }
  }
})();