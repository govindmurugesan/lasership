(function () {
    'use strict';

    angular
        .module('beam')
        .factory('deviceEventsService', deviceEventsService);

    deviceEventsService.$inject = ['deviceService', 'eventsService', 'driverService', '$q', 'eventsAdapterService', '$log','AppVersion','eventsSerenityService'];
    function deviceEventsService(deviceService, eventsService, driverService, $q, eventsAdapterService, $log,AppVersion,eventsSerenityService) {
        var service = {
            sendLocation: sendLocation,
            sendLog: sendLog,
            sendPing: sendPing
        };

        return service;

        function sendLocation() {
            var driverInfo = driverService.getDriverInfo();
            var def = $q.defer();
            var _date = new Date();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'Miscellaneous',
                'eventModifier': 'LGPS',
                'appversion': AppVersion,
                'driverId': driverInfo.driverId,
                'facilityId': driverInfo.FacilityCode,
            };
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID(),
                deviceService.getAccurateLocation()]).then(
                function (data) {
                    var _ip = data[0];
                    var _phoneNum = data[1];
                    var uuid = data[2];
                    var loc = data[3];
                    var event = {
                        "DriverInfo": {
                            "DriverID": driverInfo.driverId,
                            "FacilityID": driverInfo.FacilityCode,
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "Events": [{
                            "_eventID": 0,
                            "_eventDateTime": _date,
                            "BarCode": '',
                            "PieceKey": '',
                            "CustomerID": '',
                            "EventType": "Miscellaneous",
                            "EventModifier": "LGPS",
                            "EventISODateTime": _date.toMyISOString(),
                            "Location": '',
                            "DoorTag": "",
                            "TypedNameSignature": "",
                            "SignatureWKT": "",
                            "GroupingGuid": "",
                            "LocationInfo": {
                                "Longitude": loc.long,
                                "Latitude": loc.lat,
                                "GPSPrecision": "1",
                                "GPSFixDateTime": loc.timeStamp.toMyISOString()
                            },
                            "SecurityGuid": md
                        }]
                    };
                    sendDataToAdapter(event);

                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }

        function sendLog(logMsg, logTime, logType) {
            var driverInfo = driverService.getDriverInfo();
            var def = $q.defer();
            var guidRequest = {
                'eventDate': logTime,
                'eventType': 'Miscellaneous',
                'eventModifier': 'LLOG',
                'appversion': AppVersion,
                'driverId': driverInfo.driverId,
                'facilityId': driverInfo.FacilityCode,
            };
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID(),
                deviceService.getLocation()]).then(
                function (data) {
                    var _ip = data[0];
                    var _phoneNum = data[1];
                    var uuid = data[2];
                    var loc = data[3];
                    var event = {
                        "DriverInfo": {
                            "DriverID": driverInfo.driverId,
                            "FacilityID": driverInfo.FacilityCode,
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "Events": [{
                            "_eventID": 0,
                            "_eventDateTime": logTime,
                            "BarCode": '',
                            "PieceKey": '',
                            "CustomerID": '',
                            "EventType": "Miscellaneous",
                            "EventModifier": "LLOG",
                            "EventISODateTime": logTime.toMyISOString(),
                            "Location": '',
                            "DoorTag": "",
                            "TypedNameSignature": "",
                            "SignatureWKT": "",
                            "GroupingGuid": "",
                            "LocationInfo": {
                                "Longitude": loc.long,
                                "Latitude": loc.lat,
                                "GPSPrecision": "1",
                                "GPSFixDateTime": loc.timeStamp.toMyISOString()
                            },
                            "EventActionValue": 'Beam|'+ logType + '|Script|'+encodeURI(logMsg.slice(0,200)),
                            "SecurityGuid": md
                        }]
                    };
                    sendDataToAdapter(event);

                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }

        function sendPing() {
            var driverInfo = driverService.getDriverInfo();
            var def = $q.defer();
            var _date = new Date();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'Miscellaneous',
                'eventModifier': 'PING',
                'appversion': AppVersion,
                'driverId': driverInfo.driverId,
                'facilityId': driverInfo.FacilityCode,
            };
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID(),
                deviceService.getLocation()]).then(
                function (data) {
                    var _ip = data[0];
                    var _phoneNum = data[1];
                    var uuid = data[2];
                    var loc = data[3];
                    var event = {
                        "DriverInfo": {
                            "DriverID": driverInfo.driverId,
                            "FacilityID": driverInfo.FacilityCode,
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "Events": [{
                            "_eventID": 0,
                            "_eventDateTime": _date,
                            "BarCode": '',
                            "PieceKey": '',
                            "CustomerID": '',
                            "EventType": "Miscellaneous",
                            "EventModifier": "PING",
                            "EventISODateTime": _date.toMyISOString(),
                            "Location": '',
                            "DoorTag": "",
                            "TypedNameSignature": "",
                            "SignatureWKT": "",
                            "GroupingGuid": "",
                            "LocationInfo": {
                                "Longitude": loc.long,
                                "Latitude": loc.lat,
                                "GPSPrecision": "1",
                                "GPSFixDateTime": loc.timeStamp.toMyISOString()
                            },
                            "SecurityGuid": md
                        }]
                    };
                    sendDataToAdapter(event);

                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }

        function sendDataToAdapter(event) {
            var sendFormat = {json:event};
            if (deviceService.getConnectionStatus()) {
                var mfpServer = JSON.parse(window.localStorage.mfpConnection?window.localStorage.mfpConnection:'{}');
                if (mfpServer.mfpConnectionStatus && event.Events[0].EventModifier!=='LLOG'){
                    eventsAdapterService.sendDataToAdapter(sendFormat).then(
                        function (result) {
                            
                        },
                        function (err) {
                            $log.info('Err sending device event, saving event', err);
                        }
                    );
                }
                else{
                    //$log.error('Device Online but MFP Server Disconnected. Status @' + new Date(mfpServer.mfpConnectionUpdatedTime));
                }
                eventsSerenityService.sendDataToSerenity(sendFormat).then(
                    function (result) {
                        if (!result || result==[] || (result[0] && result[0]._errSerenitySend && result[0]._errSerenitySend===true))
                            eventsService.eventsUpdate(event);
                    }, function (err) {
                        $log.error('Err Syncing Events', err);
                        eventsService.eventsUpdate(event);
                    }
                );
            }
            else
                eventsService.eventsUpdate(event);
        }
    }
})();