(function () {
    'use strict';

    angular
        .module('beam')
        .factory('deviceService', deviceService);

    deviceService.$inject = ['$window', '$cordovaDevice', '$cordovaGeolocation', '$cordovaNetwork', '$q', 'APIKey', 'AppVersion', '$log', '$http', '$timeout'];
    function deviceService($window, $cordovaDevice, $cordovaGeolocation, $cordovaNetwork, $q, APIKey, AppVersion, $log, $http, $timeout) {
        var _defaultData = {
            'Phone': '',
            'Location': { lat: '', long: '', timeStamp: new Date() },
            'Ip': '',
            'default': true
        };
        var _locWatcher;
        var service = {
            getPhoneNum: getPhoneNum,
            getLocation: getRecentLocation,
            getIpAddress: getIpAddress,
            getConnectionStatus: getConnectionStatus,
            getUUID: getUUID,
            getSecurityGuid: getSecurityGuid,
            startLocationWatcher: startLocationWatcher,
            clearLocationWatcher: clearLocationWatcher,
            getAccurateLocation: getLocation
        };

        return service;

        function getPhoneNum() {
            var def = $q.defer();
            if (!($window.plugins && $window.plugins.sim))
                def.resolve('');
            else
                $window.plugins.sim
                    .requestReadPermission()
                    .then(function () {
                        $.when($window.plugins.sim
                            .getSimInfo())
                            .then(function (deviceInfo) {
                                def.resolve(deviceInfo.getPhoneNum ? deviceInfo.getPhoneNum : _defaultData.Phone);
                            },
                            function (err) {
                                //report error to logs
                                $log.debug('Error getting Phone #', err);
                                def.resolve('');
                            });
                    }).
                    error(function (err) {
                        $log.debug('Get Phone Num Err', err);
                        def.resolve(_defaultData.Phone);
                    });
            return def.promise;

        }

        function getLocation() {
            var posOptions = { timeout: 15000, enableHighAccuracy: false, maximumAge: 30000 };
            var def = $q.defer();
            if ($window.ionic.Platform.isIOS()) {
                $cordovaGeolocation
                    .getCurrentPosition(posOptions)
                    .then(function (position) {
                        var data = {
                            lat: position.coords.latitude,
                            long: position.coords.longitude,
                            timeStamp: new Date(position.timestamp),
                            accuracy: 0
                        };
                        //console.debug('New Location Acquired: LGPS', data);
                        $window.localStorage.recentLocation = JSON.stringify(data);
                        def.resolve(data);

                    }, function (err) {
                        // error
                        //$log.debug("Error getting location. Going with defaults", err);
                        def.resolve(_defaultData.Location);
                    });
            }
            else {
                $window.cordova.plugins.diagnostic.isLocationEnabled(function (enabled) {
                    if (enabled)
                        $cordovaGeolocation
                            .getCurrentPosition(posOptions)
                            .then(function (position) {
                                var data = {
                                    lat: position.coords.latitude,
                                    long: position.coords.longitude,
                                    timeStamp: new Date(position.timestamp)
                                };
                                //console.debug('New Location Acquired: LGPS', data);
                                $window.localStorage.recentLocation = JSON.stringify(data);
                                def.resolve(data);
                            }, function (err) {
                                // error
                                //$log.debug("Error getting location. Going with defaults", err);
                                def.resolve(_defaultData.Location);
                            });
                    else
                        def.resolve(_defaultData.Location);
                }, function (error) {
                    $log.debug("The following error occurred: " + error);
                });
            }

            return def.promise;
        }

        function getIpAddress() {
            var def = $q.defer();
            if (!getConnectionStatus())
                def.resolve('');
            else
                $window.networkinterface
                    .getIPAddress(
                    function (ip) {
                        return def.resolve(ip);
                    },
                    function (err) {
                        //report error to logs
                        if ($window.localStorage.lastKnowIp) {
                            var lastIp = JSON.parse($window.localStorage.lastKnowIp);
                            var timeDiff = (new Date().getTime() - new Date(lastIp.time).getTime());
                            var seconds = Math.floor((timeDiff) / (1000));
                            if (seconds < 300) {
                                def.resolve(lastIp.address);
                            }
                            else {
                                $http.get('https://ngevent.lasership.com/my_ip.php?hardwareid=' + getUUID(), { timeout: 2000 }).then(function (ip) {
                                    $window.localStorage.lastKnowIp = JSON.stringify({ address: ip.data, time: new Date() });
                                    def.resolve(ip.data);
                                },
                                    function (err) {
                                        $log.debug('Error getting IP #', err);
                                        def.resolve('');
                                    });
                            }
                        }
                        else {
                            $http.get('https://ngevent.lasership.com/my_ip.php?hardwareid=' + getUUID(), { timeout: 2000 }).then(function (ip) {
                                $window.localStorage.lastKnowIp = JSON.stringify({ address: ip.data, time: new Date() });
                                def.resolve(ip.data);
                            },
                                function (err) {
                                    $log.debug('Error getting IP #', err);
                                    def.resolve('');
                            });
                        }
                    });
            return def.promise;
        }

        function getConnectionStatus() {
            return $cordovaNetwork.isOnline();
        }
        function getUUID() {
            return $cordovaDevice.getUUID();
        }

        function getSecurityGuid(data) {
            var pkey = APIKey;
            var _date = data.eventDate ? data.eventDate : data._eventDateTime;
            var _isoDate = _date.toMyISOString().split('T')[0];
            var _isoDateTime = _date.toMyISOString();
            var user = $window.localStorage['beamCredentials'] ? JSON.parse($window.localStorage['beamCredentials']) : { Token: '' };
            var hashString = _isoDate;
            hashString += data.eventType ? data.eventType : '';
            hashString += data.eventModifier;
            hashString += 'Beam';
            hashString += AppVersion;
            hashString += user.driverId ? user.driverId : data.driverId;
            hashString += user.FacilityCode ? user.FacilityCode : data.FacilityCode;
            hashString += _isoDateTime;
            hashString += user.Token;
            var md = $window.CryptoJS.MD5(pkey + hashString).toString();
            return md;
        }

        function startLocationWatcher() {
            var watchOptions = {
                timeout: 30000, //5 minutes max before location timesout
                enableHighAccuracy: false, // may cause errors if true,
                maximumAge: 60000 //max time of a cached result
            };
            if ($window.ionic.Platform.isIOS()) {
                if (_locWatcher)
                    $timeout.cancel(_locWatcher);
                _locWatcher = $timeout(function () {
                    getLocation().then(function (data) {
                        //console.log('iOS Location Updated', data);
                    });
                }, 60000);
            }
            else {
                $window.cordova.plugins.diagnostic.isLocationEnabled(function (enabled) {
                    if (enabled) {
                        _locWatcher = $cordovaGeolocation.watchPosition(watchOptions);
                        _locWatcher.then(
                            null,
                            function (err) {
                                // error
                                $log.debug('Error acquiring location', err);
                                $window.localStorage.removeItem('recentLocation');
                            },
                            function (position) {
                                $window.cordova.plugins.diagnostic.isLocationEnabled(function (enabledStill) {
                                    if (enabledStill) {
                                        var data = {
                                            lat: position.coords.latitude,
                                            long: position.coords.longitude,
                                            timeStamp: new Date(position.timestamp),
                                            accuracy: 0
                                        };
                                        $window.localStorage.recentLocation = JSON.stringify(data);
                                        //console.info('Updated Device Location', data);
                                    }
                                    else {
                                        $log.debug('Location Access Disabled. Loc Status ', enabledStill);
                                        $window.localStorage.removeItem('recentLocation');
                                    }
                                });

                            });
                    }
                    else {
                        // error
                        $log.debug('Error acquiring location. GPS Status ', enabled);
                        clearLocationWatcher();
                        $timeout(function () {
                            startLocationWatcher();
                            //console.info('Trying location acquire again');
                        }, 30000);
                    }

                }, function (err) {
                    $log.debug('Location Permission Issue', err);
                });
            }


        }

        function clearLocationWatcher() {
            if (_locWatcher)
                if ($window.ionic.Platform.isIOS())
                    $timeout.cancel(_locWatcher);
                else
                    _locWatcher.clearWatch();
            $window.localStorage.removeItem('recentLocation');
        }

        function getRecentLocation() {
             
            if ($window.localStorage.recentLocation) {
                var recentLocation = JSON.parse($window.localStorage.recentLocation);
                recentLocation.timeStamp = new Date(recentLocation.timeStamp);
                var currentTime = new Date();
                var diff = currentTime.getTime() - recentLocation.timeStamp.getTime();
                var seconds = Math.floor((diff) / (1000));
                if (seconds > 120) {
                    //console.info('Older GPS Value found. Updating to newer value. Value older by', seconds);
                    return getLocation();
                }
                else{
                    var def = $q.defer();
                    def.resolve(recentLocation);
                    return def.promise;
                }
            }
            else
                return getLocation();
        }
    }
})();