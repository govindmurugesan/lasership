(function () {
    'use strict';

    angular
        .module('beam')
        .factory('customerService', customerService);

    customerService.$inject = ['$window', '$q', 'custConfigSerenityService', 'jsonStoreService', '$log'];
    
    function customerService($window,  $q, custConfigSerenityService, jsonStoreService, $log) {
		var service = {
		 	getCustomerConfig:   getCustomerConfig,
            loadCustomerConfig: loadCustomerConfig,
            getCustomerConfigByID:getCustomerConfigByID  ,
            getCustomerAttemptByID : getCustomerAttemptByID,
            getQuestionnaire : getQuestionnaire

		};

        return service;

        function loadCustomerConfig(customerIds){
        	var customerCollection = jsonStoreService.getCustomerCollection();
            customerIds.forEach(function(custId){
                custConfigSerenityService.getCustConfig(custId).then(function(customerConfig){
                    customerConfig.customerId = custId;
                    customerCollection.add(customerConfig,{markDirty: false});
                },function(err){
                    $log.error("Error in getting customer configs for " + custId, err);
                });
            },this);
        }

        // calling in network check just for know how collection updated
        function getCustomerConfig(){
             var deferred = $q.defer();
        	var customerCollection = jsonStoreService.getCustomerCollection();
        	customerCollection.findAll().then(function(data){
            deferred.resolve(data);
        	},function(err){
        		$log.error("Error in getting customer config", err);
                 deferred.reject(err);
        	});
            return deferred.promise;
        }

         function getCustomerConfigByID(id) {
          var deferred = $q.defer();
          var cusBy_id=[];
          var promiseColl = [];
           angular.forEach(id, function (piece, index) {
            promiseColl.push( 
            WL.JSONStore.get('customerCollection').find({customerId: piece},[]).then(function(data){
              if(data.length > 0)
                cusBy_id.push(data[0].json.DropLocations);
           }),function(err){
            $log.error("Error in getting customer config",err);
           });
          });
           $q.all(promiseColl).then(function (data) {
                deferred.resolve(cusBy_id);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getCustomerAttemptByID(id) {
          var deferred = $q.defer();
          var cusBy_id=[];
          var promiseColl = [];
           angular.forEach(id, function (piece, index) {
            promiseColl.push( 
            WL.JSONStore.get('customerCollection').find({customerId: piece},[]).then(function(data){
              if(data.length > 0)
                cusBy_id.push(data[0].json.Attempts);
           }),function(err){
            $log.error("Error in getting customer config",err);
           }
           )
          })
           $q.all(promiseColl).then(function (data) {
                deferred.resolve(cusBy_id);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getQuestionnaire(id){
          var deferred = $q.defer();
          var questionnaire=[];
          var promiseColl = [];
           angular.forEach(id, function (piece, index) {
            promiseColl.push( 
              WL.JSONStore.get('customerCollection').find({customerId: piece},[]).then(function(data){
                if(data.length > 0)
                 questionnaire.push(data[0].json.Questionnaire);
              }),function(err){
                $log.error("Error in getting customer config",err)
              }
            )
          })
           $q.all(promiseColl).then(function (data) {
                deferred.resolve(questionnaire);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }
      }     
})();