//Method to Download the Manifest
function getManifestItems(data) {
	var reqPath = getManifestString(data,'DMNF');
	var input = {
		method: 'GET',
		path: reqPath
	};
	//WL.Logger.info("**ELIAPP**>>>>> " + reqPath + " <<<<<");
	return WL.Server.invokeHttp(input);
}

//Method to Download Manifest Item Detail
function getManifestDetail(data) {
	var input = {
		method: 'POST',
		path: getManifestString(data,'PCDL'),
	};
	return WL.Server.invokeHttp(input);
}

function getManifestString(data, eventModifier) {
	//Parse the data object and build the string below
	var curDat = new Date().toISOString();
	var req = '/v10/InfoRequest/';
	req += eventModifier;
	req += '/Beam/';
	req += data.DeviceInfo.AppVersion+'/';
	req += data.DriverInfo.FacilityID+'/';
	req += data.DriverInfo.DriverID+'/';
	req += data.EventISODateTime+'/';
	req += data.LocationInfo.Longitude+'/';
	req += data.LocationInfo.Latitude+'/';
	req += data.LocationInfo.GPSFixDateTime+'/';
	req += data.LocationInfo.GPSPrecision+'/';
	req += data.SecurityGuid+'/';
	req += data.DeviceInfo.DeviceID+'/';
	req += data.DeviceInfo.PhoneIp+'/';
	req += data.DeviceInfo.PhoneNum+'/';
	req += data.DriverInfo.DriverID;
	req += '//////';
	if (data.BarCode) {
		req += data.BarCode;
	}
	req += '/////';
	return req;
}
