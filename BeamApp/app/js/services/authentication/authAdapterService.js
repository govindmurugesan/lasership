(function () {
    'use strict';

    angular
        .module('beam')
        .factory('AuthAdapterService', AuthAdapterService);

    AuthAdapterService.$inject = ['$window', '$http', 'deviceService', '$q', 'driverService', 'jsonStoreService','AppVersion', '$log'];
    function AuthAdapterService($window, $http, deviceService, $q, driverService, jsonStoreService,AppVersion, $log) {
        var service = {
            login: login,
            loginStatus: status,
            logOut: logOut,
        };
        return service;

        function login(driverId, password) {
            if (status()) {
                driverService.clearDriverInfo();
            }
            var def = $q.defer();
            var _date = new Date();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'InfoRequest',
                'eventModifier': 'LOGN',
                'appversion': AppVersion,
                'driverId': driverId,//'06243',
                'FacilityCode':driverId
            };
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([deviceService.getLocation(),
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID()])
                .then(function (data) {
                    var _pos = data[0];
                    var _ip = data[1];
                    var _phoneNum = data[2];
                    var uuid = data[3];
                    var req = {
                        "DriverInfo": {
                            "DriverID": guidRequest.driverId,
                            "FacilityID": guidRequest.FacilityCode,
                            "Password": password//"19857977"
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "LocationInfo": {
                            "Longitude": _pos.long,
                            "Latitude": _pos.lat,
                            "GPSPrecision": "1",
                            "GPSFixDateTime": _pos.timeStamp.toMyISOString()
                        },
                        "SecurityGuid": md,
                        "EventISODateTime": guidRequest.eventDate.toMyISOString()
                    };
                    var loginRequest = new WLResourceRequest(
                        "/adapters/Authenticator/getLogin",
                        WLResourceRequest.POST,
                        2000
                    );
                    loginRequest.setQueryParameter("params", [req]);
                    try {
                        loginRequest.send().then(
                            function (response) {
                                if (response.status == 200 && response.responseJSON.isSuccessful && response.responseJSON.statusCode != 404) {
                                    if (response.responseJSON.ErrorMessage && response.responseJSON.Error)
                                        def.reject(response.responseJSON.ErrorMessage);
                                    else {
                                        response.responseJSON.currentDriverId = driverId;
                                        driverService.saveDriverInfo(response.responseJSON);
                                        def.resolve(response.responseJSON);
                                    }
                                }
                                else {
                                    def.reject('Login Failed. Try Again');
                                }

                            },
                            function (error) {
                                def.reject(error);
                            }
                        );
                    } catch (error) {
                        def.reject(error)
                    }

                }, function (err) {
                    console.log('Error acquiring device infos', err);
                    def.reject(err);
                })
            return def.promise
        }

        function logOut() {
            var def = $q.defer();
            var _date = new Date();
            var driverInfo = driverService.getDriverInfo();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'InfoRequest',
                'eventModifier': 'LOGT',
                'appversion': AppVersion,
                'driverId': driverInfo.driverId,
                'facilityId': driverInfo.FacilityCode,
            }
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([deviceService.getLocation(),
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID()])
                .then(function (data) {
                    var _pos = data[0];
                    var _ip = data[1];
                    var _phoneNum = data[2];
                    var uuid = data[3];
                    var logoutRequest = new WLResourceRequest(
                        "/adapters/Authenticator/getLogout",
                        WLResourceRequest.POST,
                        2000
                    );
                    var req = {
                        "DriverInfo": {
                            "DriverID": driverInfo.driverId,
                            "FacilityID": driverInfo.FacilityCode,
                            "Password": "19857977"
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "LocationInfo": {
                            "Longitude": _pos.long,
                            "Latitude": _pos.lat,
                            "GPSPrecision": "1",
                            "GPSFixDateTime": _pos.timeStamp.toMyISOString()
                        },
                        "SecurityGuid": md,
                        "EventISODateTime": guidRequest.eventDate.toMyISOString()
                    };
                    logoutRequest.setQueryParameter("params", [req]);
                    try {
                        logoutRequest.send().then(
                            function (response) {
                                if (response.status == 200 && response.responseJSON.isSuccessful && response.responseJSON.errors.length == 0) // Logout Successful else throw error
                                    def.resolve(response.responseJSON);
                                else
                                    def.reject(response.responseJSON);
                                jsonStoreService.closeStore();

                            },
                            function (error) {
                                def.reject(error);
                                jsonStoreService.closeStore();
                            }
                        );
                    } catch (error) {
                        $log.error('Error calling adapter', error)
                    }

                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }

        function status() {
            return $window.localStorage['beamCredentials'] !== undefined;

        }
    }

})();
