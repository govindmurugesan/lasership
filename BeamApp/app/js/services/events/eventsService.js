(function () {
    'use strict';

    angular
        .module('beam')
        .factory('eventsService', eventsService);

    eventsService.$inject = ['$window', 'deviceService', '$q', 'jsonStoreService', 'manifestService', 'driverService', 'eventsAdapterService', 'AppVersion', '$log','SyncStats','eventsSerenityService'];
    function eventsService($window, deviceService, $q, jsonStoreService, manifestService, driverService, eventsAdapterService, AppVersion, $log,SyncStats,eventsSerenityService) {
        var service = {
            eventsUpdate: eventsUpdate,
            buildEventsData: buildEventsData,
            loadEventUpdate: loadEventUpdate,
            pickUpEventUpdate: pickUpEventUpdate,
            deliveredEventUpdate: deliveredEventUpdate,
            attemptBarcodesEventUpdate: attemptBarcodesEventUpdate,
            locationScanBarcodesEventUpdate: locationScanBarcodesEventUpdate,
            getUpdatedEvent: getUpdatedEvent,
            getUpdatedEventCount: getUpdatedEventCount,
            sendEvents: sendEvents,
            updateEventFlag: updateEventFlag,
            removeEvent : removeEvent,
            removeAllEvents : removeAllEvents
        };

        return service;

        function loadEventUpdate(loadedBarcodes, locationFirstBarcode, eventType) {
            var driverInfo = driverService.getDriverInfo();
            var def = $q.defer();
            $q.all([
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getLocation(),
                deviceService.getUUID()]).then(
                function (data) {
                    var _ip = data[0];
                    var _phoneNum = data[1];
                    var loc = data[2];
                    buildEventsData(loadedBarcodes, loc, locationFirstBarcode, eventType).then(function (BarcodesToUpdateInEvent) {
                        var event = {
                            "DriverInfo": {
                                "DriverID": driverInfo.driverId,
                                "FacilityID": driverInfo.FacilityCode,
                            },
                            "DeviceInfo": {
                                "DeviceID": data[3],
                                "PhoneNum": _phoneNum,
                                "PhoneIp": _ip,
                                "AppVersion": AppVersion
                            },
                            "Events": BarcodesToUpdateInEvent,
                        };
                        eventsUpdate(event).then(function (dataFromEvent) {
                            def.resolve(dataFromEvent);
                        }, function (err) {
                            $log.error('Error in updating thr load event', err);
                        });
                    }, function (err) {
                        $log.error('Error trying to build barcode events', err);
                    });
                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }

        function buildEventsData(loadedBarcodes, data, locationFirstBarcode, eventType) {
            var deferred = $q.defer();
            var BarcodesToUpdateInEvent = [];
            var promiseColl = [];
            var BarcodeDetail = {};
            angular.forEach(loadedBarcodes, function (piece, index) {
                promiseColl.push(
                    manifestService.getManifestDetailByBarcode(piece.barcode).then(
                        function (manifestData) {
                            if (manifestData.length > 0) {
                                BarcodeDetail = {
                                    "_eventID": index,
                                    "_eventDateTime": piece.scannedTime,
                                    "BarCode": manifestData[0].json.Barcode,
                                    "PieceKey": manifestData[0].json.PieceKey,
                                    "CustomerID": manifestData[0].json.CustomerID,
                                    "EventType": eventType,
                                    "EventModifier": "",
                                    "EventISODateTime": piece.scannedTime.toMyISOString(),
                                    "Location": locationFirstBarcode,
                                    "DoorTag": "",
                                    "TypedNameSignature": "",
                                    "SignatureWKT": "",
                                    "GroupingGuid": "",
                                    "LocationInfo": {
                                        "Longitude": data.long,
                                        "Latitude": data.lat,
                                        "GPSPrecision": "1",
                                        "GPSFixDateTime": data.timeStamp.toMyISOString()
                                    },
                                };
                            } else {
                                BarcodeDetail = {
                                    "_eventID": index,
                                    "_eventDateTime": piece.scannedTime,
                                    "BarCode": piece.barcode,
                                    "PieceKey": "",
                                    "CustomerID": "",
                                    "EventType": eventType,
                                    "EventModifier": "",
                                    "EventISODateTime": piece.scannedTime.toMyISOString(),
                                    "Location": locationFirstBarcode,
                                    "DoorTag": "",
                                    "TypedNameSignature": "",
                                    "SignatureWKT": "",
                                    "GroupingGuid": "",
                                    "LocationInfo": {
                                        "Longitude": data.long,
                                        "Latitude": data.lat,
                                        "GPSPrecision": "1",
                                        "GPSFixDateTime": data.timeStamp.toMyISOString()
                                    },
                                };
                            }
                            BarcodesToUpdateInEvent.push(BarcodeDetail);
                        }, function (err) {
                            $log.error('Error getting manifest data', err);
                        }
                    ));
            });
            $q.all(promiseColl).then(function () {
                deferred.resolve(BarcodesToUpdateInEvent);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function eventsUpdate(event) {
            var deferred = $q.defer();
            var eventColl = jsonStoreService.getEventsCollection();
            eventColl.add(event,{markDirty: false}).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                $log.error('error', err);
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function pickUpEventUpdate(pickedUpBarcodes, notes, address, customer, name, eventActionvalue, locationFirstBarcode, pickUpType, GUIDValue) {
            var driverInfo = driverService.getDriverInfo();
            var def = $q.defer();
            $q.all([
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getLocation(),
                deviceService.getUUID()]).then(
                function (data) {
                    var _ip = data[0];
                    var _phoneNum = data[1];
                    var loc = data[2];
                    buildEventsDataForPickUP(pickedUpBarcodes, loc, notes, address, customer, name, eventActionvalue, locationFirstBarcode, pickUpType, GUIDValue).then(function (BarcodesToUpdateInEvent) {
                        var event = {
                            "DriverInfo": {
                                "DriverID": driverInfo.driverId,
                                "FacilityID": driverInfo.FacilityCode,
                            },
                            "DeviceInfo": {
                                "DeviceID": data[3],
                                "PhoneNum": _phoneNum,
                                "PhoneIp": _ip,
                                "AppVersion": AppVersion
                            },
                            "Events": BarcodesToUpdateInEvent,
                        };
                        eventsUpdate(event).then(function (dataFromEvent) {
                            def.resolve(dataFromEvent);
                        }, function (err) {
                            $log.error('Error in updating the Pickup event', err);
                        });
                    }, function (err) {
                        $log.error('Error trying to build barcode events', err);
                    });
                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }

        function buildEventsDataForPickUP(pickedUpBarcodes, data, notes, address, customer, name, eventActionvalue, locationFirstBarcode, pickUpType, GUIDValue) {
            var deferred = $q.defer();
            var BarcodesToUpdateInEvent = [];
            var promiseColl = [];
            var BarcodeDetail = {};
            angular.forEach(pickedUpBarcodes, function (piece, index) {
                promiseColl.push(
                    manifestService.getManifestDetailByBarcode(piece.barcode).then(
                        function (manifestData) {
                            if (manifestData.length > 0 && manifestData[0].json.Origin != undefined) {
                                BarcodeDetail = {
                                    "_eventID": index,
                                    "_eventDateTime": piece.scannedTime,
                                    "BarCode": manifestData[0].json.Barcode,
                                    "PieceKey": manifestData[0].json.PieceKey,
                                    "CustomerID": manifestData[0].json.CustomerID,
                                    'EventType': 'Received',
                                    'EventModifier': 'PKUP',
                                    "EventISODateTime": piece.scannedTime.toMyISOString(),
                                    "Location": locationFirstBarcode,
                                    "DoorTag": "",
                                    "TypedNameSignature": "",
                                    "SignatureWKT": "",
                                    "GroupingGuid": GUIDValue,
                                    "LocationInfo": {
                                        "Longitude": data.long,
                                        "Latitude": data.lat,
                                        "GPSPrecision": "1",
                                        "GPSFixDateTime": data.timeStamp.toMyISOString()
                                    },
                                    "Notes": notes,
                                    "Address": manifestData[0].json.Origin.Address,
                                    "Customer": customer,
                                    "Name": manifestData[0].json.Origin.Contact,
                                    "EventActionValue": eventActionvalue + '|' + pickUpType
                                };
                            } else {
                                BarcodeDetail = {
                                    "_eventID": index,
                                    "_eventDateTime": piece.scannedTime,
                                    "BarCode": piece.barcode,
                                    "PieceKey": "",
                                    "CustomerID": "",
                                    'EventType': 'Received',
                                    'EventModifier': 'PKUP',
                                    "EventISODateTime": piece.scannedTime.toMyISOString(),
                                    "Location": locationFirstBarcode,
                                    "DoorTag": "",
                                    "TypedNameSignature": "",
                                    "SignatureWKT": "",
                                    "GroupingGuid": GUIDValue,
                                    "LocationInfo": {
                                        "Longitude": data.long,
                                        "Latitude": data.lat,
                                        "GPSPrecision": "1",
                                        "GPSFixDateTime": data.timeStamp.toMyISOString()
                                    },
                                    "Notes": notes,
                                    "Address": address,
                                    "Customer": customer,
                                    "Name": name,
                                    "EventActionValue": eventActionvalue + '|' + pickUpType
                                };
                            }
                            BarcodesToUpdateInEvent.push(BarcodeDetail);
                        }, function (err) {
                            $log.error('Error getting manifest data', err);
                        }
                    ));
            });
            $q.all(promiseColl).then(function () {
                deferred.resolve(BarcodesToUpdateInEvent);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function deliveredEventUpdate(deliveredBarcodes, typedNameSignature, signatureWKT, location, EventActionValue, doorTag) {
            var driverInfo = driverService.getDriverInfo();
            var def = $q.defer();
            $q.all([
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getLocation(),
                deviceService.getUUID()]).then(
                function (data) {
                    var _ip = data[0];
                    var _phoneNum = data[1];
                    var loc = data[2];
                    buildEventsDataForDelivered(deliveredBarcodes, loc, typedNameSignature, signatureWKT, location, EventActionValue, doorTag).then(function (BarcodesToUpdateInEvent) {
                        var event = {
                            "DriverInfo": {
                                "DriverID": driverInfo.driverId,
                                "FacilityID": driverInfo.FacilityCode,
                            },
                            "DeviceInfo": {
                                "DeviceID": data[3],
                                "PhoneNum": _phoneNum,
                                "PhoneIp": _ip,
                                "AppVersion": AppVersion
                            },
                            "Events": BarcodesToUpdateInEvent,
                        };
                        eventsUpdate(event).then(function (dataFromEvent) {
                            def.resolve(dataFromEvent);
                        }, function (err) {
                            $log.error('Error in updating the delivered event', err);
                        });
                    }, function (err) {
                        $log.error('Error trying to build barcode events', err);
                    });
                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }

        function buildEventsDataForDelivered(deliveredBarcodes, data, typedNameSignature, signatureWKT, location, EventActionValue, doorTag) {
            var deferred = $q.defer();
            var BarcodesToUpdateInEvent = [];
            var promiseColl = [];
            var BarcodeDetail = {};
            angular.forEach(deliveredBarcodes, function (piece, index) {
                promiseColl.push(
                    manifestService.getManifestDetailByBarcode(piece.barcode).then(
                        function (manifestData) {
                            if (manifestData.length > 0){
                                BarcodeDetail = {
                                    "_eventID": index,
                                    "_eventDateTime": piece.scannedTime,
                                    "BarCode": manifestData[0].json.Barcode,
                                    "PieceKey": manifestData[0].json.PieceKey,
                                    "CustomerID": manifestData[0].json.CustomerID,
                                    'EventType': 'Delivered',
                                    'EventModifier': '',
                                    "EventISODateTime": piece.scannedTime.toMyISOString(),
                                    "Location": location,
                                    "DoorTag": doorTag,
                                    "TypedNameSignature": typedNameSignature,
                                    "SignatureWKT": signatureWKT,
                                    "GroupingGuid": "",
                                    "LocationInfo": {
                                        "Longitude": data.long,
                                        "Latitude": data.lat,
                                        "GPSPrecision": "1",
                                        "GPSFixDateTime": data.timeStamp.toMyISOString()
                                    },
                                    "EventActionValue": EventActionValue[0]
                                };
                            }else{
                                BarcodeDetail = {
                                    "_eventID": index,
                                    "_eventDateTime": piece.scannedTime,
                                    "BarCode": piece.barcode,
                                    "PieceKey": "",
                                    "CustomerID": "",
                                    'EventType': 'Delivered',
                                    'EventModifier': '',
                                    "EventISODateTime": piece.scannedTime.toMyISOString(),
                                    "Location": location,
                                    "DoorTag": doorTag,
                                    "TypedNameSignature": typedNameSignature,
                                    "SignatureWKT": signatureWKT,
                                    "GroupingGuid": "",
                                    "LocationInfo": {
                                        "Longitude": data.long,
                                        "Latitude": data.lat,
                                        "GPSPrecision": "1",
                                        "GPSFixDateTime": data.timeStamp.toMyISOString()
                                    },
                                    "EventActionValue": EventActionValue[0]
                                };
                            }
                            BarcodesToUpdateInEvent.push(BarcodeDetail);
                        }, function (err) {
                            $log.error('Error getting manifest data', err);
                        }
                    ));
            });
            $q.all(promiseColl).then(function () {
                deferred.resolve(BarcodesToUpdateInEvent);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function attemptBarcodesEventUpdate(AttempteBarcodes, doorTag, attemptReason, location) {
            var driverInfo = driverService.getDriverInfo();
            var def = $q.defer();
            $q.all([
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getLocation(),
                deviceService.getUUID()]).then(
                function (data) {
                    var _ip = data[0];
                    var _phoneNum = data[1];
                    var loc = data[2];
                    buildEventsDataForAttempt(AttempteBarcodes, loc, doorTag, attemptReason, location).then(function (BarcodesToUpdateInEvent) {

                        var event = {
                            "DriverInfo": {
                                "DriverID": driverInfo.driverId,
                                "FacilityID": driverInfo.FacilityCode,
                            },
                            "DeviceInfo": {
                                "DeviceID": data[3],
                                "PhoneNum": _phoneNum,
                                "PhoneIp": _ip,
                                "AppVersion": AppVersion
                            },
                            "Events": BarcodesToUpdateInEvent,
                        };
                        eventsUpdate(event).then(function (dataFromEvent) {
                            def.resolve(dataFromEvent);
                        }, function (err) {
                            $log.error('Error in updating the Attempt event', err);
                        });
                    }, function (err) {
                        $log.error('Error trying to build barcode events', err);
                    });
                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }
        function buildEventsDataForAttempt(AttempteBarcodes, data, doorTag, attemptReason, location) {
            var deferred = $q.defer();
            var BarcodesToUpdateInEvent = [];
            var promiseColl = [];
            var BarcodeDetail = {};
            angular.forEach(AttempteBarcodes, function (piece, index) {
                promiseColl.push(
                    manifestService.getManifestDetailByBarcode(piece.barcode).then(
                        function (manifestData) {
                            if (manifestData.length > 0){
                                BarcodeDetail = {
                                    "_eventID": index,
                                    "_eventDateTime": piece.scannedTime,
                                    "BarCode": manifestData[0].json.Barcode,
                                    "PieceKey": manifestData[0].json.PieceKey,
                                    "CustomerID": manifestData[0].json.CustomerID,
                                    'EventType': 'Attempted',
                                    'EventModifier': attemptReason,
                                    "EventISODateTime": piece.scannedTime.toMyISOString(),
                                    "Location": location,
                                    "DoorTag": doorTag,
                                    "TypedNameSignature": "",
                                    "SignatureWKT": "",
                                    "GroupingGuid": "",
                                    "LocationInfo": {
                                        "Longitude": data.long,
                                        "Latitude": data.lat,
                                        "GPSPrecision": "1",
                                        "GPSFixDateTime": data.timeStamp.toMyISOString()
                                    }
                                };
                            }else{
                                BarcodeDetail = {
                                    "_eventID": index,
                                    "_eventDateTime": piece.scannedTime,
                                    "BarCode": piece.barcode,
                                    "PieceKey": "",
                                    "CustomerID": "",
                                    'EventType': 'Attempted',
                                    'EventModifier': attemptReason,
                                    "EventISODateTime": piece.scannedTime.toMyISOString(),
                                    "Location": location,
                                    "DoorTag": doorTag,
                                    "TypedNameSignature": "",
                                    "SignatureWKT": "",
                                    "GroupingGuid": "",
                                    "LocationInfo": {
                                        "Longitude": data.long,
                                        "Latitude": data.lat,
                                        "GPSPrecision": "1",
                                        "GPSFixDateTime": data.timeStamp.toMyISOString()
                                    }
                                };
                            }
                            BarcodesToUpdateInEvent.push(BarcodeDetail);
                        }, function (err) {
                            $log.error('Error getting manifest data', err);
                        }
                    ));
            });
            $q.all(promiseColl).then(function () {
                deferred.resolve(BarcodesToUpdateInEvent);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function locationScanBarcodesEventUpdate(locationBarcodes, eventActionvalue) {
            var driverInfo = driverService.getDriverInfo();
            var def = $q.defer();
            $q.all([
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID(),
                deviceService.getLocation()]).then(
                function (data) {
                    var _ip = data[0];
                    var _phoneNum = data[1];
                    var loc = data[3];
                    buildEventsDataForlocation(locationBarcodes, loc, eventActionvalue).then(function (BarcodesToUpdateInEvent) {
                        var event = {
                            "DriverInfo": {
                                "DriverID": driverInfo.driverId,
                                "FacilityID": driverInfo.FacilityCode,
                            },
                            "DeviceInfo": {
                                "DeviceID": data[2],
                                "PhoneNum": _phoneNum,
                                "PhoneIp": _ip,
                                "AppVersion": AppVersion
                            },
                            "Events": BarcodesToUpdateInEvent,
                        };
                        eventsUpdate(event).then(function (dataFromEvent) {
                            def.resolve(dataFromEvent);
                        }, function (err) {
                            $log.error('Error in updating the Location event', err);
                        });
                    }, function (err) {
                        $log.error('Error trying to build barcode events', err);
                    });
                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }

        function buildEventsDataForlocation(locationBarcodes, data, eventActionvalue) {
            var deferred = $q.defer();
            var BarcodesToUpdateInEvent = [];
            var promiseColl = [];
            angular.forEach(locationBarcodes, function (piece, index) {
                var BarcodeDetail = {
                    "_eventID": index,
                    "_eventDateTime": piece.scannedTime,
                    "BarCode": "",
                    "PieceKey": "",
                    "CustomerID": "",
                    'EventType': 'Miscellaneous',
                    'EventModifier': 'LCTN',
                    "EventISODateTime": piece.scannedTime.toMyISOString(),
                    "Location": piece.barcode,
                    "DoorTag": "",
                    "TypedNameSignature": "",
                    "SignatureWKT": "",
                    "GroupingGuid": "",
                    "LocationInfo": {
                        "Longitude": data.long,
                        "Latitude": data.lat,
                        "GPSPrecision": "1",
                        "GPSFixDateTime": data.timeStamp.toMyISOString()
                    },
                    "EventActionValue": eventActionvalue
                };
                BarcodesToUpdateInEvent.push(BarcodeDetail);
                promiseColl.push(BarcodeDetail);
            });
            $q.all(promiseColl).then(function () {
                deferred.resolve(BarcodesToUpdateInEvent);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getUpdatedEvent() {
            var deferred = $q.defer();
            var eventsUpdated = jsonStoreService.getEventsCollection();
            eventsUpdated.findAll().then(function (data) {
                deferred.resolve(data);
            }, function (error) {
                $log.error('Err finding all - events', error);
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getTopEventsToSend() {
            var deferred = $q.defer();
            var eventsUpdated = jsonStoreService.getEventsCollection();
            eventsUpdated.find({},{limit:300}).then(function (data) {
                deferred.resolve(data);
            }, function (error) {
                $log.error('Err finding all - events', error);
                deferred.reject(error);
            });
            return deferred.promise;
        }

        function getUpdatedEventCount() {
            var deferred = $q.defer();
            var eventColl = jsonStoreService.getEventsCollection();
            eventColl.count().then(function (res) {
                deferred.resolve(res);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function updateEventFlag(data) {
            var eventColl = jsonStoreService.getEventsCollection();
            var deferred = $q.defer();
            eventColl.replace(data, {}).then(function (x) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
        }

        function removeEvent(id) {
            var deferred = $q.defer();
            var eventColl = jsonStoreService.getEventsCollection();
            var query = { _id: id };
            var options = { exact: true,markDirty:false };
            eventColl.remove(query, options).then(function (res) {
                deferred.resolve(res);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function removeAllEvents() {
            var deferred = $q.defer();
            var eventColl = jsonStoreService.getEventsCollection();
            eventColl.findAll().then(function (data) {
                var ids = data.map(function(current){ return current._id;});
                var promises = [];
                while (ids.length) {
                promises.push(eventColl.remove(ids.pop()));
                }
                $q.all(promises).then(function (res) {
                    deferred.resolve(res);
                }, function (err) {
                    deferred.reject(err);
                });
            }, function (error) {
                $log.error('Err finding all - events', error);
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function handleEventResponses(serverResponses) {
            var promises = [];
            var removalEvents = [];
            var retryEvents = [];
            serverResponses.forEach(function (resp, index) {
                if (resp.serverResponse.statusCode == 200 && resp.serverResponse.isSuccessful) {
                    removalEvents.push(resp._eventID);
                }
                else if (resp.serverResponse.statusCode == 404) {
                    retryEvents.push(resp._eventID);
                }

            });
            if (removalEvents.length > 0 || retryEvents.length > 0) {
                removeSuccessfulEvent(serverResponses[0]._jsonID, removalEvents, retryEvents);
            }
        }

        function handleSerenityResponses(serverResponses) {
            var promises = [];
            var removalEvents = [];
            var retryEvents = [];
            serverResponses.forEach(function (resp, index) {
                if (resp.status == 200) {
                    removalEvents.push(resp._eventID);
                }
                else if (resp.status == 404) {
                    retryEvents.push(resp._eventID);
                }

            });
            if (removalEvents.length > 0 || retryEvents.length > 0) {
                removeSuccessfulEvent(serverResponses[0]._jsonID, removalEvents, retryEvents).then(function(){
                    sendEvents();
                    //console.info('Finished cleaning and called send again');
                },function(err){
                    $log.error('Err while cleaning events ' ,err); 
                });
            }
        }

        function removeSuccessfulEvent(_docid, _successEventIDs, _retryEventIDs) {
            var deferred = $q.defer();
            var eventColl = jsonStoreService.getEventsCollection();
            var query = { _id: _docid };
            var options = { exact: true, limit:1 };
            //console.info('Removing Succesfull Events : ' + _docid + ' events ' + _successEventIDs);
            eventColl.find(query, options).then(function (res) {
                if (res.length > 0) {
                    if (res[0].json.Events.length == 1) {
                        //TODO before removing check if eventid in success or retry
                        if (_retryEventIDs.indexOf(res[0].json.Events[0]._eventID) > -1) {
                            if (JSON.stringify(res).indexOf("Retry") == -1) {
                                res[0].json.Events[0].Retry = 1;
                                updateEventFlag(res);
                            } else if (res[0].json.Events[0].Retry == 1) {
                                res[0].json.Events[0].Retry = 2;
                                updateEventFlag(res);
                            } else if (res[0].json.Events[0].Retry == 2) {
                                res[0].json.Events[0].Retry = 3;
                                updateEventFlag(res);
                            } else if (res[0].json.Events[0].Retry == 3) {
                                var jsonEvent ={
                                    BarCode : res[0].json.Events[0].BarCode,
                                    EventType : res[0].json.Events[0].EventType,
                                    EventModifier : res[0].json.Events[0].EventModifier,
                                    EventISODateTime : res[0].json.Events[0].EventISODateTime
                                }
                                deferred.resolve(removeEvent(_docid));
                                $log.error('Removed from the events, Retry event reached max tries. Actual event: ' + JSON.stringify(jsonEvent)); //TODO
                            }
                        } else {
                            deferred.resolve(removeEvent(_docid));
                        }
                    }
                    else {
                        var newEvents = [];
                        res[0].json.Events.forEach(function (event) {
                            if (_successEventIDs.indexOf(event._eventID) == -1) {
                                if (_retryEventIDs.indexOf(event._eventID) > -1) {
                                    if (JSON.stringify(event).indexOf("Retry") == -1) {
                                        event.Retry = 1;
                                        newEvents.push(event);
                                    } else if (event.Retry == 1) {
                                        event.Retry = 2;
                                        newEvents.push(event);
                                    } else if (event.Retry == 2) {
                                        event.Retry = 3;
                                        newEvents.push(event);
                                    } else if (event.Retry == 3) {
                                        var jsonEvent ={
                                            BarCode : event.BarCode,
                                            EventType : event.EventType,
                                            EventModifier : event.EventModifier,
                                            EventISODateTime : event.EventISODateTime
                                        }
                                        deferred.resolve(removeEvent(_docid));
                                        $log.error('Removed from the events, Retry event reached max tries. Actual event ' + JSON.stringify(jsonEvent)); //TODO
                                    }
                                }
                                else{
                                    newEvents.push(event); // Not in success nor in retry so queue it back
                                }
                            }

                        }, this);
                        if (newEvents.length === 0) {
                            deferred.resolve(removeEvent(_docid));
                        }
                        else {
                            var jsonDoc = res[0].json;
                            jsonDoc.Events = newEvents;
                            var document = { _id: _docid, json: jsonDoc };
                            eventColl.replace(document, {}).then(function (x) {
                                deferred.resolve(document);
                            }, function (err) {
                                deferred.reject(err);
                            });
                        }

                    }

                }
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function sendEvents() {
            if (!deviceService.getConnectionStatus()){
                //SyncStats.synced = false;
                SyncStats.lastSynced = 'Offline >' + Date();
                $log.warn('Skipping Event send - Device Offline');
            }
            else
                getTopEventsToSend().then(function (events) {
                    events.forEach(function (eventDoc) {
                        eventDoc.json.Events.forEach(function (event) {
                            event.SecurityGuid = deviceService.getSecurityGuid({
                                eventType: event.EventType,
                                eventModifier: event.EventModifier,
                                _eventDateTime: new Date(event._eventDateTime),
                            });
                            event.EventActionValue = event.EventActionValue ? encodeURI(event.EventActionValue) : '';
                            event.BarCode = encodeURIComponent(event.BarCode.trim());
                            event.Location = encodeURIComponent(event.Location.trim());
                            event.DoorTag = encodeURIComponent(event.DoorTag.trim());
                            event.TypedNameSignature = event.TypedNameSignature ? encodeURI(event.TypedNameSignature) : '';
                        }, this);
                    }, this);
                    if (events.length > 0){
                        eventsSerenityService.sendEvents(events).then(function (responses) {
                            //console.info('*** Serenity Responses ***', responses);
                            responses.forEach(function (response) {
                                handleSerenityResponses(response);
                            });
                        }, function (err) {
                            $log.error('Err Syncing Events', err);
                        });
                        var mfpServer = JSON.parse($window.localStorage.mfpConnection?$window.localStorage.mfpConnection:'{}');
                        if (mfpServer && mfpServer.mfpConnectionStatus){
                            eventsAdapterService.sendEvents(events).then(function (responses) {
                                //console.info('*** API Responses ***', responses);
                            }, function (err) {
                                $log.error('Err Syncing Events', err);
                            });
                        }else{
                            //$log.error('Device Online but MFP Server Disconnected. Status @' + new Date(mfpServer.mfpConnectionUpdatedTime));
                        }
                    }
                    else{
                        //console.log('Should not be here',events);
                    }
                        
                });
        }
    }
})();