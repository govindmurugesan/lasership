# LaserShip-BeamApp
LaserShip Beam Mobile App

###BeamApp
This folder contains the actual UI for the mobile application.

 - Create platforms & plugins folders before adding environments
 - To add environments use mfp cordova platform add <platform name>
 - Make sure you have all plugins installed.

###BeamAppServer
This folder contains the server components for mobilefirst server.
 - Run mfp start (make sure server is running)
 - Run mfp push (make sure runtime with BeamAppServer exists on mfp console)
 - Console can be accessed using mfp console.
