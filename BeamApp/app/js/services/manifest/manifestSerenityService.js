(function () {
    'use strict';

    angular
        .module('beam')
        .factory('manifestSerenityService', manifestSerenityService);

    manifestSerenityService.$inject = ['$window', 'deviceService', '$q', 'driverService', '$log','AppVersion','SerenityURL','$http'];
    function manifestSerenityService($window, deviceService, $q, driverService, $log,AppVersion,SerenityURL,$http) {
        var service = {
            loadManifest: loadManifest
        };
        return service;

        function loadManifest() {
            var def = $q.defer();
            var _date = new Date();
            var driverInfo = driverService.getDriverInfo();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'InfoRequest',
                'eventModifier': 'DMNF',
                'appversion': AppVersion,
                'driverId': driverInfo.driverId,
                'facilityId': driverInfo.FacilityCode,
            }
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([deviceService.getLocation(),
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID()])
                .then(function (data) {
                    var _pos = data[0];
                    var _ip = data[1];
                    var _phoneNum = data[2];
                    var uuid = data[3];
                    var manifestRequest = new WLResourceRequest(
                        "/adapters/Manifest/getManifestItems",
                        WLResourceRequest.POST
                    );
                    var req = {
                        "DriverInfo": {
                            "DriverID": driverInfo.driverId,
                            "FacilityID": driverInfo.FacilityCode,
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "LocationInfo": {
                            "Longitude": _pos.long,
                            "Latitude": _pos.lat,
                            "GPSPrecision": "1",
                            "GPSFixDateTime": _pos.timeStamp.toMyISOString()
                        },
                        "SecurityGuid": md,
                        "EventISODateTime": guidRequest.eventDate.toMyISOString()
                    };
                    try {
                            var reqdata = getManifestString(req,'DMNF');
                            var serenityURL = SerenityURL +reqdata;
                            $http.get(serenityURL,{timeout:5000}).then(function (success) {
                                def.resolve(success.data);
                            }, function (error) {
                                def.reject(error);
                                $log.error('Error getting manifest from serenity ', error);
                        });
                    } catch (error) {
                        $log.error('Error calling serenity', error);
                    }

                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }

        function getManifestString(data, eventModifier) {
            //Parse the data object and build the string below
            var curDat = new Date().toISOString();
            var req = '/v10/InfoRequest/';
            req += eventModifier;
            req += '/Beam/';
            req += data.DeviceInfo.AppVersion+'/';
            req += data.DriverInfo.FacilityID+'/';
            req += data.DriverInfo.DriverID+'/';
            req += data.EventISODateTime+'/';
            req += data.LocationInfo.Longitude+'/';
            req += data.LocationInfo.Latitude+'/';
            req += data.LocationInfo.GPSFixDateTime+'/';
            req += data.LocationInfo.GPSPrecision+'/';
            req += data.SecurityGuid+'/';
            req += data.DeviceInfo.DeviceID+'/';
            req += data.DeviceInfo.PhoneIp+'/';
            req += data.DeviceInfo.PhoneNum+'/';
            req += data.DriverInfo.DriverID;
            req += '//////';
            if (data.BarCode) {
                req += data.BarCode;
            }
            req += '/////';
            return req;
        }

    }
})();