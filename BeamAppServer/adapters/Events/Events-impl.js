
function sendEvents(data) {
	var responses = [];

	if (data.json.DeviceInfo.AppVersion === '1.3.8') {
		return { 'adapterResponse': [] };
	}
	else {
		for (var i = 0; i < data.json.Events.length; i++) {
			responses.push(sendRequest(data, i));
		}
	}
	var resp = { 'adapterResponse': responses };
	return resp;
}

function sendRequest(data, eventIndex) {
	var input;
	var bodyContent = 'SignatureWKT=' + (data.json.Events[eventIndex].SignatureWKT ? data.json.Events[eventIndex].SignatureWKT : '');
	bodyContent = bodyContent + '&AdditionalSignatureWKT=&Note=' + (data.json.Events[eventIndex].Notes ? data.json.Events[eventIndex].Notes : '');
	if (data.json.Events[eventIndex].EventModifier === 'LLOG')
		bodyContent = bodyContent + '&EventActionValue=' + (data.json.Events[eventIndex].EventActionValue ? data.json.Events[eventIndex].EventActionValue : '');

	input = {
		method: 'POST',
		path: getEventString(data.json, eventIndex),
		body: {
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			content: bodyContent,
		},
	};

	var returnData = {
		'serverResponse': WL.Server.invokeHttp(input),
		'path': input.path,
		'_eventID': data.json.Events[eventIndex]._eventID,
		'_jsonID': data._id
	};
	return returnData;
}

function getEventString(data, eventIndex) {
	//Parse the data object and build the string below
	var curDat = new Date().toISOString();
	var req = '/v10/';
	req += data.Events[eventIndex].EventType + '/';
	req += data.Events[eventIndex].EventModifier;
	req += '/Beam/';
	req += data.DeviceInfo.AppVersion + '/';
	req += data.DriverInfo.FacilityID + '/';
	req += data.DriverInfo.DriverID + '/';
	req += data.Events[eventIndex].EventISODateTime + '/';
	req += data.Events[eventIndex].LocationInfo.Longitude + '/';
	req += data.Events[eventIndex].LocationInfo.Latitude + '/';
	req += data.Events[eventIndex].LocationInfo.GPSFixDateTime + '/';
	req += data.Events[eventIndex].LocationInfo.GPSPrecision + '/';
	req += data.Events[eventIndex].SecurityGuid + '/';
	req += data.DeviceInfo.DeviceID + '/';
	req += data.DeviceInfo.PhoneIp + '/';
	req += data.DeviceInfo.PhoneNum + '/';
	req += data.DriverInfo.DriverID + '/';
	if (data.Events[eventIndex].EventModifier !== 'LLOG')
		req += (data.Events[eventIndex].EventActionValue ? data.Events[eventIndex].EventActionValue : '');
	req += '/';
	if (data.Events[eventIndex].SignatureWKT) { req += '1'; }
	else { req += '0'; }
	req += '//';
	if (data.Events[eventIndex].CustomerID && data.Events[eventIndex].CustomerID !== "") { req += data.Events[eventIndex].CustomerID; }
	else if (data.Events[eventIndex].Customer && data.Events[eventIndex].Customer !== "") { req += encodeURI(data.Events[eventIndex].Customer).replace("/", "%2F"); }
	req += '/';
	if (data.Events[eventIndex].PieceKey) { req += data.Events[eventIndex].PieceKey; }
	req += '/';
	if (data.Events[eventIndex].BarCode) { req += data.Events[eventIndex].BarCode; }
	req += '/';
	if (data.Events[eventIndex].Location) { req += data.Events[eventIndex].Location; }
	req += '/';
	if (data.Events[eventIndex].DoorTag) { req += data.Events[eventIndex].DoorTag; }
	req += '/';
	if (data.Events[eventIndex].TypedNameSignature) { req += data.Events[eventIndex].TypedNameSignature; }
	req += '//';
	if (data.Events[eventIndex].GroupingGuid) { req += data.Events[eventIndex].GroupingGuid; }
	return req;
}

