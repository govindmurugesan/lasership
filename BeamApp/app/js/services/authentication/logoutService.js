(function () {
    'use strict';

    angular
        .module('beam')
        .factory('logoutService', logoutService);

    logoutService.$inject = ['$window', 'rootScopeService', 'driverService', '$log', 'dataSyncService','scanService','AuthSerenityService','notificationService','deviceService','$location','SyncStats','$state'];
    function logoutService($window, rootScopeService,  driverService, $log, dataSyncService,scanService,AuthSerenityService,notificationService,deviceService,$location,SyncStats,$state) {
        var service = {
            processLogout : processLogout
        };
        
        return service;

        function processLogout() {
            if($window.cordova.plugins.backgroundMode)
                $window.cordova.plugins.backgroundMode.disable();
            var loc = $location.path().split('/');
            if (("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/load') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/delivery') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/attempt') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/pickup') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/location') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/door')) {
                scanService.hideScanner();
            }
            notificationService.showLoad('Logging Out');
            AuthSerenityService.logOut().then(function (data) {
                notificationService.hideLoad();
                $state.go('login');
                dataSyncService.removeSync();
                dataSyncService.cancelLocationPoll();
                driverService.clearDriverInfo();
                dataSyncService.stopEventSyncr();
                deviceService.clearLocationWatcher();
                rootScopeService.clearLocalStorageVariables();
                rootScopeService.clearLoadedLocalVariables();
                $window.localStorage.removeItem("previousURL");
                WL.App.close();
            }, function (err) {
                $log.warn('Error logging out', err);
                notificationService.hideLoad();
                $state.go('login');
                dataSyncService.removeSync();
                dataSyncService.cancelLocationPoll();
                driverService.clearDriverInfo();
                dataSyncService.stopEventSyncr();
                deviceService.clearLocationWatcher();
                rootScopeService.clearLocalStorageVariables();
                rootScopeService.clearLoadedLocalVariables();
                $window.localStorage.removeItem("previousURL");
                WL.App.close();
            });
            if (driverService.getDriverInfo().OfflineAuth)
                $window.removeEventListener('online', reAuthSetup, false);
            SyncStats.synced = false;
            SyncStats.syncInProgress = false;
            SyncStats.directSyncInProgress=false;
        }
    }
})();