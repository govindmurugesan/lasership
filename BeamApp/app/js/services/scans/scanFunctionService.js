(function () {
    'use strict';

    angular
        .module('beam')
        .factory('scanFunctionService', scanFunctionService);
        scanFunctionService.$inject = ['$rootScope'];
        function scanFunctionService($rootScope) {

        	var service = {
	            scanType: scanType,
	            clearRootScope: clearRootScope,
             	clearScan: clearScan
	        };
	        
        	return service;

        	function scanType(){

	          if(angular.element('#d_delivery').hasClass('boxshadow')){
	            return 'delivery';
	          }else if(angular.element('#delivery_attempt').hasClass('boxshadow')){
	            return 'attempt';
	          }else if(angular.element('#delivery_clear').hasClass('boxshadow')){
	          	return 'clear';
	          }else if(angular.element('#delivery_pickup').hasClass('boxshadow')){
	          	return 'pickup';
	          }else if(angular.element('#delivery_location').hasClass('boxshadow')){
	          	return 'location';
	          }else if(angular.element('#load_location').hasClass('boxshadow')){
	          	return 'location';
	          }else if(angular.element('#load_clear').hasClass('boxshadow')){
	          	return 'clear';
	          }else if(angular.element('#l_load').hasClass('boxshadow')){
	          	return 'load';
	          }else if(angular.element('#a_attempt').hasClass('boxshadow')){
	          	return 'attempt';
	          }else if(angular.element('#attempt_location').hasClass('boxshadow')){
	          	return 'location';
	          }else if(angular.element('#attempt_clear').hasClass('boxshadow')){
	          	return 'clear';
	          }else if(angular.element('#attempt_pickup').hasClass('boxshadow')){
	          	return 'pickup';
	          }else if(angular.element('#lc_location').hasClass('boxshadow')){
	          	return 'location';
	          }else if(angular.element('#location_load').hasClass('boxshadow')){
	          	return 'load';
	          }else if(angular.element('#location_clear').hasClass('boxshadow')){
	          	return 'clear';
	          }else if(angular.element('#location_pickup').hasClass('boxshadow')){
	          	return 'pickup';
	          }else if(angular.element('#location_attempt').hasClass('boxshadow')){
	          	return 'attempt';
	          }else if(angular.element('#location_delivery').hasClass('boxshadow')){
	          	return 'delivery';
	          }else if(angular.element('#p_pickup').hasClass('boxshadow')){
	          	return 'pickup';
	          }else if(angular.element('#pickup_location').hasClass('boxshadow')){
	          	return 'location';
	          }else if(angular.element('#pickup_clear').hasClass('boxshadow')){
	          	return 'clear';
	          }else if(angular.element('#doortag').hasClass('boxshadow')){
	          	return 'door';
	          }else if(angular.element('#door_clear').hasClass('boxshadow')){
	          	return 'clear';
	          }else{
	          	return '';
	          }
	          
	        } 
	        function clearRootScope(){
	            $rootScope.Reasonselected = [];
	            $rootScope.addUnassignpickupItems = [];
	            $rootScope.unassignedScanItems = [];
	            $rootScope.BarcodesBelongsToStop = [];
	            $rootScope.unCheckedScanItems = [];
	         }
	        function clearScan(ScannedItems, itemToClear){
	            for(var i in ScannedItems){
	            	if(ScannedItems[i].barcode==itemToClear){
	                    ScannedItems.splice(i,1);
	                    break;
	                }
	            }
	            return ScannedItems;   
	        }

        }
})();