// This is a generated file. Do not edit. See application-descriptor.xml.
// WLClient configuration variables.
console.log("Running static_app_props.js");
var WL = WL ? WL : {};
WL.StaticAppProps = {
  "APP_DISPLAY_NAME": "Eli",
  "APP_SERVICES_URL": "/apps/services/",
  "APP_ID": "com_lasership_eli",
  "APP_VERSION": "1.0.0",
  "WORKLIGHT_PLATFORM_VERSION": "7.1.0.0",
  "ENVIRONMENT": "iphone",
  "LOGIN_DISPLAY_TYPE": "embedded",
  "WORKLIGHT_NATIVE_VERSION": "2486286929",
  "mfpManualInit": false,
  "WORKLIGHT_ROOT_URL": "/apps/services/api/com_lasership_eli/iphone/"
};