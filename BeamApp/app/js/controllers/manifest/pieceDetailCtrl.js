(function () {
  'use strict';

  angular
  .module('beam')
  .controller('detailCtrl', detailCtrl);

  detailCtrl.$inject = ['$scope', '$state', '$ionicHistory', 'mapService', '$stateParams', 'manifestService', 'notificationService','deviceService','packageStatusService','$rootScope','eventsService','scanFunctionService','$log','$q','$filter','rootScopeService'];

  function detailCtrl($scope, $state, $ionicHistory, mapService, $stateParams, manifestService, notificationService,deviceService,packageStatusService,$rootScope,eventsService,scanFunctionService, $log, $q,$filter,rootScopeService) { 

    var vm = this;
    vm.locationRouting = locationRouting;  
    vm.deliveryScan = deliveryScan;
    vm.pickUpScanRouting =  pickUpScanRouting;
    vm.loadScanRouting = loadScanRouting;
    vm.geolocate = geolocate;
    var location;
    vm.googleMapLoad = false;
    vm.pieceDetail = [];
    vm.navigation = navigation;
    vm.manifestType = "delivery";
    vm.stopIdentifier ="";
    vm.groupBy ="";
    vm.Barcode ="";
    vm.type = $stateParams.type;
    vm.unassignLoad = $rootScope.unassignedScanItems;
    vm.name ="";
    vm.address ="";
    vm.notes = "";
    vm.distanceLocator = distanceLocator;
    vm.SignatureShow = false;
    var pieceDetailWithPackageStatus = [];
    var pieceDetailFromManifest = [];
    var promises = [];
    if(vm.type == "pickup"){
      vm.pickup = true;
      vm.delivery = false
    }else{
      vm.delivery = true;
      vm.pickup = false
    }

    $scope.$on("$ionicView.beforeEnter", function () {
      if(deviceService.getConnectionStatus()) location = deviceService.getLocation();
    });

    $scope.$on("$ionicView.enter", function(){ 
      if($rootScope.unassignedScanItems.length > 0){
        initialize();
      }else{
        if(vm.type == "pickup"){
          vm.pickup = true;
          vm.delivery = false;        
        }else{
          vm.delivery = true;
          vm.pickup = false;
        }
        var deferred = $q.defer();
        promises.push(loadManifest());
        $q.all(promises).then(function (data) {
          var promiseColl = [];
          angular.forEach(pieceDetailFromManifest, function (manifestPiece, index) { 
            if($rootScope.packageCollection.length>0){
              promiseColl.push(
                packageStatusService.getPackageByBarcode(manifestPiece.Barcode).then(function(data){
                  if(data.length>0){
                    addManifestWithPackageStatus(manifestPiece,data);                          
                  }else{
                    addManifest(manifestPiece);
                  }
                },function (err){
                  $log.info("Error in getting package status",err);
                })
                )
            }else{
              promiseColl.push(
                addManifest(manifestPiece)
                )
            }
          })
          $q.all(promiseColl).then(function (picedetaildata) {
            var orderedData = $filter('orderBy')(pieceDetailWithPackageStatus, 'DeliveryRouteSequence');
            var groupedData = $filter('groupBy')(orderedData, 'Contact');
            var notloaded = 0;
            var delivered = 0;
            var attempted = 0;
            var loaded = 0;
            var showSignatureCount = 0;            
            angular.forEach(groupedData, function (key, val) {
              if(key.length>1){
                var length = ((key.length) - 1);
                for (var i in key) {
                  if (key[i].Loaded == true){
                    if(key[i].Delivered == true){
                      delivered++;
                      loaded++;
                    }else if(key[i].Attempted == true){
                      attempted++
                      loaded++
                    }else{
                      loaded++
                    }
                  }else{
                    if(key[i].Delivered == true){
                      delivered++;
                    }else if(key[i].Attempted == true){
                      attempted++;
                    }else{
                       notloaded++;
                    }
                   
                  }
                  if (key[i].SignatureRequirement == 'Required') {
                    showSignatureCount++

                  }
                  if ((i == length) && delivered == (key.length)) {
                    for (var j in key) {
                      key[j].status = 'delivered'
                      vm.deliveryrouting = true;
                      vm.loadrouting =false;
                      vm.attempteScanrouting = false;
                    }
                  }
                  if ((i == length) && attempted == (key.length)) {
                    for (var j in key) {
                      key[j].status = 'attempted'
                      vm.attempteScanrouting = true;
                      vm.deliveryrouting = false;
                      vm.loadrouting =false;
                    }
                  }
                  if ((i == length) && (delivered != (key.length)) && (attempted != (key.length)) && (loaded > 0) ) {
                    for (var j in key) {
                      key[j].status = 'readyToDeliver'
                      vm.deliveryrouting = true;
                      vm.loadrouting =false;
                      vm.attempteScanrouting = false;
                    }
                  }
                  if ((i == length) && (delivered != (key.length)) && (attempted != (key.length)) && (loaded == 0) ) {
                    for (var j in key) {
                      key[j].status = 'notloaded'
                      vm.deliveryrouting = false;
                      vm.loadrouting =true;
                      vm.attempteScanrouting = false;
                    }
                  }
                  if ((i == length) && showSignatureCount >= 1) {
                    for (var l in key) {
                      key[l].showSignature = true;
                    }
                  }
                }
                vm.pieceDetail.push(key);
              }else{
                if (key[0].Loaded == true){
                  if(key[0].Delivered == true){
                    key[0].status = 'delivered'
                    vm.deliveryrouting = true;
                    vm.loadrouting =false;
                    vm.attempteScanrouting = false;

                  }else if(key[0].Attempted == true){
                    key[0].status = 'attempted'
                    vm.attempteScanrouting = true;
                    vm.deliveryrouting = false;
                    vm.loadrouting =false;

                  }else{
                    key[0].status = 'readyToDeliver'
                    vm.deliveryrouting = true;
                    vm.loadrouting =false;
                    vm.attempteScanrouting = false;

                  }
                }else{
                  if(key[0].Delivered == true){
                    key[0].status = 'delivered'
                    vm.deliveryrouting = true;
                    vm.loadrouting =false;
                    vm.attempteScanrouting = false;

                  }else if(key[0].Attempted == true){
                    key[0].status = 'attempted'
                    vm.attempteScanrouting = true;
                    vm.deliveryrouting = false;
                    vm.loadrouting =false;

                  }else{
                    key[0].status = 'notloaded'
                    vm.loadrouting= true;
                    vm.deliveryrouting =false;
                    vm.attempteScanrouting = false;
                  }
                  
                }
                if (key[0].SignatureRequirement == 'Required') {
                  key[0].showSignature = true;
                }
                vm.pieceDetail.push(key);
              }
            })
            if(vm.type == 'delivery' && vm.pieceDetail[0][0].Instruction != "" && ($scope.previousURL == 'app.manifest' || $scope.previousURL == 'app.seqmanifest')){
              notificationService.alert('','',vm.pieceDetail[0][0].Instruction,function(){
                distanceLocator();
              });
            }else{
              distanceLocator();
            }
          })
})

}
}); 

$scope.$on("$ionicView.afterEnter", function () {
  notificationService.hideLoad();
});

function locationRouting() {
  if(deviceService.getConnectionStatus()){
    deviceService.getLocation().then(function(data){
      var currentAddress = data.lat + ',' +data.long;
      if (ionic.Platform.isIOS())
        window.open("http://maps.apple.com/?q=" + currentAddress, '_system');
      else
        window.open("http://maps.google.com/?q="+currentAddress, '_system');
    },function(err){
      $log.info("Error in getting location detail",err);
    })
  }      
};

function deliveryScan(stopIdentifier) {
  scanFunctionService.clearRootScope();
  rootScopeService.clearLocalStorage();
  $ionicHistory.nextViewOptions({
    disableBack: true
  });
  $state.go('app.scan.stop',{'stopIdentifier': stopIdentifier});
};
function pickUpScanRouting(stopIdentifier){
  scanFunctionService.clearRootScope();
  rootScopeService.clearLocalStorage();
  $ionicHistory.nextViewOptions({
    disableBack: true
  });
  $state.go('app.scan.stop',{'stopIdentifier': stopIdentifier});
}
function loadScanRouting(stopIdentifier){ 
  scanFunctionService.clearRootScope();
  rootScopeService.clearLocalStorage();
  $ionicHistory.nextViewOptions({
    disableBack: true
  });
  $state.go('app.scan.stop',{'stopIdentifier': stopIdentifier});
}
function navigation(data) {
  if (ionic.Platform.isIOS())
    window.open("http://maps.apple.com/?q=" + data, '_system');
  else
    window.open("http://maps.google.com/?q="+data, '_system');
}

var placeSearch, autocomplete;

function initialize() {
  if (typeof google === 'object' && typeof google.maps === 'object' && deviceService.getConnectionStatus() == true) {
    autocomplete = new google.maps.places.Autocomplete(
      (document.getElementById('autoCompleteAddress')), {
        types: ['geocode']
      });

    google.maps.event.addDomListener(document.getElementById('autoCompleteAddress'), 'focus', geolocate);
  }
}

function geolocate() { 
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
      var geolocation = new google.maps.LatLng(
        position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
function distanceLocator(){
  if($scope.previousURL == 'app.manifest' || $scope.previousURL == 'app.seqmanifest'){
    notificationService.confirm('','Are you at'+' '+vm.pieceDetail[0][0].Address+'?', 'Yes', 'No',function(){
      $log.info('At the current address');
    },function(){
      $log.info('Not at the current address');
    })
  }
  if(vm.pieceDetail[0][0].Latitude != "" && vm.pieceDetail[0][0].Longitude != "" && (deviceService.getConnectionStatus()) ){
    var destLatLog = {lat: parseFloat(vm.pieceDetail[0][0].Latitude), lng: parseFloat(vm.pieceDetail[0][0].Longitude)};
    var mapDestination = mapService.destinationPieceMap(destLatLog);
    mapDestination.then(function(data){
      vm.googleMapLoad = data; 
    }, function(err){
      vm.googleMapLoad = false;
      $log.warn('Error in getting googleMap status', err);
    });
  }
}

function loadManifest() {
  if(vm.pickup){
    var deferred = $q.defer();
    manifestService.getPickUpDetailByStopIdentifier($stateParams.stopIdentifier).then(
      function (data) {
        vm.groupBy = 'Destination.Contact'
        vm.stopIdentifier = data[0].json.StopIdentifier;
        pieceDetailFromManifest = [];
        angular.forEach(data, function (piece, index) {
          pieceDetailFromManifest.push(piece.json);
        });
        deferred.resolve(data);
      },function (err) {
        $log.error('Error getting manifest data', err);
        deferred.reject(err);
      })
  }else{
    var deferred = $q.defer();
    manifestService.getManifestDetailByStopIdentifier($stateParams.stopIdentifier).then(
      function (data) {
        vm.groupBy = 'Destination.Contact'
        vm.stopIdentifier = data[0].json.Destination.StopIdentifier;
        pieceDetailFromManifest = [];
        angular.forEach(data, function (piece, index) {
          pieceDetailFromManifest.push(piece.json);
        });
        deferred.resolve(data);
      },function (err) {
        $log.error('Error getting manifest data', err);
        deferred.reject(err);
      })
  }
  return deferred.promise;
}


function addManifest(manifestPiece){
  var manifestPieceDetail = "";
  if(manifestPiece.Origin !== undefined && manifestPiece.Destination == undefined){
    manifestPieceDetail= {
      'Type': 'pickup',
      'Barcode': manifestPiece.Barcode,
      'SignatureRequirement':manifestPiece.SignatureRequirement,
      'Address': manifestPiece.Origin.Address,
      'Address2': manifestPiece.Origin.Address2,
      'AddressQuality':manifestPiece.Origin.AddressQuality,
      'City': manifestPiece.Origin.City,
      'Contact':  manifestPiece.Origin.Contact,
      'Country': manifestPiece.Origin.Country,
      'DeliveryRoute':  manifestPiece.Origin.DeliveryRoute,
      'DeliveryRouteSequence': manifestPiece.Origin.DeliveryRouteSequence,
      'FacilityCode': manifestPiece.Origin.FacilityCode,
      'Instruction':  manifestPiece.Origin.Instruction,
      'Latitude':  manifestPiece.Origin.Latitude,
      'LocationType':  manifestPiece.Origin.LocationType,
      'Longitude':  manifestPiece.Origin.Longitude,
      'Organization': manifestPiece.Origin.Organization,
      'Phone':  manifestPiece.Origin.Phone,
      'PhoneExtension':  manifestPiece.Origin.PhoneExtension,
      'PostalCode':  manifestPiece.Origin.PostalCode,
      'State': manifestPiece.Origin.State,
      'StopIdentifier':  manifestPiece.Origin.StopIdentifier,
      'UTCExpectedDeliveryBy':manifestPiece.Origin.UTCExpectedDeliveryBy,
      'StopIdentifierForGrouping':  "Origin"+manifestPiece.Origin.StopIdentifier,
      'Assigned': '',
      'Attempted': '',
      'Delivered': '',
      'Loaded': '',
      'PickUp': '',
      'showSignature': false,
      'status': 'notloaded'
    }
    pieceDetailWithPackageStatus.push(manifestPieceDetail);
  }
  if(manifestPiece.Origin == undefined && manifestPiece.Destination != undefined){
    manifestPieceDetail ={
      'Type': 'delivery',
      'Barcode': manifestPiece.Barcode,
      'SignatureRequirement':manifestPiece.SignatureRequirement,
      'Address': manifestPiece.Destination.Address,
      'Address2': manifestPiece.Destination.Address2,
      'AddressQuality':manifestPiece.Destination.AddressQuality,
      'City': manifestPiece.Destination.City,
      'Contact':  manifestPiece.Destination.Contact,
      'Country': manifestPiece.Destination.Country,
      'DeliveryRoute':  manifestPiece.Destination.DeliveryRoute,
      'DeliveryRouteSequence': manifestPiece.Destination.DeliveryRouteSequence,
      'FacilityCode': manifestPiece.Destination.FacilityCode,
      'Instruction':  manifestPiece.Destination.Instruction,
      'Latitude':  manifestPiece.Destination.Latitude,
      'LocationType':  manifestPiece.Destination.LocationType,
      'Longitude':  manifestPiece.Destination.Longitude,
      'Organization': manifestPiece.Destination.Organization,
      'Phone': manifestPiece.Destination.Phone,
      'PhoneExtension':  manifestPiece.Destination.PhoneExtension,
      'PostalCode':  manifestPiece.Destination.PostalCode,
      'State': manifestPiece.Destination.State,
      'StopIdentifier':  manifestPiece.Destination.StopIdentifier,
      'UTCExpectedDeliveryBy':manifestPiece.Destination.UTCExpectedDeliveryBy,
      'StopIdentifierForGrouping':  "Destination"+manifestPiece.Destination.StopIdentifier,
      'Assigned': '',
      'Attempted': '',
      'Delivered': '',
      'Loaded': '',
      'PickUp': '',
      'showSignature': false,
      'status': 'notloaded'
    }
    pieceDetailWithPackageStatus.push(manifestPieceDetail);
  }

  if(manifestPiece.Origin != undefined && manifestPiece.Destination != undefined){
    if(vm.pickup){
      manifestPieceDetail= {
        'Type': 'pickup',
        'Barcode': manifestPiece.Barcode,
        'SignatureRequirement':manifestPiece.SignatureRequirement,
        'Address': manifestPiece.Origin.Address,
        'Address2': manifestPiece.Origin.Address2,
        'AddressQuality':manifestPiece.Origin.AddressQuality,
        'City': manifestPiece.Origin.City,
        'Contact':  manifestPiece.Origin.Contact,
        'Country': manifestPiece.Origin.Country,
        'DeliveryRoute':  manifestPiece.Origin.DeliveryRoute,
        'DeliveryRouteSequence': manifestPiece.Origin.DeliveryRouteSequence,
        'FacilityCode': manifestPiece.Origin.FacilityCode,
        'Instruction':  manifestPiece.Origin.Instruction,
        'Latitude':  manifestPiece.Origin.Latitude,
        'LocationType':  manifestPiece.Origin.LocationType,
        'Longitude':  manifestPiece.Origin.Longitude,
        'Organization': manifestPiece.Origin.Organization,
        'Phone':  manifestPiece.Origin.Phone,
        'PhoneExtension':  manifestPiece.Origin.PhoneExtension,
        'PostalCode':  manifestPiece.Origin.PostalCode,
        'State': manifestPiece.Origin.State,
        'StopIdentifier':  manifestPiece.Origin.StopIdentifier,
        'UTCExpectedDeliveryBy':manifestPiece.Origin.UTCExpectedDeliveryBy,
        'StopIdentifierForGrouping':  "Origin"+manifestPiece.Origin.StopIdentifier,
        'Assigned': '',
        'Attempted': '',
        'Delivered': '',
        'Loaded': '',
        'PickUp': '',
        'showSignature': false,
        'status': 'notloaded'
      }
      pieceDetailWithPackageStatus.push(manifestPieceDetail);
    }else{
      manifestPieceDetail ={
        'Type': 'delivery',
        'Barcode': manifestPiece.Barcode,
        'SignatureRequirement':manifestPiece.SignatureRequirement,
        'Address': manifestPiece.Destination.Address,
        'Address2': manifestPiece.Destination.Address2,
        'AddressQuality':manifestPiece.Destination.AddressQuality,
        'City': manifestPiece.Destination.City,
        'Contact':  manifestPiece.Destination.Contact,
        'Country': manifestPiece.Destination.Country,
        'DeliveryRoute':  manifestPiece.Destination.DeliveryRoute,
        'DeliveryRouteSequence': manifestPiece.Destination.DeliveryRouteSequence,
        'FacilityCode': manifestPiece.Destination.FacilityCode,
        'Instruction':  manifestPiece.Destination.Instruction,
        'Latitude':  manifestPiece.Destination.Latitude,
        'LocationType':  manifestPiece.Destination.LocationType,
        'Longitude':  manifestPiece.Destination.Longitude,
        'Organization': manifestPiece.Destination.Organization,
        'Phone': manifestPiece.Destination.Phone,
        'PhoneExtension':  manifestPiece.Destination.PhoneExtension,
        'PostalCode':  manifestPiece.Destination.PostalCode,
        'State': manifestPiece.Destination.State,
        'StopIdentifier':  manifestPiece.Destination.StopIdentifier,
        'UTCExpectedDeliveryBy':manifestPiece.Destination.UTCExpectedDeliveryBy,
        'StopIdentifierForGrouping':  "Destination"+manifestPiece.Destination.StopIdentifier,
        'Assigned': '',
        'Attempted': '',
        'Delivered': '',
        'Loaded': '',
        'PickUp': '',
        'showSignature': false,
        'status': 'notloaded'
      }
      pieceDetailWithPackageStatus.push(manifestPieceDetail);
    }
  }
}

function addManifestWithPackageStatus(manifestPiece, packageStatus) {
  var manifestPieceDetail = {};
  if (manifestPiece.Origin !== undefined && manifestPiece.Destination === undefined) {
    manifestPieceDetail = {
      'Type': 'pickup',
      'Barcode': manifestPiece.Barcode,
      'SignatureRequirement': manifestPiece.SignatureRequirement,
      'Address': manifestPiece.Origin.Address,
      'Address2': manifestPiece.Origin.Address2,
      'AddressQuality': manifestPiece.Origin.AddressQuality,
      'City': manifestPiece.Origin.City,
      'Contact': manifestPiece.Origin.Contact,
      'Country': manifestPiece.Origin.Country,
      'DeliveryRoute': manifestPiece.Origin.DeliveryRoute,
      'DeliveryRouteSequence': manifestPiece.Origin.DeliveryRouteSequence,
      'FacilityCode': manifestPiece.Origin.FacilityCode,
      'Instruction': manifestPiece.Origin.Instruction,
      'Latitude': manifestPiece.Origin.Latitude,
      'LocationType': manifestPiece.Origin.LocationType,
      'Longitude': manifestPiece.Origin.Longitude,
      'Organization': manifestPiece.Origin.Organization,
      'Phone': manifestPiece.Origin.Phone,
      'PhoneExtension': manifestPiece.Origin.PhoneExtension,
      'PostalCode': manifestPiece.Origin.PostalCode,
      'State': manifestPiece.Origin.State,
      'StopIdentifier': manifestPiece.Origin.StopIdentifier,
      'UTCExpectedDeliveryBy': manifestPiece.Origin.UTCExpectedDeliveryBy,
      'Assigned': packageStatus[0].json.Assigned,
      'Attempted': packageStatus[0].json.Attempted,
      'Delivered': packageStatus[0].json.Delivered,
      'Loaded': packageStatus[0].json.Loaded,
      'PickUp': packageStatus[0].json.PickUp,
      'StopIdentifierForGrouping': "Origin" + manifestPiece.Origin.StopIdentifier,
      'showSignature': false,
      'status': 'notloaded'

    };
    pieceDetailWithPackageStatus.push(manifestPieceDetail);
  }
  if (manifestPiece.Origin === undefined && manifestPiece.Destination !== undefined) {
    manifestPieceDetail = {
      'Type': 'delivery',
      'Barcode': manifestPiece.Barcode,
      'SignatureRequirement': manifestPiece.SignatureRequirement,
      'Address': manifestPiece.Destination.Address,
      'Address2': manifestPiece.Destination.Address2,
      'AddressQuality': manifestPiece.Destination.AddressQuality,
      'City': manifestPiece.Destination.City,
      'Contact': manifestPiece.Destination.Contact,
      'Country': manifestPiece.Destination.Country,
      'DeliveryRoute': manifestPiece.Destination.DeliveryRoute,
      'DeliveryRouteSequence': manifestPiece.Destination.DeliveryRouteSequence,
      'FacilityCode': manifestPiece.Destination.FacilityCode,
      'Instruction': manifestPiece.Destination.Instruction,
      'Latitude': manifestPiece.Destination.Latitude,
      'LocationType': manifestPiece.Destination.LocationType,
      'Longitude': manifestPiece.Destination.Longitude,
      'Organization': manifestPiece.Destination.Organization,
      'Phone': manifestPiece.Destination.Phone,
      'PhoneExtension': manifestPiece.Destination.PhoneExtension,
      'PostalCode': manifestPiece.Destination.PostalCode,
      'State': manifestPiece.Destination.State,
      'StopIdentifier': manifestPiece.Destination.StopIdentifier,
      'UTCExpectedDeliveryBy': manifestPiece.Destination.UTCExpectedDeliveryBy,
      'Assigned': packageStatus[0].json.Assigned,
      'Attempted': packageStatus[0].json.Attempted,
      'Delivered': packageStatus[0].json.Delivered,
      'Loaded': packageStatus[0].json.Loaded,
      'PickUp': packageStatus[0].json.PickUp,
      'StopIdentifierForGrouping': "Destination" + manifestPiece.Destination.StopIdentifier,
      'showSignature': false,
      'status': 'notloaded'
    };
    pieceDetailWithPackageStatus.push(manifestPieceDetail);
  }

  if (manifestPiece.Origin != undefined && manifestPiece.Destination != undefined) {
    if(vm.pickup){
      manifestPieceDetail = {
        'Type': 'delivery',
        'Barcode': manifestPiece.Barcode,
        'SignatureRequirement': manifestPiece.SignatureRequirement,
        'Address': manifestPiece.Destination.Address,
        'Address2': manifestPiece.Destination.Address2,
        'AddressQuality': manifestPiece.Destination.AddressQuality,
        'City': manifestPiece.Destination.City,
        'Contact': manifestPiece.Destination.Contact,
        'Country': manifestPiece.Destination.Country,
        'DeliveryRoute': manifestPiece.Destination.DeliveryRoute,
        'DeliveryRouteSequence': manifestPiece.Destination.DeliveryRouteSequence,
        'FacilityCode': manifestPiece.Destination.FacilityCode,
        'Instruction': manifestPiece.Destination.Instruction,
        'Latitude': manifestPiece.Destination.Latitude,
        'LocationType': manifestPiece.Destination.LocationType,
        'Longitude': manifestPiece.Destination.Longitude,
        'Organization': manifestPiece.Destination.Organization,
        'Phone': manifestPiece.Destination.Phone,
        'PhoneExtension': manifestPiece.Destination.PhoneExtension,
        'PostalCode': manifestPiece.Destination.PostalCode,
        'State': manifestPiece.Destination.State,
        'StopIdentifier': manifestPiece.Destination.StopIdentifier,
        'UTCExpectedDeliveryBy': manifestPiece.Destination.UTCExpectedDeliveryBy,
        'Assigned': packageStatus[0].json.Assigned,
        'Attempted': packageStatus[0].json.Attempted,
        'Delivered': packageStatus[0].json.Delivered,
        'Loaded': packageStatus[0].json.Loaded,
        'PickUp': packageStatus[0].json.PickUp,
        'StopIdentifierForGrouping': "Destination" + manifestPiece.Destination.StopIdentifier,
        'showSignature': false,
        'status': 'notloaded',

      };
      pieceDetailWithPackageStatus.push(manifestPieceDetail);
    }else{
      manifestPieceDetail = {
        'Type': 'pickup',
        'Barcode': manifestPiece.Barcode,
        'SignatureRequirement': manifestPiece.SignatureRequirement,
        'Address': manifestPiece.Origin.Address,
        'Address2': manifestPiece.Origin.Address2,
        'AddressQuality': manifestPiece.Origin.AddressQuality,
        'City': manifestPiece.Origin.City,
        'Contact': manifestPiece.Origin.Contact,
        'Country': manifestPiece.Origin.Country,
        'DeliveryRoute': manifestPiece.Origin.DeliveryRoute,
        'DeliveryRouteSequence': manifestPiece.Origin.DeliveryRouteSequence,
        'FacilityCode': manifestPiece.Origin.FacilityCode,
        'Instruction': manifestPiece.Origin.Instruction,
        'Latitude': manifestPiece.Origin.Latitude,
        'LocationType': manifestPiece.Origin.LocationType,
        'Longitude': manifestPiece.Origin.Longitude,
        'Organization': manifestPiece.Origin.Organization,
        'Phone': manifestPiece.Origin.Phone,
        'PhoneExtension': manifestPiece.Origin.PhoneExtension,
        'PostalCode': manifestPiece.Origin.PostalCode,
        'State': manifestPiece.Origin.State,
        'StopIdentifier': manifestPiece.Origin.StopIdentifier,
        'UTCExpectedDeliveryBy': manifestPiece.Origin.UTCExpectedDeliveryBy,
        'Assigned': packageStatus[0].json.Assigned,
        'Attempted': packageStatus[0].json.Attempted,
        'Delivered': packageStatus[0].json.Delivered,
        'Loaded': packageStatus[0].json.Loaded,
        'PickUp': packageStatus[0].json.PickUp,
        'StopIdentifierForGrouping': "Origin" + manifestPiece.Origin.StopIdentifier,
        'showSignature': false,
        'status': 'notloaded'
      };
      pieceDetailWithPackageStatus.push(manifestPieceDetail);
    }
  }
}
}
})();