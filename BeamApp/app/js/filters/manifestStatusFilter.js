(function () {
  'use strict';

  angular
    .module('beam')
    .filter('manifestStatus', manifestStatus);
  function manifestStatus ($rootScope) {
    function manifestStatusFilter(barcode) {
      var status = "loaded" ;
      var showItem = "true";
      var packages = $rootScope.packageCollection;
      if($rootScope.packageCollection.length > 0){
           for (var item in packages) {
          if(showItem){
            if(packages[item].json.Barcode == barcode){
              if(packages[item].json.Delivered == false && packages[item].json.Assigned == true && packages[item].json.Attempted == false && packages[item].json.Loaded == true){
                status = "readyToDeliver";
                showItem= false; 
              }
               if(packages[item].json.Delivered == true){
                status = "delivered";
                showItem= false;
              }
               if(packages[item].json.Assigned == false && packages[item].json.Delivered == false && packages[item].json.Attempted == false){
                status = "notAssigned";
                showItem= "false"; 
                return status;
              }
              if(packages[item].json.Attempted == true && packages[item].json.Delivered == false){
                status = "attempted";
                showItem = "false";
                return status;                
              }
              if(packages[item].json.PickUP == true){
                status = "pickUp";
                showItem = "false";
              }                   
            }else{
                status = "loaded"
            }            
          }
        };
      }else{
        status = "loaded";
      }
      return status;
    }
    manifestStatusFilter.$stateful = true;
    return manifestStatusFilter;
  };
})();













