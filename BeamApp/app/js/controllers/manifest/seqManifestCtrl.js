(function () {
    'use strict';

    angular
    .module('beam')
    .controller('seqManifestCtrl', seqManifestCtrl);

    seqManifestCtrl.$inject = ['$scope', '$stateParams', '$state', '$ionicHistory', 'mapService', 'manifestService', 'notificationService', 'deviceService', '$q','$rootScope','packageStatusService','$log','$filter','rootScopeService','scanFunctionService'];

    function seqManifestCtrl($scope, $stateParams, $state, $ionicHistory, mapService, manifestService, notificationService, deviceService, $q, $rootScope, packageStatusService, $log, $filter,rootScopeService,scanFunctionService) {
        var vm = this;
        vm.deliveryScanRouting = deliveryScanRouting;
        vm.pieceDetails = pieceDetails;
        vm.listViewRouting = listViewRouting;
        vm.routingToLoadScan = routingToLoadScan;
        vm.routingToPickUpScan = routingToPickUpScan;
        var location;
        vm.googleMapLoadRoute = false;
        vm.navigation = navigation;
        var seqmanifestView = [];
        var promiseColl = [];
        $scope.$on("$ionicView.beforeEnter", function () {
         seqmanifestView = [];
         vm.onRoute = $rootScope.onRoute;
          angular.forEach($rootScope.manifestData, function (manifestPiece, index) { 
                if($rootScope.packageCollection.length>0){
                    promiseColl.push(
                        packageStatusService.getPackageByBarcode(manifestPiece.Barcode).then(function(data){
                            if(data.length>0){
                                if(data[0].json.Delivered === false && data[0].json.PickUp === false){
                                    addManifestWithPackageStatus(manifestPiece,data);
                                }
                            }else{
                                addManifest(manifestPiece);
                            }
                        },function (err){
                            $log.info("Error in getting package status",err);
                        })
                        );
                }else{
                    promiseColl.push(addManifest(manifestPiece));
                }
            });
            
        });

        $scope.$on("$ionicView.enter", function () {
             if(deviceService.getConnectionStatus()) location = deviceService.getLocation(); 
            $q.all(promiseColl).then(function () {
                vm.groupedSeqManifestView = [];
                var grpList = [];
                var orderedData = $filter('orderBy')(seqmanifestView, 'DeliveryRouteSequence');
                var groupedData = $filter('groupBy')(orderedData, 'StopIdentifierForGrouping');
                angular.forEach(groupedData, function (key, val) {
                    if(key.length >1){
                        var length;
                        if(key.length == 1){
                            length = 0;
                        }else{
                            length = ((key.length) -1);
                        }
                        var showSignatureCount = 0;
                        for (var i in key){
                            if(key[i].SignatureRequirement == 'Required'){
                                showSignatureCount++           
                            }
                            if((i == length) && showSignatureCount >= 1){
                                for (var j in key){
                                    key[j].showSignature = true;
                                }
                            }        
                        }
                        grpList.push(key);
                    }else{
                        if(key[0].SignatureRequirement == 'Required'){
                            key[0].showSignature = true;       
                        }
                        grpList.push(key);
                    }

                });
                vm.groupedSeqManifestView = grpList;
                if((deviceService.getConnectionStatus()) && (vm.groupedSeqManifestView.length !=0)){
                    location.then(function (data) {
                        var originLatLog;
                        var destLatLog;
                        if(data.lat == "" && data.long == ""){
                            destLatLog = { lat: parseFloat(vm.groupedSeqManifestView[0][0].Latitude), lng: parseFloat(vm.groupedSeqManifestView[0][0].Longitude) };
                            var mapDestination = mapService.destinationSeqMap(destLatLog);
                              mapDestination.then(function(data){
                                vm.googleMapLoad = data; 
                              }, function(err){
                                vm.googleMapLoad = false;
                                $log.warn('Error in getting googleMap status', err);
                              });
                        }else{
                            originLatLog = { lat: data.lat, lng: data.long };
                            destLatLog = { lat: parseFloat(vm.groupedSeqManifestView[0][0].Latitude), lng: parseFloat(vm.groupedSeqManifestView[0][0].Longitude) };
                            var mapload = mapService.mapWithRouting(originLatLog, destLatLog);
                            mapload.then(function (data) {
                                vm.googleMapLoadRoute = data;
                            }, function (err) {
                                vm.googleMapLoadRoute = false;
                                $log.info('Error in getting googleMap status', err);
                            });
                        }
                        
                    }, function (error) {
                        $log.info('Error in getting location status', error);
                    })
                }
            }, function (err) {
            });
        });

        $scope.$on("$ionicView.afterEnter", function () {
            
            notificationService.hideLoad();
        });

        function addManifest(manifestPiece){
            var seqmanifest = "";
            if(manifestPiece.Origin !== undefined && manifestPiece.Destination == undefined){
                seqmanifest= {
                    'Type': 'pickup',
                    'Barcode': manifestPiece.Barcode,
                    'SignatureRequirement':manifestPiece.SignatureRequirement,
                    'Address': manifestPiece.Origin.Address,
                    'Address2': manifestPiece.Origin.Address2,
                    'AddressQuality':manifestPiece.Origin.AddressQuality,
                    'City': manifestPiece.Origin.City,
                    'Contact':  manifestPiece.Origin.Contact,
                    'Country': manifestPiece.Origin.Country,
                    'DeliveryRoute':  manifestPiece.Origin.DeliveryRoute,
                    'DeliveryRouteSequence': manifestPiece.Origin.DeliveryRouteSequence,
                    'FacilityCode': manifestPiece.Origin.FacilityCode,
                    'Instruction':  manifestPiece.Origin.Instruction,
                    'Latitude':  manifestPiece.Origin.Latitude,
                    'LocationType':  manifestPiece.Origin.LocationType,
                    'Longitude':  manifestPiece.Origin.Longitude,
                    'Organization': manifestPiece.Origin.Organization,
                    'Phone':  manifestPiece.Origin.Phone,
                    'PhoneExtension':  manifestPiece.Origin.PhoneExtension,
                    'PostalCode':  manifestPiece.Origin.PostalCode,
                    'State': manifestPiece.Origin.State,
                    'StopIdentifier':  manifestPiece.Origin.StopIdentifier,
                    'UTCExpectedDeliveryBy':manifestPiece.Origin.UTCExpectedDeliveryBy,
                    'StopIdentifierForGrouping':  "Origin"+manifestPiece.Origin.StopIdentifier,
                    'Assigned': '',
                    'Attempted': '',
                    'Delivered': '',
                    'Loaded': '',
                    'PickUp': '',
                    'showSignature': false

                }
                seqmanifestView.push(seqmanifest);
            }
            if(manifestPiece.Origin == undefined && manifestPiece.Destination != undefined){
                seqmanifest ={
                    'Type': 'delivery',
                    'Barcode': manifestPiece.Barcode,
                    'SignatureRequirement':manifestPiece.SignatureRequirement,
                    'Address': manifestPiece.Destination.Address,
                    'Address2': manifestPiece.Destination.Address2,
                    'AddressQuality':manifestPiece.Destination.AddressQuality,
                    'City': manifestPiece.Destination.City,
                    'Contact':  manifestPiece.Destination.Contact,
                    'Country': manifestPiece.Destination.Country,
                    'DeliveryRoute':  manifestPiece.Destination.DeliveryRoute,
                    'DeliveryRouteSequence': manifestPiece.Destination.DeliveryRouteSequence,
                    'FacilityCode': manifestPiece.Destination.FacilityCode,
                    'Instruction':  manifestPiece.Destination.Instruction,
                    'Latitude':  manifestPiece.Destination.Latitude,
                    'LocationType':  manifestPiece.Destination.LocationType,
                    'Longitude':  manifestPiece.Destination.Longitude,
                    'Organization': manifestPiece.Destination.Organization,
                    'Phone': manifestPiece.Destination.Phone,
                    'PhoneExtension':  manifestPiece.Destination.PhoneExtension,
                    'PostalCode':  manifestPiece.Destination.PostalCode,
                    'State': manifestPiece.Destination.State,
                    'StopIdentifier':  manifestPiece.Destination.StopIdentifier,
                    'UTCExpectedDeliveryBy':manifestPiece.Destination.UTCExpectedDeliveryBy,
                    'StopIdentifierForGrouping':  "Destination"+manifestPiece.Destination.StopIdentifier,
                    'Assigned': '',
                    'Attempted': '',
                    'Delivered': '',
                    'Loaded': '',
                    'PickUp': '',
                    'showSignature': false

                }
                seqmanifestView.push(seqmanifest);
            }

            if(manifestPiece.Origin != undefined && manifestPiece.Destination != undefined){
                seqmanifest ={
                    'Type': 'delivery',
                    'Barcode': manifestPiece.Barcode,
                    'SignatureRequirement':manifestPiece.SignatureRequirement,
                    'Address': manifestPiece.Destination.Address,
                    'Address2': manifestPiece.Destination.Address2,
                    'AddressQuality':manifestPiece.Destination.AddressQuality,
                    'City': manifestPiece.Destination.City,
                    'Contact':  manifestPiece.Destination.Contact,
                    'Country': manifestPiece.Destination.Country,
                    'DeliveryRoute':  manifestPiece.Destination.DeliveryRoute,
                    'DeliveryRouteSequence': manifestPiece.Destination.DeliveryRouteSequence,
                    'FacilityCode': manifestPiece.Destination.FacilityCode,
                    'Instruction':  manifestPiece.Destination.Instruction,
                    'Latitude':  manifestPiece.Destination.Latitude,
                    'LocationType':  manifestPiece.Destination.LocationType,
                    'Longitude':  manifestPiece.Destination.Longitude,
                    'Organization': manifestPiece.Destination.Organization,
                    'Phone': manifestPiece.Destination.Phone,
                    'PhoneExtension':  manifestPiece.Destination.PhoneExtension,
                    'PostalCode':  manifestPiece.Destination.PostalCode,
                    'State': manifestPiece.Destination.State,
                    'StopIdentifier':  manifestPiece.Destination.StopIdentifier,
                    'UTCExpectedDeliveryBy':manifestPiece.Destination.UTCExpectedDeliveryBy,
                    'StopIdentifierForGrouping':  "Destination"+manifestPiece.Destination.StopIdentifier,
                    'Assigned': '',
                    'Attempted': '',
                    'Delivered': '',
                    'Loaded': '',
                    'PickUp': '',
                    'showSignature': false

                }
                seqmanifestView.push(seqmanifest);
                seqmanifest= {
                    'Type': 'pickup',
                    'Barcode': manifestPiece.Barcode,
                    'SignatureRequirement':manifestPiece.SignatureRequirement,
                    'Address': manifestPiece.Origin.Address,
                    'Address2': manifestPiece.Origin.Address2,
                    'AddressQuality':manifestPiece.Origin.AddressQuality,
                    'City': manifestPiece.Origin.City,
                    'Contact':  manifestPiece.Origin.Contact,
                    'Country': manifestPiece.Origin.Country,
                    'DeliveryRoute':  manifestPiece.Origin.DeliveryRoute,
                    'DeliveryRouteSequence': manifestPiece.Origin.DeliveryRouteSequence,
                    'FacilityCode': manifestPiece.Origin.FacilityCode,
                    'Instruction':  manifestPiece.Origin.Instruction,
                    'Latitude':  manifestPiece.Origin.Latitude,
                    'LocationType':  manifestPiece.Origin.LocationType,
                    'Longitude':  manifestPiece.Origin.Longitude,
                    'Organization': manifestPiece.Origin.Organization,
                    'Phone':  manifestPiece.Origin.Phone,
                    'PhoneExtension':  manifestPiece.Origin.PhoneExtension,
                    'PostalCode':  manifestPiece.Origin.PostalCode,
                    'State': manifestPiece.Origin.State,
                    'StopIdentifier':  manifestPiece.Origin.StopIdentifier,
                    'UTCExpectedDeliveryBy':manifestPiece.Origin.UTCExpectedDeliveryBy,
                    'StopIdentifierForGrouping':  "Origin"+manifestPiece.Origin.StopIdentifier,
                    'Assigned': '',
                    'Attempted': '',
                    'Delivered': '',
                    'Loaded': '',
                    'PickUp': '',
                    'showSignature': false

                }
                seqmanifestView.push(seqmanifest);
            }
        }

        function addManifestWithPackageStatus(manifestPiece, packageStatus) {
            var manifest = {};
            if (manifestPiece.Origin !== undefined && manifestPiece.Destination === undefined) {
                manifest = {
                    'Type': 'pickup',
                    'Barcode': manifestPiece.Barcode,
                    'SignatureRequirement': manifestPiece.SignatureRequirement,
                    'Address': manifestPiece.Origin.Address,
                    'Address2': manifestPiece.Origin.Address2,
                    'AddressQuality': manifestPiece.Origin.AddressQuality,
                    'City': manifestPiece.Origin.City,
                    'Contact': manifestPiece.Origin.Contact,
                    'Country': manifestPiece.Origin.Country,
                    'DeliveryRoute': manifestPiece.Origin.DeliveryRoute,
                    'DeliveryRouteSequence': manifestPiece.Origin.DeliveryRouteSequence,
                    'FacilityCode': manifestPiece.Origin.FacilityCode,
                    'Instruction': manifestPiece.Origin.Instruction,
                    'Latitude': manifestPiece.Origin.Latitude,
                    'LocationType': manifestPiece.Origin.LocationType,
                    'Longitude': manifestPiece.Origin.Longitude,
                    'Organization': manifestPiece.Origin.Organization,
                    'Phone': manifestPiece.Origin.Phone,
                    'PhoneExtension': manifestPiece.Origin.PhoneExtension,
                    'PostalCode': manifestPiece.Origin.PostalCode,
                    'State': manifestPiece.Origin.State,
                    'StopIdentifier': manifestPiece.Origin.StopIdentifier,
                    'UTCExpectedDeliveryBy': manifestPiece.Origin.UTCExpectedDeliveryBy,
                    'Assigned': packageStatus[0].json.Assigned,
                    'Attempted': packageStatus[0].json.Attempted,
                    'Delivered': packageStatus[0].json.Delivered,
                    'Loaded': packageStatus[0].json.Loaded,
                    'PickUp': packageStatus[0].json.PickUp,
                    'StopIdentifierForGrouping': "Origin" + manifestPiece.Origin.StopIdentifier,
                    'showSignature': false


                };
                seqmanifestView.push(manifest);
            }
            if (manifestPiece.Origin === undefined && manifestPiece.Destination !== undefined) {
                manifest = {
                    'Type': 'delivery',
                    'Barcode': manifestPiece.Barcode,
                    'SignatureRequirement': manifestPiece.SignatureRequirement,
                    'Address': manifestPiece.Destination.Address,
                    'Address2': manifestPiece.Destination.Address2,
                    'AddressQuality': manifestPiece.Destination.AddressQuality,
                    'City': manifestPiece.Destination.City,
                    'Contact': manifestPiece.Destination.Contact,
                    'Country': manifestPiece.Destination.Country,
                    'DeliveryRoute': manifestPiece.Destination.DeliveryRoute,
                    'DeliveryRouteSequence': manifestPiece.Destination.DeliveryRouteSequence,
                    'FacilityCode': manifestPiece.Destination.FacilityCode,
                    'Instruction': manifestPiece.Destination.Instruction,
                    'Latitude': manifestPiece.Destination.Latitude,
                    'LocationType': manifestPiece.Destination.LocationType,
                    'Longitude': manifestPiece.Destination.Longitude,
                    'Organization': manifestPiece.Destination.Organization,
                    'Phone': manifestPiece.Destination.Phone,
                    'PhoneExtension': manifestPiece.Destination.PhoneExtension,
                    'PostalCode': manifestPiece.Destination.PostalCode,
                    'State': manifestPiece.Destination.State,
                    'StopIdentifier': manifestPiece.Destination.StopIdentifier,
                    'UTCExpectedDeliveryBy': manifestPiece.Destination.UTCExpectedDeliveryBy,
                    'Assigned': packageStatus[0].json.Assigned,
                    'Attempted': packageStatus[0].json.Attempted,
                    'Delivered': packageStatus[0].json.Delivered,
                    'Loaded': packageStatus[0].json.Loaded,
                    'PickUp': packageStatus[0].json.PickUp,
                    'StopIdentifierForGrouping': "Destination" + manifestPiece.Destination.StopIdentifier,
                    'showSignature': false


                };
                seqmanifestView.push(manifest);
            }

            if (manifestPiece.Origin != undefined && manifestPiece.Destination != undefined) {
                manifest = {
                    'Type': 'delivery',
                    'Barcode': manifestPiece.Barcode,
                    'SignatureRequirement': manifestPiece.SignatureRequirement,
                    'Address': manifestPiece.Destination.Address,
                    'Address2': manifestPiece.Destination.Address2,
                    'AddressQuality': manifestPiece.Destination.AddressQuality,
                    'City': manifestPiece.Destination.City,
                    'Contact': manifestPiece.Destination.Contact,
                    'Country': manifestPiece.Destination.Country,
                    'DeliveryRoute': manifestPiece.Destination.DeliveryRoute,
                    'DeliveryRouteSequence': manifestPiece.Destination.DeliveryRouteSequence,
                    'FacilityCode': manifestPiece.Destination.FacilityCode,
                    'Instruction': manifestPiece.Destination.Instruction,
                    'Latitude': manifestPiece.Destination.Latitude,
                    'LocationType': manifestPiece.Destination.LocationType,
                    'Longitude': manifestPiece.Destination.Longitude,
                    'Organization': manifestPiece.Destination.Organization,
                    'Phone': manifestPiece.Destination.Phone,
                    'PhoneExtension': manifestPiece.Destination.PhoneExtension,
                    'PostalCode': manifestPiece.Destination.PostalCode,
                    'State': manifestPiece.Destination.State,
                    'StopIdentifier': manifestPiece.Destination.StopIdentifier,
                    'UTCExpectedDeliveryBy': manifestPiece.Destination.UTCExpectedDeliveryBy,
                    'Assigned': packageStatus[0].json.Assigned,
                    'Attempted': packageStatus[0].json.Attempted,
                    'Delivered': packageStatus[0].json.Delivered,
                    'Loaded': packageStatus[0].json.Loaded,
                    'PickUp': packageStatus[0].json.PickUp,
                    'StopIdentifierForGrouping': "Destination" + manifestPiece.Destination.StopIdentifier,
                    'showSignature': false

                };
                seqmanifestView.push(manifest);
                manifest = {
                    'Type': 'pickup',
                    'Barcode': manifestPiece.Barcode,
                    'SignatureRequirement': manifestPiece.SignatureRequirement,
                    'Address': manifestPiece.Origin.Address,
                    'Address2': manifestPiece.Origin.Address2,
                    'AddressQuality': manifestPiece.Origin.AddressQuality,
                    'City': manifestPiece.Origin.City,
                    'Contact': manifestPiece.Origin.Contact,
                    'Country': manifestPiece.Origin.Country,
                    'DeliveryRoute': manifestPiece.Origin.DeliveryRoute,
                    'DeliveryRouteSequence': manifestPiece.Origin.DeliveryRouteSequence,
                    'FacilityCode': manifestPiece.Origin.FacilityCode,
                    'Instruction': manifestPiece.Origin.Instruction,
                    'Latitude': manifestPiece.Origin.Latitude,
                    'LocationType': manifestPiece.Origin.LocationType,
                    'Longitude': manifestPiece.Origin.Longitude,
                    'Organization': manifestPiece.Origin.Organization,
                    'Phone': manifestPiece.Origin.Phone,
                    'PhoneExtension': manifestPiece.Origin.PhoneExtension,
                    'PostalCode': manifestPiece.Origin.PostalCode,
                    'State': manifestPiece.Origin.State,
                    'StopIdentifier': manifestPiece.Origin.StopIdentifier,
                    'UTCExpectedDeliveryBy': manifestPiece.Origin.UTCExpectedDeliveryBy,
                    'Assigned': packageStatus[0].json.Assigned,
                    'Attempted': packageStatus[0].json.Attempted,
                    'Delivered': packageStatus[0].json.Delivered,
                    'Loaded': packageStatus[0].json.Loaded,
                    'PickUp': packageStatus[0].json.PickUp,
                    'StopIdentifierForGrouping': "Origin" + manifestPiece.Origin.StopIdentifier,
                    'showSignature': false


                };
                seqmanifestView.push(manifest);
            }
        }

        function deliveryScanRouting(stopIdentifier,instruction,type) {
            scanFunctionService.clearRootScope();
            rootScopeService.clearLocalStorage();
            if(instruction != ""){
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                $state.go('app.detail', { 'stopIdentifier': stopIdentifier, 'type': type });
            }else{
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                $state.go('app.scan.stop', { 'stopIdentifier': stopIdentifier });
            }
        };
        function pieceDetails(stopIdentifier, type) {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('app.detail', { 'stopIdentifier': stopIdentifier, 'type': type })
        };
        function listViewRouting() {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('app.manifest');
        };
        function routingToLoadScan(stopIdentifier,instruction) {
            scanFunctionService.clearRootScope();
            rootScopeService.clearLocalStorage();
            if(instruction != ""){
                notificationService.alert('','',instruction,function(){
                    $ionicHistory.nextViewOptions({
                        disableBack: true
                    });
                    $state.go('app.scan.stop', { 'stopIdentifier': stopIdentifier });
                });
            }else{
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                $state.go('app.scan.stop', { 'stopIdentifier': stopIdentifier });
            }
        }
        function navigation(data) {
            if (ionic.Platform.isIOS())
                window.open("http://maps.apple.com/?q=" + data, '_system');
            else
                window.open("http://maps.google.com/?q="+data, '_system');
        }
        function routingToPickUpScan(stopIdentifier,instruction){
            scanFunctionService.clearRootScope();
            rootScopeService.clearLocalStorage();
            if(instruction != ""){
                notificationService.alert('','',instruction,function(){
                    $ionicHistory.nextViewOptions({
                        disableBack: true
                    });
                    $state.go('app.scan.stop', { 'stopIdentifier': stopIdentifier });
                });
            }else{
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                $state.go('app.scan.stop', { 'stopIdentifier': stopIdentifier });
            }
        }
    }
})();